<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/* Admin Routes */
$route['default_controller'] = 'Admin/login';
/* End Admin Routes */


/* Start Customer Api List */
$route['api/customer/login']['POST'] = 'Customer/login';
$route['api/customer/otp_verify']['POST'] = 'Customer/otp_verify';
$route['api/customer/otp_generate']['POST'] = 'Customer/otp_generate';
$route['api/customer/register']['POST'] = 'Customer/register';
$route['api/customer/fcm_token']['POST'] = 'Customer/update_fcm_token';
$route['api/customer/profile']['GET'] = 'Customer/get_profile';
$route['api/customer/updateprofile']['POST'] = 'Customer/update_profile';
$route['api/customer/locationupdate']['POST'] = 'Customer/locationupdate';
$route['api/customer/book_service']['POST'] = 'Customer/book_service';
$route['api/customer/book_service_image']['POST'] = 'Customer/book_service_image';
$route['api/customer/category_list']['GET'] = 'Customer/category_list';
$route['api/customer/booking_list']['GET'] = 'Customer/booking_list';
$route['api/customer/logout']['GET'] = 'Customer/logout';
$route['api/customer/cancel_service']['GET'] = 'Customer/cancel_service';
$route['api/customer/search_service']['POST'] = 'Customer/search_service';

$route['api/customer/send_sms']['GET'] = 'Customer/send_sms';

/* End Customer Api List */ 


/* Start Vendor Api List */
$route['api/vendor/login']['POST'] = 'Vendor/login';
$route['api/vendor/otp_verify']['POST'] = 'Vendor/otp_verify';
$route['api/vendor/otp_generate']['POST'] = 'Vendor/otp_generate';
$route['api/vendor/register']['POST'] = 'Vendor/register';
$route['api/vendor/fcm_token']['POST'] = 'Vendor/update_fcm_token';
$route['api/vendor/profile']['GET'] = 'Vendor/get_profile';
$route['api/vendor/updateprofile']['POST'] = 'Vendor/update_profile';
$route['api/vendor/update_profile_data']['POST'] = 'Vendor/update_profile_data';
$route['api/vendor/locationupdate']['POST'] = 'Vendor/locationupdate';
$route['api/vendor/job_list']['GET'] = 'Vendor/job_list';
$route['api/vendor/accept_job']['POST'] = 'Vendor/accept_job';
$route['api/vendor/job_start_otp_verify']['POST'] = 'Vendor/job_start_otp_verify';
$route['api/vendor/stop_job']['POST'] = 'Vendor/stop_job';
$route['api/vendor/category_based_services']['GET'] = 'Vendor/category_based_services';
$route['api/vendor/logout']['GET'] = 'Vendor/logout';
$route['api/vendor/followup_action']['POST'] = 'Vendor/followup_action';
$route['api/vendor/complete_action']['POST'] = 'Vendor/complete_action';
$route['api/vendor/complete_action_image']['POST'] = 'Vendor/complete_action_image';
$route['api/vendor/today_earnings']['GET'] = 'Vendor/today_earnings';
$route['api/vendor/month_earnings']['POST'] = 'Vendor/month_earnings';
$route['api/vendor/custom_earnings']['POST'] = 'Vendor/custom_earnings';
$route['api/vendor/insert_chat']['POST'] = 'Vendor/insert_chat';
$route['api/vendor/chat']['GET'] = 'Vendor/chat';


/* End Vendor Api List */





$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
