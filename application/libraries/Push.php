<?php

/**
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Push {

    // push message title
    private $title;
    private $message;
    private $image;
    private $type;
    private $id;
   // private $typeid;
    // private $time;
    // push message payload
  
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;

    function __construct() {
        
    }

    public function setTitle($title) {
        $this->title = $title;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
    public function setType($data) {
        $this->type = $data;
    }
    public function setId($data) {
        $this->id = $data;
    }
    
    public function setPayload($data) {
        $this->data = $data;
    }
    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }

    public function getPush() {
        $res = array();
        $res['data']['title'] = $this->title;
        $res['data']['is_background'] = $this->is_background;
        $res['data']['message'] = $this->message;
        $res['data']['type'] = $this->type;
        $res['data']['id'] = $this->id;
       /* $res['data']['typeid'] = $this->typeid;
        $res['data']['studentid'] = $this->studentid;    // Additional adding student id
        $res['data']['classid'] = $this->classid;         // Additional adding class id*/
        $res['data']['image'] = $this->image;
        $res['data']['payload'] = $this->data;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');;

        $res['notification']['title'] = $this->title;
        $res['notification']['is_background'] = $this->is_background;
        $res['notification']['message'] = $this->message;
        $res['notification']['type'] = $this->type;
        $res['notification']['id'] = $this->id;
        /*$res['data']['typeid'] = $this->typeid;
        $res['notification']['studentid'] = $this->studentid;    // Additional adding student id
        $res['data']['classid'] = $this->classid;   */              // Additional adding class id
        $res['notification']['image'] = $this->image;
        $res['notification']['payload'] = $this->data;
        $res['notification']['timestamp'] = date('Y-m-d G:i:s');;

        return $res;
    }

}
