<?php
class Admin_model extends CI_Model
{
	
	public function login($data)
	{
		    $this->db->select('*');
			$this->db->from('admin');
			$this->db->where('email',$data['email']);
			$this->db->where('password',md5($data['password']));
			$this->db->limit(1);
			$query = $this->db->get();
			//print_r($data);print_r($query);exit;
			$res=$query->result_array();
			if ($query->num_rows() == 1) {
			    return $res[0];
			} else { return 0; }
	}

	public function profile(){
		$admin_login=$this->session->userdata('admin'); 
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id',$admin_login['id']);
		$this->db->limit(1);
		$query = $this->db->get();
			$res=$query->result_array();
			if ($query->num_rows() == 1) {
			    return $res[0];
			} else { return 0; }

	}
	public function updateprofile($data){
		$admin_login=$this->session->userdata('admin'); 
		$admin_id=$admin_login['id'];
		$admin_data = array(
		 	        'username' => $data['email'],
					'email' => $data['email'],
					'name' => $data['name'],
					'mobile' => $data['mobile']
					);
		$this->db->where("id",$admin_id);
		$this->db->update('admin', $admin_data);
		return 1;
	}

	public function changepassword($data){
		$admin_login=$this->session->userdata('admin'); 
		$admin_id=$admin_login['id'];
		$admin_data = array(
		 	        'password' => md5($data['password']),
					'hash' => $data['password']
					);
		$this->db->where("id",$admin_id);
		$this->db->update('admin', $admin_data);
		return 1;
	}


	public function get_all_notification_admin(){
		$this->db->select('*');
		$this->db->from('notification');
		$this->db->where("type",'admin');
		$this->db->where("status",0);
		$query = $this->db->get();
		$res=$query->result_array();
		return $res;
	}

	public function get_all_notification_admin_read(){
		$array=array('status'=>1);
		$this->db->where("type",'admin');
		$this->db->update('notification',$array);
		return 1;
	}


	public function get_customer_count(){
		$customer = $this->db->get_where('customer',array('status' => 1))->result_array();
		return $customer;
	}

	public function get_vendor_count(){
		$vendor = $this->db->get_where('vendor',array('status' => 1))->result_array();
		return $vendor;
	}

	public function get_completed_jobs_count(){
		$jobs = $this->db->get_where('service',array('status' => 5))->result_array();
		return $jobs;
	}

	public function get_category_count(){
		$category = $this->db->get_where('category',array('status' => 0))->result_array();
		return $category;
	}

	public function get_service_count(){
		$service = $this->db->get_where('servicetype',array('status' => 1))->result_array();
		return $service;
	}


	public function get_service_req_count(){
		$service = $this->db->get_where('book_request',array('status' => 0))->result_array();
		return $service;
	}

	public function get_service_inprogress_count(){
		//$service = $this->db->get_where('service',array('status' => 2))->result_array();
		$condition="status=7 OR status=2";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$query = $this->db->get();
		$service=$query->result_array();
		return $service;
	}

	public function get_today_earning(){
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_time=$date->format('h:i A');
		$cur_date=$date->format('d-m-Y');
		$today_service = $this->db->get_where('service', array('status'=>5,'complete_date'=>$cur_date))->result_array();
		//print_r($today_service);
		$today=array();$i=0;$today_earn=0;
		if(count($today_service)>0){
			foreach ($today_service as $key => $value) {
				$today_earn += $value['total_cost'];
			}
		}
		return number_format($today_earn,2);
	}



	public function get_vendor_counts()
    {
		$this->db->select('a.name,a.email,a.mobile,a.address,a.id,b.timestmp,b.message,b.id as chat_id,a.token,a.photo');
		$this->db->from('vendor a');
		$this->db->join('chat b','b.receiver_id=a.id','left');
		$this->db->order_by('b.timestmp','DESC');
		$query = $this->db->get();
		$res=$query->result_array();
		$cc=$this->unique_array($res,'id');
		return $cc;

	}

	public function unique_array($my_array, $key) { 
	    $result = array(); 
	    $i = 0; 
	    $key_array = array(); 
	    
	    foreach($my_array as $val) { 
	        if (!in_array($val[$key], $key_array)) { 
	            $key_array[$i] = $val[$key]; 
	            $result[$i] = $val; 
	        } 
	        $i++; 
	    } 
	    return $result; 
	}



	public function get_message($id,$date1=''){
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		if($date1==''){
			$cond1 = "(sender_id ='" . $sender_id . "' AND receiver_id ='".$id."' ) OR (sender_id ='" . $id . "' AND receiver_id ='".$sender_id."')";
			$this->db->select('*');
			$this->db->from('chat');
			$this->db->where($cond1);
			$this->db->order_by('timestmp','ASC');
			$this->db->group_by('date');
			$query = $this->db->get();
			$res=$query->result_array();
			//print_r($res);
		}
		else if($date1!=''){
			$cond1 = "((sender_id ='" . $sender_id . "' AND receiver_id ='".$id."' ) OR (sender_id ='" . $id . "' AND receiver_id ='".$sender_id."')) AND date='".$date1."'";
			$this->db->select('*');
			$this->db->from('chat');
			$this->db->where($cond1);
			$this->db->order_by('timestmp','ASC');
			$query = $this->db->get();
			$res=$query->result_array();
			
		}
		
		return $res;
	}

	public function get_all_messages_count($id){
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$cond1 = "(sender_id ='" . $sender_id . "' AND receiver_id ='".$id."' ) OR (sender_id ='" . $id . "' AND receiver_id ='".$sender_id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$query = $this->db->get();
		$res=$query->result_array();
	
		return $res;
		
	}

	public function customer_details_id($id)
	{
		$this->db->select('*');
		$this->db->from('vendor');
		$this->db->where("id",$id);
		$query = $this->db->get();
		$res=$query->result_array();
		return $res[0];
	}

	public function get_msg_count($data){
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$cond1 = "(sender_id ='" . $sender_id . "' AND receiver_id ='".$data."' ) OR (sender_id ='" . $data . "' AND receiver_id ='".$sender_id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$query = $this->db->get();
		$res=$query->result_array();
		return count($res);
	}

	public function get_msg_counts($data){
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$cond1 = "(sender_id ='" . $sender_id . "' AND receiver_id ='".$data['id']."' ) OR (sender_id ='" . $data['id'] . "' AND receiver_id ='".$sender_id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$query = $this->db->get();
		$res=$query->result_array();
		return count($res);
	}

	public function insertchat($data){

		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$type='admin';
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$date_time = $date->format('Y-m-d H:i:s');
		$date1 = $date->format('Y-m-d');
		$time = $date->format('H:i A');
		
		$insert_array=array('sender_id'=>$sender_id,'receiver_id'=>$data['receiver_id'],'type'=>$type,'status'=>0,'date_time'=>$date_time,'date'=>$date1,'time'=>$time,'message' => $data['message'],'timestmp' => strtotime($date_time));
		$this->db->insert('chat',$insert_array);
        $chat_id=$this->db->insert_id();
        if($chat_id){
        	$customer = $this->db->get_where('vendor', array('id' => $data['receiver_id']))->result_array();
        	if(count($customer)>0){

        		if($customer[0]['firebase_token']!=''){

        			$this->load->library('Firebase');
					$this->load->library('Push');
					$firebase = new Firebase();
					$push = new Push();		
					$this->load->helper('date');
					$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
					$cur_time = $date->format('Y-m-d H:i:s');
					$not_time = $date->format('H:i:s');
					$user_details = $this->customer_details_id($data['receiver_id']);
					$user_name=$customer[0]['name'];
					$registrationIds=$customer[0]['firebase_token'];
					$url = 'https://fcm.googleapis.com/fcm/send';
					$priority="high";
					$title = 'New Message Received';
					$notification= array('title' => $title,'message' => 'Hi '.$user_name.', You have received new message from Admin','image'=>'','timestamp'=>$not_time );
					$notification_array= array('sender_id'=>$sender_id,'receiver_id'=>$data['receiver_id'],'title' => $title,'message' => 'Hi '.$user_name.', You have received new message from Admin','status'=>0,'type'=>'','date'=>$date1,'time'=>$not_time);
					$this->db->insert('notification',$notification_array);
					$payload = array();
					$message = $notification['message'];
					$push_type = isset($data['push_type']) ? $data['push_type'] : '';
					$push->setTitle($title);
					$push->setMessage($message);
					$push->setType("chat");
					$push->setId($chat_id);
					$push->setImage("");
					$push->setIsBackground(FALSE);
					$push->setPayload($payload);
					$json = '';
					$response = '';
					$json = $push->getPush();
					$response = $firebase->send($registrationIds, $json);
					
        		}
        	}
        }
        
        $chat = $this->db->get_where('chat', array('id' => $chat_id))->result_array();
        return $chat;
	}


	public function get_msg($data){
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$cond1 = "(sender_id ='" . $sender_id . "' AND receiver_id ='".$data['id']."' ) OR (sender_id ='" . $data['id'] . "' AND receiver_id ='".$sender_id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$this->db->group_by('date');
		$query = $this->db->get();
		$res=$query->result_array();
		$response['messages']=$res;
		$customer = $this->db->get_where('vendor', array('id' => $data['id']))->result_array();
		$response['customer']=$customer;
		return $response;
	}

	public function timeago($time) { 
  
	    // Calculate difference between current 
	    // time and given timestamp in seconds 
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$date_time = $date->format('Y-m-d H:i:s');
	   
	    $diff     = strtotime($date_time) - $time; 

	      //1573036765 1573043473
	    // Time difference in seconds 
	    $sec     = $diff; 
	      
	    // Convert time difference in minutes 
	    $min     = round($diff / 60 ); 
	      
	    // Convert time difference in hours 
	    $hrs     = round($diff / 3600); 
	      
	    // Convert time difference in days 
	    $days     = round($diff / 86400 ); 
	      
	    // Convert time difference in weeks 
	    $weeks     = round($diff / 604800); 
	      
	    // Convert time difference in months 
	    $mnths     = round($diff / 2600640 ); 
	      
	    // Convert time difference in years 
	    $yrs     = round($diff / 31207680 ); 

	    // Check for seconds 
	    if($sec <= 60) { 
	        echo "$sec seconds ago"; 
	    } 
	      
	    // Check for minutes 
	    else if($min <= 60) { 
	        if($min==1) { 
	            echo "one minute ago"; 
	        } 
	        else { 
	            echo "$min minutes ago"; 
	        } 
	    } 
	      
	    // Check for hours 
	    else if($hrs <= 24) { 
	        if($hrs == 1) {  
	            echo "an hour ago"; 
	        } 
	        else { 
	            echo "$hrs hours ago"; 
	        } 
	    } 
	      
	    // Check for days 
	    else if($days <= 7) { 
	        if($days == 1) { 
	            echo "Yesterday"; 
	        } 
	        else { 
	            echo "$days days ago"; 
	        } 
	    } 
	      
	    // Check for weeks 
	    else if($weeks <= 4.3) { 
	        if($weeks == 1) { 
	            echo "a week ago"; 
	        } 
	        else { 
	            echo "$weeks weeks ago"; 
	        } 
	    } 
	      
	    // Check for months 
	    else if($mnths <= 12) { 
	        if($mnths == 1) { 
	            echo "a month ago"; 
	        } 
	        else { 
	            echo "$mnths months ago"; 
	        } 
	    } 
	      
	    // Check for years 
	    else { 
	        if($yrs == 1) { 
	            echo "one year ago"; 
	        } 
	        else { 
	            echo "$yrs years ago"; 
	        } 
	    } 
	} 

	
	public function admin($admin_id){
		$admin = $this->db->get_where('admin', array('id' => $admin_id))->result_array();
		return $admin;
	}

	public function getofferlist(){
		$offer = $this->db->get_where('offer', array('status' => 1))->result_array();
		return $offer;
	}

	public function addoffer($post){
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_date = $date->format('Y-m-d H:i:s');
		$cur_time = $date->format('H:i:s');

		if(count($post['product_cate'])>0){
			foreach ($post['product_cate'] as $key => $value) {

				$input = array('offer_name' =>  $post['offer_name'],'offer_per' => $post['offer_per'],'status' => '1','start_date'=>$post['start_date'],'end_date'=>$post['end_date'],'pro_cate_id'=>$value,'date'=>$cur_date,'time'=>$cur_time
				);
				$result = $this->db->insert('offer', $input);
						
			}
			if($result){
				return 1;
			}
		}
		
  	}

  	public function updateoffer($data){
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_date = $date->format('Y-m-d H:i:s');
		$cur_time = $date->format('H:i:s');

		if(isset($data['product_cate']) && $data['product_cate']!=''){
			$product_cate=$data['product_cate'];
		}
		else{
			$product_cate='';
		}

		if($data['offer_name'] ==''){
			return 1;
		}
		else if($data['offer_per'] ==''){
			return 2;
		}
		else if($product_cate ==''){
			return 3;
		}
		else if( $data['start_date'] ==''){
			return 4;
		}
		else if( $data['end_date'] ==''){
			return 5;
		}
		else{

			$offer = $this->db->get_where('offer', array('status' => 1,'id'=>$data['hiddenid']))->result_array();
			if(count($offer)>0){
				$input = array('offer_name' =>  $data['offer_name'],'offer_per' => $data['offer_per'],'status' => '1','start_date'=>$data['start_date'],'end_date'=>$data['end_date'],'pro_cate_id'=>$product_cate,'date'=>$cur_date,'time'=>$cur_time
				);
				$this->db->where('id',$data['hiddenid']);
				$update=$this->db->update('offer', $input);
				if($update){
					return 6;
				}
			}

		}
		
  	}

  	public function deleteoffer($data){
        $this->db->where('id',$data['del_id']);
        $this->db->delete('offer');
        return 1;
	}

	public function addbrand($post){
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_date = $date->format('Y-m-d H:i:s');
		$cur_time = $date->format('H:i:s');

		$brand_id=implode(',',$post['brand_name']);
		//print_r($brand_id);exit;
		
		$cat_brand = $this->db->get_where('category_brand', array('category_id' => $post['selectcat'],'status'=>1))->result_array();
		if(count($cat_brand)>0){
			$input = array('category_id' =>  $post['selectcat'],'brand_id' => $brand_id,'status' => 1,'date'=>$cur_date,'time'=>$cur_time);
			$this->db->where('id',$cat_brand[0]['id']);
			$result=$this->db->update('category_brand', $input);
		}
		else{
			$input = array('category_id' =>  $post['selectcat'],'brand_id' => $brand_id,'status' => 1,'date'=>$cur_date,'time'=>$cur_time);
			$result = $this->db->insert('category_brand', $input);
		}

		if($result){
			return 1;
		}
		
  	}

  	public function updatebrand($data){
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_date = $date->format('Y-m-d H:i:s');
		$cur_time = $date->format('H:i:s');
		
		if($data['upselectcat'] ==''){
			return 1;
		}
		else if($data['upbrand_name'] ==''){
			return 2;
		}
		else{
			$brand_id=implode(',',$data['upbrand_name']);
			$cat_brand = $this->db->get_where('category_brand', array('id' => $data['hiddenid'],'status'=>1))->result_array();
			if(count($cat_brand)>0){
				$input = array('category_id' =>  $data['upselectcat'],'brand_id' => $brand_id,'status' => 1,'date'=>$cur_date,'time'=>$cur_time);
				$this->db->where('id',$cat_brand[0]['id']);
				$update=$this->db->update('category_brand', $input);
				if($update){
					return 3;
				}
			}

		}
		
  	}

  	public function deletebrand($data){
        $this->db->where('id',$data['del_id']);
        $this->db->delete('category_brand');
        return 1;
	}

	public function getnewbooking(){
		$condition = "a.status =0 AND a.notify_status=0  ";
		$this->db->select('a.*,b.*,a.id as booking_id');
        $this->db->from('book_request a');
        $this->db->where($condition); 
        $this->db->join('customer b', 'b.id = a.customer_id', 'left');
        $this->db->limit('1');
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}

	public function getsuperadmin(){
		$condition = "role=1";
		$this->db->select('*');
        $this->db->from('admin');
        $this->db->where($condition); 
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}

	public function updatenotifystatus($data){
		$booking = $this->db->get_where('book_request', array('id' => $data['booking_id'], 'status'=>0,'notify_status' => 0))->result_array();
		if(count($booking)>0){
			$condition = "id ='" . $booking[0]['id'] . "'  ";
        	$datas = array('notify_status'=>1);
		    $this->db->where($condition);
		    $update = $this->db->update('book_request', $datas ); 
		    if($update){
		    	return 1;
		    }
		}
	}

	
	
        
    
	
    
}
