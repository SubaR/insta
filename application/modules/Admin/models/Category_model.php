<?php 
    
class Category_model extends CI_Model
{

  public function __construct()
    {
      parent::__construct();
      $this->load->helper('string');
    }

/*
|---------------------------------------------------------------------------------------------------------
|                      Category List
|---------------------------------------------------------------------------------------------------------
*/
    public function viewcategorylist()
    {
            $this->db->select('*');
            $this->db->from('category');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
    }
/*
|---------------------------------------------------------------------------------------------------------
|                     Get Main Category 
|---------------------------------------------------------------------------------------------------------
*/
     public function getcategory($get_id)
    {
            $this->db->select('*');
            $this->db->from('category');
            $condition = "id ='".$get_id ."'";
            $this->db->where($condition);
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
    }


/*
|---------------------------------------------------------------------------------------------------------
|                     Adding Category 
|---------------------------------------------------------------------------------------------------------
*/


public function addcategory($crl){

           $imgname = "";
          if (!is_dir('uploads')) {
              mkdir('./uploads', 0777, true);
          }

          if(isset($_FILES) && !empty($_FILES))
          {
            $config['upload_path'] = './uploads/';     
            $config['allowed_types'] = '*';     
            $config['max_size'] = '64000';
            $this->load->library('upload', $config);  

            $upload_files = $_FILES;
            if($upload_files['photos']['name']!='')
            {
              $temp = explode(".",$upload_files['photos']['name']);
              $random_file="upload_file_".rand();
              $newfilename = $random_file. '.' .end($temp);
              $_FILES['photos'] = array(
              'name' => $newfilename,
              'type' => $upload_files['photos']['type'],
              'tmp_name' => $upload_files['photos']['tmp_name'],
              'error' => $upload_files['photos']['error'],
              'size' => 10
              );    
              $this->upload->do_upload("photos");
              $upload_data = $this->upload->data();   
              $Lastimg =$upload_data['file_name'];
               // print_r($this->upload->display_errors());exit;
            }
               $imgname =  $Lastimg;  
         }      
          $input = array(
                    'category_name' =>  $crl['category_name'],
                    'category_amount' =>  $crl['serviceamount'],
                    'icon' => $imgname 
                  );
          $result = $this->db->insert('category', $input);

          if($result == true)
          {
                return 1;
          }
          else
          {
                return 2;
          }

    }


/*
|---------------------------------------------------------------------------------------------------------
|                     Update Category 
|---------------------------------------------------------------------------------------------------------
*/
    
    public function updatecategory($upd)
    {
          $update_id=$upd['hidden_id'];

          $data = array(
          'category_name' => $upd['update_title'],
          'category_amount' =>  $upd['serviceamount'],
          );
            $condition = "id =" . "'" . $upd['hidden_id'] . "'";
            unset($upd['hidden_id']); 
            $this->db->where($condition);
            $update = $this->db->update('category', $data, $condition);

            
            if($update){
             
              $imgname = "";
              if (!is_dir('uploads')) {
                  mkdir('./uploads', 0777, true);
              }
             

              if(isset($_FILES) && !empty($_FILES))
              {
                $config['upload_path'] = './uploads/';     
                $config['allowed_types'] = '*';     
                $config['max_size'] = '64000';
                $this->load->library('upload', $config);  

                $upload_files = $_FILES;
                if($upload_files['upphotos']['name']!='')
                {
                  $temp = explode(".",$upload_files['upphotos']['name']);
                  $random_file="upload_file_".rand();
                  $newfilename = $random_file. '.' .end($temp);
                  $_FILES['upphotos'] = array(
                  'name' => $newfilename,
                  'type' => $upload_files['upphotos']['type'],
                  'tmp_name' => $upload_files['upphotos']['tmp_name'],
                  'error' => $upload_files['upphotos']['error'],
                  'size' => 10
                  );    
                  $this->upload->do_upload("upphotos");
                  $upload_data = $this->upload->data();   
                  $Lastimg =$upload_data['file_name'];
                }
                  $imgname =  $Lastimg;   
                  $reg_imgname = array('icon' => $imgname);
                  $condition = "id =" . "'" . $update_id . "'";
                  $this->db->where($condition);
                  $updates = $this->db->update('category', $reg_imgname );
              }
             
            }

            if($update == 1)
            {
                  return 1;
            }
            else
            {
                  return 0;
            }

     }


/*
|---------------------------------------------------------------------------------------------------------
|                     Delete Category 
|---------------------------------------------------------------------------------------------------------
*/
    public function delete_category($delid)
    {

            $this->db->where('id', $delid['del_id']);
            $empty =$this->db->delete('category');

            if($empty == TRUE)
            {
                    return 1;
            }
            else
            {
                    return 2;
            }
    }


/*
|---------------------------------------------------------------------------------------------------------
|                      Sub Category  List
|---------------------------------------------------------------------------------------------------------
*/
    public function viewsubcategorylist()
    {
            $this->db->select('*');
            $this->db->from('sub_category');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
    }


/*
|---------------------------------------------------------------------------------------------------------
|                     Adding Sub Category 
|---------------------------------------------------------------------------------------------------------
*/

public function addsubcategory($crl){
  
    
        $imgname = "";

        if (!is_dir('uploads')) {
          mkdir('./uploads/', 0777, true);
        }

          
          
      if(isset($_FILES) && !empty($_FILES))
      {
        $config['upload_path'] = './uploads/';     
        $config['allowed_types'] = '*';     
        $config['max_size'] = '64000';
        $this->load->library('upload', $config);  

        $upload_files = $_FILES;
        if($upload_files['upphotos']['name']!='')
        {
          $temp = explode(".",$upload_files['upphotos']['name']);
          $random_file="upload_file_".rand();
          $newfilename = $random_file. '.' .end($temp);
          $_FILES['upphotos'] = array(
          'name' => $newfilename,
          'type' => $upload_files['upphotos']['type'],
          'tmp_name' => $upload_files['upphotos']['tmp_name'],
          'error' => $upload_files['upphotos']['error'],
          'size' => 10
          );    
          $this->upload->do_upload("upphotos");
          $upload_data = $this->upload->data();   
          $Lastimg =$upload_data['file_name'];
        }
                  $imgname =  $Lastimg; 

        } 

        $input = array(
        'category_id' =>  $crl['selectcat'],
        'sub_category_name' =>  $crl['subtitle'],
        'image' => $imgname ,
        'service_type' => $crl['service_types']
      );
      $result = $this->db->insert('sub_category', $input); 

    

       if($result == true)
          {
                return 1;
          }
          else
          {
                return 2;
          }



    }


/*
|---------------------------------------------------------------------------------------------------------
|                     Update Sub Category 
|---------------------------------------------------------------------------------------------------------
*/
    
    public function updatesubcategory($upd)
    {
          $update_id=$upd['hidden_id'];
          $data = array(
          'category_id' => $upd['selectcat'],
          'sub_category_name' => $upd['subcategory'],
          'service_type' => $upd['service_types1']
          );
          
            $cond = "id =" . "'" . $upd['hidden_id'] . "'";
            unset($upd['hidden_id']); 
            $this->db->where($cond);
            $update = $this->db->update('sub_category', $data);

            
            if($update){
             
              $imgname = "";
              if (!is_dir('uploads')) {
                  mkdir('./uploads', 0777, true);
              }
             

              if(isset($_FILES) && !empty($_FILES))
              {
                $config['upload_path'] = './uploads/';     
                $config['allowed_types'] = '*';     
                $config['max_size'] = '64000';
                $this->load->library('upload', $config);  

                $upload_files = $_FILES;
                if($upload_files['upphotos']['name']!='')
                {
                  $temp = explode(".",$upload_files['upphotos']['name']);
                  $random_file="upload_file_".rand();
                  $newfilename = $random_file. '.' .end($temp);
                  $_FILES['upphotos'] = array(
                  'name' => $newfilename,
                  'type' => $upload_files['upphotos']['type'],
                  'tmp_name' => $upload_files['upphotos']['tmp_name'],
                  'error' => $upload_files['upphotos']['error'],
                  'size' => 10
                  );    
                  $this->upload->do_upload("upphotos");
                  $upload_data = $this->upload->data();   
                  $Lastimg =$upload_data['file_name'];
                }
                  $imgname =  $Lastimg;   
                  $reg_imgname = array('image' => $imgname);
                  $condition1 = "id =" . "'" . $update_id . "'";
                  $this->db->where($condition1);
                  $updates = $this->db->update('sub_category', $reg_imgname );
              }
             
            }

            if($update == 1)
            {
                  return 1;
            }
            else
            {
                  return 0;
            }

     }


/*
|---------------------------------------------------------------------------------------------------------
|                     Delete Sub Category 
|---------------------------------------------------------------------------------------------------------
*/
    public function delete_subcategory($delid)
    {

            $this->db->where('id', $delid['del_id']);
            $empty =$this->db->delete('sub_category');

            if($empty == TRUE)
            {
                    return 1;
            }
            else
            {
                    return 2;
            }
    }



  /* Suba Starts Work  */


    public function viewservicelist()
    {
      $result = $this->db->get_where('servicetype', array('status' => 1))->result_array();
      return $result;
    }

    public function addservicetype($post){

      $this->db->select('*');
      $this->db->from('servicetype');
      $checkcondition = "service_type ='" . $post['servicetype'] . "'  AND  status =1    ";
      $this->db->where($checkcondition);
      $checkquery = $this->db->get();
      $Exsit_data = $checkquery->result_array();
      if(count($Exsit_data)==0){

          $input = array(
                    'service_type' =>  $post['servicetype'],
                   /* 'service_amount' => $post['serviceamount'],*/
                    'status' => '1'
                  );
          $result = $this->db->insert('servicetype', $input);

          if($result == true)
          {
                return 1;
          }
          else
          {
                return 2;
          }

      }else{
        return 'RepeatData';
      }
  }



  public function updateservicetype($upd) {
   
      $update_id=$upd['hidden_id']; 
      $updatequery = array(
          'service_type' =>  $upd['servicetype'],
          /*'service_amount' => $upd['serviceamount']*/
        );
      $condition = "id =" . "'" . $update_id . "'  AND  status =1  ";
      $this->db->where($condition);
      $update = $this->db->update('servicetype', $updatequery );
      if($update == 1)
      {
            return 1;
      }
      else
      {
            return 2;
      }

  }


  public function deleteservicetype($delid)
    {

      $this->db->where('id', $delid['del_id']);
      $empty =$this->db->delete('servicetype');

      if($empty == TRUE)
      {
              return 1;
      }
      else
      {
              return 2;
      }
    }


    public function service_request_list(){
      $this->db->select('a.*,b.*,a.id as booking_id,c.name,c.mobile,c.email,a.status as book_status,b.address,a.reassign_status,a.brand_id');
      $this->db->from('book_request a');
      $this->db->join('customer_address b', 'b.id = a.address_id', 'left');
      $this->db->join('customer c', 'c.id = a.customer_id', 'left');
      $this->db->order_by('a.id DESC');
      $query = $this->db->get();
      $result = $query->result_array();
      //print_r($result);exit;
      return $result;
    }

    public function get_cron_vendor($get_id){
      $this->db->select('*');
      $this->db->from('cron_job_cache');
      $condition = "request_id ='".$get_id ."'";
      $this->db->where($condition);
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;
    }

    public function get_service_request_vendor($get_id){
      $this->db->select('*');
      $this->db->from('service');
      $condition = "request_id ='".$get_id ."'";
      $this->db->where($condition);
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;
    }
    public function get_service_details($get_id){
      $condition = "a.id ='".$get_id ."'";
      $this->db->select('a.*,b.name as vendor_name,b.email as vendor_email,b.mobile as vendor_mobile,b.address as vendor_address,c.name as customer_name,c.email as customer_email,c.mobile as customer_mobile,c.address as customer_address');
      $this->db->from('service a');
      $this->db->join('vendor b', 'b.id = a.vendor_id', 'left');
      $this->db->join('customer c', 'c.id = a.customer_id', 'left');
      $this->db->where($condition);
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;
    }

    public function getservice($get_id)
    {
        $this->db->select('*');
        $this->db->from('servicetype');
        $condition = "id ='".$get_id ."'";
        $this->db->where($condition);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_service_based_vendors($cat_id){
      
        $this->db->select('*');
        $this->db->from('vendor');
        $condition = "category LIKE '%".$cat_id."%'";
        $this->db->where($condition);
        //$this->db->where_in('category', $cat_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result_array();
       // print_r($result);
        return $result;
    }

    public function get_request_service($req_id){
      $condition="a.id='".$req_id."'";
      $this->db->select('a.service_id,b.service_type,a.level');
      $this->db->from('book_request a');
      $this->db->where($condition);
      $this->db->join('servicetype b', 'b.id = a.service_id', 'left');
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;
    }


    public function job_completed_list(){
      //$completed_list = $this->db->get_where('service', array('status'=>5))->result_array();
      $condition="a.status=5";
      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.mobile as customer_mobile,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,a.followup_date,a.followup_time,a.followup_comments,a.total_cost,a.brand_id,a.reassign_vendor_id,a.reassign_status,a.vendor_id,a.reassign_vendor_id');
      //c.name as vendor_name,c.mobile as vendor_mobile,c.id as vendor_id,c.photo
      $this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      //$this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $this->db->order_by('a.id DESC');
      $query = $this->db->get();
      $completed_list = $query->result_array();
      $complete=array();
      if(count($completed_list)>0){
        $i=0;
        foreach ($completed_list as $key => $value) {
          $complete[$i]['job_service_id']=$value['job_id'];
          $complete[$i]['job_ref']=$value['invoice_ref_no'];

          $complete[$i]['customer_name']=$value['customer_name'];
          $complete[$i]['customer_mobile']=$value['customer_mobile'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $complete[$i]['customer_address']=$add;
          $complete[$i]['commands']=$value['commands'];

          if($value['reassign_status']==1){
              $vendor = $this->db->get_where('vendor',array('id' => $value['reassign_vendor_id']))->result_array();
              $complete[$i]['vendor_name']=$vendor[0]['name'];
              $complete[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $complete[$i]['vendor_photo']=$vendor[0]['photo'];
              $complete[$i]['vendor_id']=$value['reassign_vendor_id'];
          }
          else{
              $vendor = $this->db->get_where('vendor',array('id' => $value['vendor_id']))->result_array();
              $complete[$i]['vendor_name']=$vendor[0]['name'];
              $complete[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $complete[$i]['vendor_photo']=$vendor[0]['photo'];
              $complete[$i]['vendor_id']=$value['vendor_id'];
          }

          $complete[$i]['vend_id']=$value['vendor_id'];
          
          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
          if($value['job_status']==5){
              $status='Completed';
          }
          $complete[$i]['job_status']=$status;
          $complete[$i]['category_name']=$category_name;

          $req=$this->get_request_service($value['request_id']);
          $complete[$i]['service']=$req[0]['service_type'];
          $complete[$i]['level']=$req[0]['level'];
          $complete[$i]['amount']=$value['total_cost'];
          $complete[$i]['date']=$value['date'];
          $complete[$i]['time']=$value['time'];
          $complete[$i]['status']=$value['job_status'];
          
          $complete[$i]['category_id']=$value['category_id'];
          $complete[$i]['followup_date']=$value['followup_date'];
          $complete[$i]['followup_time']=$value['followup_time'];
          $complete[$i]['followup_comments']=$value['followup_comments'];
          $complete[$i]['reassign_vendor_id']=$value['reassign_vendor_id'];
          $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
          $brand_name='';
          if(count($brand)>0){
            $brand_name=$brand[0]['brand_name'];
          }
          $complete[$i]['brand_name']=$brand_name;
          $i++;
        }
      }
      return $complete;

    }

    public function job_followup_list(){
      $condition="a.status=7";
      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.mobile as customer_mobile,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,a.followup_date,a.followup_time,a.followup_comments,a.total_cost,a.brand_id,a.reassign_vendor_id,a.reassign_status,a.vendor_id');
      //c.name as vendor_name,c.mobile as vendor_mobile,c.photo,c.id as vendor_id
      $this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      //$this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $this->db->order_by('a.id DESC');
      $query = $this->db->get();
      $followup_list = $query->result_array();
      $follow=array();
      if(count($followup_list)>0){
        $i=0;
        foreach ($followup_list as $key => $value) {
          $follow[$i]['job_service_id']=$value['job_id'];
          $follow[$i]['job_ref']=$value['invoice_ref_no'];

          $follow[$i]['customer_name']=$value['customer_name'];
          $follow[$i]['customer_mobile']=$value['customer_mobile'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $follow[$i]['customer_address']=$add;
          $follow[$i]['commands']=$value['commands'];
          if($value['reassign_status']==1){
              $vendor = $this->db->get_where('vendor',array('id' => $value['reassign_vendor_id']))->result_array();
              $follow[$i]['vendor_name']=$vendor[0]['name'];
              $follow[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $follow[$i]['vendor_photo']=$vendor[0]['photo'];
              $follow[$i]['vendor_id']=$value['reassign_vendor_id'];
             
          }
          else{
              $vendor = $this->db->get_where('vendor',array('id' => $value['vendor_id']))->result_array();
              $follow[$i]['vendor_name']=$vendor[0]['name'];
              $follow[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $follow[$i]['vendor_photo']=$vendor[0]['photo'];
              $follow[$i]['vendor_id']=$value['vendor_id'];
             
          }
          $follow[$i]['vend_id']=$value['vendor_id'];
         /* $follow[$i]['vendor_name']=$value['vendor_name'];
          $follow[$i]['vendor_mobile']=$value['vendor_mobile'];*/
          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
          $status='';
          if($value['job_status']==7){
              $status='Followup';
          }
          $follow[$i]['job_status']=$status;
          $follow[$i]['category_name']=$category_name;
          $req=$this->get_request_service($value['request_id']);
          $follow[$i]['service']=$req[0]['service_type'];
          $follow[$i]['level']=$req[0]['level'];
          $follow[$i]['amount']=$value['total_cost'];
          $follow[$i]['date']=$value['date'];
          $follow[$i]['time']=$value['time'];
          $follow[$i]['status']=$value['job_status'];
          $follow[$i]['category_id']=$value['category_id'];
          $follow[$i]['followup_date']=$value['followup_date'];
          $follow[$i]['followup_time']=$value['followup_time'];
          $follow[$i]['followup_comments']=$value['followup_comments'];
         // $follow[$i]['vendor_photo']=$value['photo'];
         
          $follow[$i]['reassign_vendor_id']=$value['reassign_vendor_id'];


          $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
          $brand_name='';
          if(count($brand)>0){
            $brand_name=$brand[0]['brand_name'];
          }
          $follow[$i]['brand_name']=$brand_name;
          $i++;
        }
      }
      return $follow;
    }


    public function job_inprogress_list(){
      $condition="a.status=4 OR a.status=2 OR a.status=1 ";
      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.mobile as customer_mobile,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,a.followup_date,a.followup_time,a.followup_comments,a.total_cost,a.brand_id,a.reassign_vendor_id,a.reassign_status,a.vendor_id');
      //c.name as vendor_name,c.mobile as vendor_mobile,c.photo,c.id as vendor_id
      $this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      //$this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $this->db->order_by('a.id DESC');
      $query = $this->db->get();
      $followup_list = $query->result_array();
      $follow=array();
      if(count($followup_list)>0){
        $i=0;
        foreach ($followup_list as $key => $value) {
          $follow[$i]['job_service_id']=$value['job_id'];
          $follow[$i]['job_ref']=$value['invoice_ref_no'];

          $follow[$i]['customer_name']=$value['customer_name'];
          $follow[$i]['customer_mobile']=$value['customer_mobile'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $follow[$i]['customer_address']=$add;
          $follow[$i]['commands']=$value['commands'];
          if($value['reassign_status']==1){
              $vendor = $this->db->get_where('vendor',array('id' => $value['reassign_vendor_id']))->result_array();
              $follow[$i]['vendor_name']=$vendor[0]['name'];
              $follow[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $follow[$i]['vendor_photo']=$vendor[0]['photo'];
              $follow[$i]['vendor_id']=$value['reassign_vendor_id'];
             
          }
          else{
              $vendor = $this->db->get_where('vendor',array('id' => $value['vendor_id']))->result_array();
              $follow[$i]['vendor_name']=$vendor[0]['name'];
              $follow[$i]['vendor_mobile']=$vendor[0]['mobile'];
              $follow[$i]['vendor_photo']=$vendor[0]['photo'];
              $follow[$i]['vendor_id']=$value['vendor_id'];
             
          }
          $follow[$i]['vend_id']=$value['vendor_id'];
          /*$follow[$i]['vendor_name']=$value['vendor_name'];
          $follow[$i]['vendor_mobile']=$value['vendor_mobile'];*/
          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
          $status='';
          if($value['job_status']==7){
              $status='Followup';
          }
          $follow[$i]['job_status']=$status;
          $follow[$i]['category_name']=$category_name;
          $req=$this->get_request_service($value['request_id']);
          $follow[$i]['service']=$req[0]['service_type'];
          $follow[$i]['level']=$req[0]['level'];
          $follow[$i]['amount']=$value['total_cost'];
          $follow[$i]['date']=$value['date'];
          $follow[$i]['time']=$value['time'];
          $follow[$i]['status']=$value['job_status'];
          $follow[$i]['category_id']=$value['category_id'];
          $follow[$i]['followup_date']=$value['followup_date'];
          $follow[$i]['followup_time']=$value['followup_time'];
          $follow[$i]['followup_comments']=$value['followup_comments'];
          //$follow[$i]['vendor_photo']=$value['photo'];
          
          $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
          $brand_name='';
          if(count($brand)>0){
            $brand_name=$brand[0]['brand_name'];
          }
          $follow[$i]['brand_name']=$brand_name;
          $follow[$i]['reassign_vendor_id']=$value['reassign_vendor_id'];

          $i++;
        }
      }
      return $follow;
    }



    public function job_assigned_list(){
      /*$condition="a.status!=0 AND a.status!=6";
      $this->db->select('a.service_id,a.level,a.service_name,c.name,c.email,c.address,c.mobile,b.vendor_id,a.id as request_id,a.category_id,a.service_id,a.level,a.status');
      $this->db->from('book_request a');
      $this->db->where($condition);
      $this->db->join('cron_job_cache b', 'b.request_id = a.id', 'left');
      $this->db->join('customer c', 'c.id = a.customer_id', 'left');
      $query = $this->db->get();
      $result = $query->result_array();
      return $result;*/
      $condition="a.status!=0 AND a.status!=6";
      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.mobile as customer_mobile,c.name as vendor_name,c.mobile as vendor_mobile,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,c.photo,c.id as vendor_id,a.category_id,a.followup_date,a.followup_time,a.followup_comments,a.total_cost,a.brand_id,a.reassign_vendor_id,a.reassign_status');
      $this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      $this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $this->db->order_by('a.id DESC');
      $query = $this->db->get();
      $completed_list = $query->result_array();

     
      $complete=array();
      if(count($completed_list)>0){
        $i=0;
        foreach ($completed_list as $key => $value) {
          $complete[$i]['job_service_id']=$value['job_id'];
          $complete[$i]['job_ref']=$value['invoice_ref_no'];

          $complete[$i]['customer_name']=$value['customer_name'];
          $complete[$i]['customer_mobile']=$value['customer_mobile'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $complete[$i]['customer_address']=$add;
          $complete[$i]['commands']=$value['commands'];
          $complete[$i]['vendor_name']=$value['vendor_name'];
          $complete[$i]['vendor_mobile']=$value['vendor_mobile'];
          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
          $status='';
          if($value['job_status']==5){
              $status='Completed';
          }
          $complete[$i]['job_status']=$status;
          $complete[$i]['category_name']=$category_name;

          $req=$this->get_request_service($value['request_id']);
          $complete[$i]['service']=$req[0]['service_type'];
          $complete[$i]['level']=$req[0]['level'];
          $complete[$i]['amount']=$value['total_cost'];
          $complete[$i]['date']=$value['date'];
          $complete[$i]['time']=$value['time'];
          $complete[$i]['status']=$value['job_status'];
          $complete[$i]['vendor_photo']=$value['photo'];
          $complete[$i]['vendor_id']=$value['vendor_id'];
          $complete[$i]['category_id']=$value['category_id'];
          $complete[$i]['followup_date']=$value['followup_date'];
          $complete[$i]['followup_time']=$value['followup_time'];
          $complete[$i]['followup_comments']=$value['followup_comments'];
          $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
          $brand_name='';
          if(count($brand)>0){
            $brand_name=$brand[0]['brand_name'];
          }
          $complete[$i]['brand_name']=$brand_name;
          $i++;
        }
      }// echo'<pre>';print_r($complete);exit;
      return $complete;

    }

     public function getvendor($get_id)
    {
        $this->db->select('*');
        $this->db->from('vendor');
        $condition = "id ='".$get_id ."'";
        $this->db->where($condition);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function report_management($data){

      //$custom_earning=$this->db->where('complete_date  BETWEEN "'.$date_from.'" AND "'.$date_to.'"')->get_where('service',array('vendor_id'=>$id))->result_array();
      
      if($data['export_type']=='completed'){

          $condition='a.status=5 AND complete_date  BETWEEN "'.$data['start_date'].'" AND "'.$data['end_date'].'"';
         
      }
      if($data['export_type']=='followup'){

          $condition='a.status=7 AND complete_date  BETWEEN "'.$data['start_date'].'" AND "'.$data['end_date'].'"';
      }

      if($data['export_type']=='inprogress'){

          $condition='(a.status=4 OR a.status=2 OR a.status=1) AND complete_date  BETWEEN "'.$data['start_date'].'" AND "'.$data['end_date'].'"';
      }

      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.email as customer_email,b.mobile as customer_mobile,c.name as vendor_name,c.mobile as vendor_mobile,c.email as vendor_email,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,c.photo,c.id as vendor_id,a.followup_date,a.followup_time,a.followup_comments,a.total_cost,a.brand_id');
      $this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      $this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $query = $this->db->get();
      $completed_list = $query->result_array();
      //$condition='a.status!=0 AND a.status!=6';
      
   
      $complete=array();
      if(count($completed_list)>0){
        $i=0;
        foreach ($completed_list as $key => $value) {
          $complete[$i]['job_service_id']=$value['job_id'];
          $complete[$i]['job_ref_id']=$value['invoice_ref_no'];

          $complete[$i]['customer_name']=$value['customer_name'];
          $complete[$i]['customer_mobile']=$value['customer_mobile'];
          $complete[$i]['customer_email']=$value['customer_email'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $complete[$i]['service_address']=$add;
          $complete[$i]['commands']=$value['commands'];
          $complete[$i]['vendor_name']=$value['vendor_name'];
          $complete[$i]['vendor_mobile']=$value['vendor_mobile'];
          $complete[$i]['vendor_email']=$value['vendor_email'];

          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
         
          $complete[$i]['category']=$category_name;

          $req=$this->get_request_service($value['request_id']);
          $complete[$i]['service']=$req[0]['service_type'];
          $complete[$i]['level']=$req[0]['level'];
          $complete[$i]['service_cost']=$value['total_cost'];
          $complete[$i]['date']=$value['date'];
          $complete[$i]['time']=$value['time'];
          $complete[$i]['followup_date']=$value['followup_date'];
          $complete[$i]['followup_time']=$value['followup_time'];
          $complete[$i]['followup_comments']=$value['followup_comments'];
          if($value['job_status']==1){
            $status='Accepted';
          }
          else if($value['job_status']==2 || $value['job_status']==4){
            $status='Inprogress';
          }
          else if($value['job_status']==7){
            $status='Followup';
          }
          else if($value['job_status']==5){
            $status='Completed';
          }
          else if($value['job_status']==6 || $value['job_status']==0){
            $status='New';
          }

          $complete[$i]['status']=$status;
          $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
          $brand_name='';
          if(count($brand)>0){
            $brand_name=$brand[0]['brand_name'];
          }

          $complete[$i]['brand_name']=$brand_name;

          $i++;
        }
        
      }
      return $complete;
    }



     public function service_report(){

      $this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.email as customer_email,b.mobile as customer_mobile,c.name as vendor_name,c.mobile as vendor_mobile,c.email as vendor_email,a.status as job_status,b.id as customer_address_id,a.invoice_ref_no,a.category_id,a.request_id,a.amount,a.date,a.time,c.photo,c.id as vendor_id,a.followup_date,a.followup_time,a.followup_comments,a.total_cost');
      //$this->db->where($condition);
      $this->db->from('service a');
      $this->db->join('customer b', 'b.id = a.customer_id', 'left');
      $this->db->join('vendor c', 'c.id = a.vendor_id', 'left');
      $query = $this->db->get();
      $completed_list = $query->result_array();
      $complete=array();
      if(count($completed_list)>0){
        $i=0;
        foreach ($completed_list as $key => $value) {
          $complete[$i]['Job Id']=$value['invoice_ref_no'];
          $req=$this->get_request_service($value['request_id']);
          $complete[$i]['Level']=$req[0]['level'];
          $category = $this->db->get_where('category',array('id' => $value['category_id']))->result_array();
          $category_name=$category[0]['category_name'];
         
          $complete[$i]['Category']=$category_name;
          $complete[$i]['Service']=$req[0]['service_type'];
          

          $complete[$i]['Customer Name']=$value['customer_name'];
          $complete[$i]['Customer Mobile']=$value['customer_mobile'];
          $complete[$i]['Customer Email']=$value['customer_email'];
          $address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
          $add='';
          if(count($address)>0){
            $add=$address[0]['address'];
          }
          $complete[$i]['Service Address']=$add;
          
          $complete[$i]['Vendor Name']=$value['vendor_name'];
          $complete[$i]['Vendor Mobile']=$value['vendor_mobile'];
          $complete[$i]['Vendor Email']=$value['vendor_email'];
          $complete[$i]['Service Cost']=$value['total_cost'];
          $complete[$i]['Comments']=$value['commands'];
          $complete[$i]['Date']=$value['date'];
          $complete[$i]['Time']=$value['time'];
          if($value['job_status']==1){
            $status='Accepted';
          }
          else if($value['job_status']==2 || $value['job_status']==4){
            $status='Inprogress';
          }
          else if($value['job_status']==7){
            $status='Followup';
          }
          else if($value['job_status']==5){
            $status='Completed';
          }
          else if($value['job_status']==6 || $value['job_status']==0){
            $status='New';
          }

          $complete[$i]['Status']=$status;
          $complete[$i]['Followup Date']=$value['followup_date'];
          $complete[$i]['Followup Time']=$value['followup_time'];
          $complete[$i]['Followup Comments']=$value['followup_comments'];
          
          $i++;
        }
      }
      return $complete;
    }


    public function getcategorybrandlist(){
      $brand = $this->db->get_where('category_brand',array('status' => 1))->result_array();
      return $brand;
    }

    public function getbrandlist(){
      $brand = $this->db->get_where('brand',array('status' => 1))->result_array();
      return $brand;
    }

    public function getbrandname($id){
      $brand_names='';
      if($id!=''){
        $brand_name=array();
        $id=explode(',',$id);
        if(count($id)>0){
          foreach ($id as $key => $value) {
            $brand = $this->db->get_where('brand',array('status' => 1,'id'=>$value))->result_array();
            $brand_name[]=$brand[0]['brand_name'];
          }
        }
        $brand_names=implode(',', $brand_name);
        
      }
      
      return $brand_names;
    }

/* End Suba */








} // End Class