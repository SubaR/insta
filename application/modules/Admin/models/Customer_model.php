<?php

class Customer_model extends CI_Model
{

	/* Customer List */
	public function customer_list(){

		$this->db->select('*');
        $this->db->from('customer');
        $this->db->order_by("id","desc");
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
  
	}

	/* Customer Creation */
	public function customer_creation($data){
		$condition = "email ='". $data['email_id']. "' OR  mobile='".$data['mobile']."'";
	    $this->db->select("*");
	    $this->db->from("customer");
	    $this->db->where($condition);
	    $query = $this->db->get();
	    $res = $query->result_array();
	   	$cur_time = date('Y-m-d H:i:s');
	    if(count($res)>0){

	    	if($res[0]['email']==$data['email_id']){
	    		return 1;
	    	}
	    	else if($res[0]['mobile']==$data['mobile']){
	    		return 2;
	    	}
	    	else{//,'password'=>md5($data['password']),'hash'=>$data['password']
	    		$insert_array=array('name'=>$data['customer_name'],'email'=>$data['email_id'],'mobile'=>$data['mobile'],'address'=>$data['address'],'status'=>1,'token'=>md5($cur_time),'country_code'=>91);
				$customer = $this->db->insert('customer',$insert_array );
				return 3;
	    	}
	    }
	    else{

	    	$insert_array=array('name'=>$data['customer_name'],'email'=>$data['email_id'],'mobile'=>$data['mobile'],'address'=>$data['address'],'status'=>1,'token'=>md5($cur_time),'country_code'=>91);
			$customer = $this->db->insert('customer',$insert_array );
			return 3;
	    }

	}


	/* Delete Customer */

	public function delete_customer($data){

		$delcon = "id ='" . $data['customer_id'] . "'  ";
		$this->db->where($delcon);
		$this->db->delete('customer');
		return 1;

	}


	/* Customer Updation */
	public function customer_updation($data){
		
		if($data['customer_name'] ==''){
			return 1;
		}
		else if($data['address'] ==''){
			return 2;
		}
		else{
			$condition = "id ='" . $data['customer_id'] . "'  ";
		    $datas = array('name' => $data['customer_name'], 'address' => $data['address']);
		    $this->db->where($condition);
		    $update = $this->db->update('customer', $datas );
		    if($update==1){
		    	return 3;
		    }
		    else{
		    	return 0;
		    }

		}
		
	}

 
}
?>