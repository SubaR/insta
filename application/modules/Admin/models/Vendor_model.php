<?php
class Vendor_model extends CI_Model
{
	private $timezone = 'Asia/Kolkata';
	public function getallvendors()
	{
		    $this->db->select('*');
			$this->db->from('vendor');
			$query = $this->db->get();
			$res=$query->result_array();
			return $res;
	}

	public function getallcategories(){
		  $this->db->select('*');
			$this->db->from('category');
			$query = $this->db->get();
			$res=$query->result_array();
			return $res;
	}

	public function checkemail_mobile($data,$type){
            $this->db->select('*');
			$this->db->from('vendor');
			if($type=='mobile'){
			   $this->db->where('mobile',$data['mobile']);
			}
			else{
				$this->db->where('email',$data['email_id']);
			}
			$query = $this->db->get();
			$res=$query->result_array();
			if(count($res)!=0){
                if($type=='mobile'){
                	return 2;
                }else{
                   return 1;
                }
			}
			else{
				return 3;
			}
			
	}

	public function create_vendor($data){

        $email_check=$this->checkemail_mobile($data,'email');
        $mobile_check=$this->checkemail_mobile($data,'mobile');
        
        if($email_check==3 && $mobile_check==3){
        	$service_type=implode(',',$data['service_type']);
        	$category=implode(',', $data['selectcat']);
        	$vendor_data = array(
		 	    'name' => $data['customer_name'],
			    'email' => $data['email_id'],
				'mobile' => $data['mobile'],
				'address' => $data['address'],
				/*'password' => md5($data['password']),*/
				'status' => 1,
				/*'token' => md5($data['email_id'].$data['mobile']),*/
				'service_type' => $service_type,
				'category' => $category,
				'country_code' =>91
				);
		    $this->db->insert('vendor', $vendor_data);

		   return 3;
        }
        else if($mobile_check==2){
        	return $mobile_check;
        }
       else if($email_check==1){
            return $email_check;
        }
		
	}

	public function update_vendor($data){
		
		if(isset($data['service_type']) && $data['service_type']!=''){
			$service_type=implode(',',$data['service_type']);
		}
		else{
			$service_type='';
		}
		
		if($data['customer_name'] ==''){
			return 1;
		}
		else if($data['address'] ==''){
			return 2;
		}
		else if($service_type ==''){
			return 4;
		}
		else if( $data['selectcat'] ==''){
			return 5;
		}
		else{
			//print_r($data);exit;
			$service_type=implode(',',$data['service_type']);
			$category=implode(',', $data['selectcat']);
	        $vendor_data = array(
			 	'name' => $data['customer_name'],
			    'email' => $data['email_id'],
				'mobile' => $data['mobile'],
				'address' => $data['address'],
				/*'token' => md5($data['email_id'].$data['mobile']),*/
				'service_type' => $service_type,
				'category' => $category
				);
	        $this->db->where('id',$data['vendor_id']);
			$update=$this->db->update('vendor', $vendor_data);
			if($update==1){
		    	return 3;
		    }
		    else{
				return 0;
			}
		}
	}

	public function delete_vendor($data){
        $this->db->where('id',$data['customer_id']);
        $this->db->delete('vendor');
        return 1;
	}


	public function send_request($data){
		
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $cur_date=$date->format('Y-m-d'); 
        $cur_time=$date->format('H:i:s');
//print_r($data['vendor_ids']);exit;
		if(count($data['vendor_ids'])>0){
			//print_r($data['vendor_ids']);exit;
			foreach ($data['vendor_ids'] as $key => $value) {
				
				/*$result = $this->db->get_where('book_request', array('status' => 0, 'id' => $data['book_id']))->result_array();*/
				$condition="(status=0 OR reassign_status=1) AND id='".$data['book_id']."'";
				$this->db->select('*');
				$this->db->from('book_request');
				$this->db->where($condition);
				$query = $this->db->get();
				$result=$query->result_array();
				//print_r($result);exit;
				if(count($result)>0){

					$catche_array=array('customer_id'=>$result[0]['customer_id'],'vendor_id'=>$value,'request_id'=>$data['book_id'],'date'=>$cur_date,'time'=>$cur_time,'status'=>0);
					$catche = $this->db->insert('cron_job_cache',$catche_array);

					$status = array('status' => 6 ,'level'=>$data['level']);
			        $this->db->where('id',$data['book_id']);
					$update=$this->db->update('book_request', $status);

					$vendor=$this->db->get_where('vendor', array('id' => $value))->result_array();
					if(count($vendor)>0){

						if($vendor[0]['firebase_token']!=''){

							$this->load->library('Firebase');
						    $this->load->library('Push');
						    $firebase = new Firebase();
						    $push = new Push();   
						    $this->load->helper('date');
						    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
						    $cur_time = $date->format('d-m-Y');
						    $not_time = $date->format('H:i:s');
						    
						    $url = 'https://fcm.googleapis.com/fcm/send';
						    $priority="high";

						    $customer=$this->db->get_where('customer', array('id' => $result[0]['customer_id']))->result_array();
						    $customer_name='';
						    if(count($customer)>0){
						    	$customer_name=$customer[0]['name'];
						    }

						    $title = 'Service Request Received';
							$title1='You have received a new service request from '.$customer_name.' ';
							$type='service_request';
							$type1='vendor';

							$name=$vendor[0]['name'];

							$registrationIds=$vendor[0]['firebase_token'];

							$send_id=$result[0]['customer_id'];
						    $rec_id=$value;

						    $notification= array('title' => $title,'message' => 'Hi '.$name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

						    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
					   
						    //print_r($registrationIds);
						    $payload = array();
						    $message = $notification['message'];
						    $push_type = isset($data['push_type']) ? $data['push_type'] : '';
						    $push->setTitle($title);
						    $push->setMessage($message);
						    $push->setType($type);
						    $push->setId($data['book_id']);
						    $push->setImage("");
						    $push->setIsBackground(FALSE);
						    $push->setPayload($payload);
						    $push_time = $date->format('Y-m-d G:i:s');
						    //$push->setTime($push_time);
						    $json = '';
						    $response = '';
						    $json = $push->getPush();
						    $response = $firebase->send($registrationIds, $json);
						 // print_r('aaaaa');print_r($response);
						    if($response){
					            $insert = $this->db->insert('notification',$notification_array); 
					        }

						}

					}

				}
			}

		}
	}

    
}
