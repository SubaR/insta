<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('string');
        $this->load->helper(array('form', 'url')); 
        $this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->model('Vendor_model');
		$this->load->model('Customer_model');
		$this->load->model('Category_model');
	}

	/* Boopathi */

	/* Authentication */
	public function index()
	{ 
 		 $data['Activebar']="dashboard";
	     $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','dashboard',$data);
		 $this->template->render();
	}
	public function login()
	{
 		 $data['Activebar']="login";
	     $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/login_templete.php');
		 $this->template->write_view('content','login',$data);
		 $this->template->render();
	}
	public function logout()
	{
 		 $this->session->flashdata('admin');
 		 $this->session->sess_destroy();
         redirect('/Admin/login', 'refresh');
	}
	public function login_action(){
		    $post=$this->input->post();
            $login = $this->Admin_model->login($post);
            if($login!=0){
            	$this->session->set_userdata('admin', $login);
            	$this->session->set_flashdata('success', 'Welcome To Instahome Admin Portal');
            	redirect('/Admin', 'refresh');
            }
            else{
                $this->session->set_flashdata('error', 'Email Id and Password Wrong. Please try Again');
            	redirect('/Admin/login', 'refresh');
            }
	}
	public function profile(){
		$data['Activebar']="dashboard";
        $data['users'] = $this->Admin_model->profile();
        $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','profile',$data);
		 $this->template->render();
	}
	public function updateprofile(){
		$post=$this->input->post();
        $data['users'] = $this->Admin_model->updateprofile($post);
        $this->session->set_flashdata('success', 'Successfully Update Profile');
        redirect('/Admin/profile', 'refresh');
	}
	public function changepassword(){
		$post=$this->input->post();
		if($post['password']==$post['cpassword']){
			$data['users'] = $this->Admin_model->changepassword($post);
	        $this->session->set_flashdata('success', 'Successfully Change Password');
	        redirect('/Admin/profile', 'refresh');
		}
		else{
			$this->session->set_flashdata('error', 'Please Enter Password are not Same');
	        redirect('/Admin/profile', 'refresh');
		}
        
	}
	/* End of Authentication */

	/* vendor functionality */
    public function vendor(){
    	$data['Activebar']="vendor";
        $data['vendors'] = $this->Vendor_model->getallvendors();
        $data['categories'] = $this->Vendor_model->getallcategories();
		$data['fetch_data']=$this->Category_model->viewcategorylist();
		$data['subcategory_fetch_data']=$this->Category_model->viewsubcategorylist();
		$data['servicetypes']=$this->Category_model->viewservicelist();
        $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','vendor',$data);
		 $this->template->render();
    }

    public function create_vendor(){
    	$post=$this->input->post();
    	$res=$this->Vendor_model->create_vendor($post);
    	if($res==3){
    		$this->session->set_flashdata('success', "Vendor Created Successfully.");
    	}
    	else if($res==2){
    		$this->session->set_flashdata('error', "Mobile Number Already Registered");
    	}
    	else if($res==1){
    		$this->session->set_flashdata('error', "Email Already Registered");
    	}
    	echo $res;
        
    }

    public function delete_vendor(){
    	$post=$this->input->post();
    	echo $this->Vendor_model->delete_vendor($post);

    }

    public function update_vendor(){
    	$post=$this->input->post();
    	echo $this->Vendor_model->update_vendor($post);
        
    }

    
	/* end of vendor functionality */

	/* end Boopathi */

	//Suba Starts Work
	
/* Customer List*/

	public function customer()
	{
		$data['Activebar']="customer";
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$data['customer']=$this->Customer_model->customer_list();
		$this->template->write_view('content','customer',$data);
		$this->template->render();
	}


/* Customer Creation*/

	public function create_customer(){
		$data=$this->input->post(); 
		$result=$this->Customer_model->customer_creation($data);
		echo $result;
		
	}

/* Delete Customer */

	public function delete_customer(){
		$input=$this->input->post();
		$result=$this->Customer_model->delete_customer($input);
		echo $result;
	}

/* Customer Updation*/

	public function update_customer(){
		$data=$this->input->post(); 
		$result=$this->Customer_model->customer_updation($data);
		echo $result;
		
	}


	public function servicelist()
	{
		 
 		 $data['Activebar']="servicetype";
 		 $data['fetchlist']=$this->Category_model->viewservicelist();
	     $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','servicetype',$data);
		 $this->template->render();
	}

	public function addservicetype()
	{
		 
 		$input=$this->input->post();
 		$result=$this->Category_model->addservicetype($input);
		if($result==1){
			 $this->session->set_flashdata('success', 'Service Type Added Successfully');
			echo '1';
		}else if($result==2){
			echo '2';
	    }else if($result=='RepeatData'){
	    	echo 'RepeatData';
	    }  
	}


	public function updateservicetype()
	{
		 
 		$input=$this->input->post();
	   	$result=$this->Category_model->updateservicetype($input);
		if($result==1){
		 $this->session->set_flashdata('success', 'Service Type Updated Successfully');
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}

	public function deleteservicetype()
	{
		 
 		$input=$this->input->post();
	   	$result=$this->Category_model->deleteservicetype($input);
		if($result==1){
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}


	public function service_request(){
		$data['Activebar']="service_request";
		$data['book_list']=$this->Category_model->service_request_list();
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','booking_request',$data);
		$this->template->render();
	}

	public function send_request(){
		$input=$this->input->post();
		$result=$this->Vendor_model->send_request($input);
		if($result){
 			$this->session->set_flashdata('success', "Service Request Send Successfully.");
 		}
		echo 1;
	}


	public function job_list($id=''){
		//print_r($id);exit;
		$data['Activebar']="job_completed_list";
		$type=base64_decode(base64_decode($id));
		if($type==1){
			$data['type']='followup';
			$data['job_list']=$this->Category_model->job_followup_list();
			$data['followup_list']=$this->Category_model->job_followup_list();
			$data['completed_list']=$this->Category_model->job_completed_list();
			$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
			$this->template->write_view('content','job_list',$data);
			$this->template->render();
		}
		else if($type==2){
			$data['type']='completed';
			$data['job_list']=$this->Category_model->job_completed_list();
			$data['followup_list']=$this->Category_model->job_followup_list();
			$data['completed_list']=$this->Category_model->job_completed_list();
			$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
			$this->template->write_view('content','job_list',$data);
			$this->template->render();
		}

		
	}

	public function notification_read()
	{
		$result = $this->Admin_model->get_all_notification_admin_read();
		redirect($this->config->item('base_url').'Admin/service_request');
		
	}


	public function chat()
	{
		$data['Activebar']="chat";
		$data['vendor']=$this->Admin_model->get_vendor_counts();
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','chat',$data);
		$this->template->render();
		
	}

	public function insertchat(){
		$data=$this->input->post();
		
		$result = $this->Admin_model->insertchat($data);
		//exit;
		$msg_count=$this->Admin_model->get_all_messages_count($data['receiver_id']);
		$count=count($msg_count);

		$admin = $this->Admin_model->admin($result[0]['sender_id']);
		$time=date('h:i A',strtotime($result[0]['date_time']));
		$date=$result[0]['date'];
		$arr = array('sender_name' => $admin[0]['name'], 'image' => "", 'message' => $result[0]['message'], 'time' => $time,'count'=>$count,'dates'=>$date);
		echo json_encode($arr);
	}

	public function getmessage(){
		$data=$this->input->post();
		$passdata = $this->Admin_model->get_msg($data);
		//print_r($passdata);exit;
		echo $this->load->view('chat_ajax', $passdata);
	}
	public function getmessages(){
		$data=$this->input->post();
		$passdata = $this->Admin_model->get_msg($data);
		echo $this->load->view('chat_ajax1', $passdata);
	}

	public function getmessage_count(){
		$data=$this->input->post();
		$passdata = $this->Admin_model->get_msg_counts($data);
		echo $passdata;
	}


	public function assign_list($id=''){
		//print_r($id);exit;
		$data['Activebar']="job_assign";

		$type=base64_decode(base64_decode($id));
		if($type==1){
			$data['type']='followup';
			$data['job_list']=$this->Category_model->job_followup_list();
			$data['followup_list']=$this->Category_model->job_followup_list();
			$data['completed_list']=$this->Category_model->job_completed_list();
			$data['inprogress_list']=$this->Category_model->job_inprogress_list();
			$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
			$this->template->write_view('content','assign_list',$data);
			$this->template->render();
		}
		else if($type==2){
			$data['type']='completed';
			$data['job_list']=$this->Category_model->job_completed_list();
			$data['followup_list']=$this->Category_model->job_followup_list();
			$data['completed_list']=$this->Category_model->job_completed_list();
			$data['inprogress_list']=$this->Category_model->job_inprogress_list();
			$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
			$this->template->write_view('content','assign_list',$data);
			$this->template->render();
		}
		else if($type==3){
			$data['type']='inprogress';
			$data['job_list']=$this->Category_model->job_inprogress_list();
			$data['followup_list']=$this->Category_model->job_followup_list();
			$data['completed_list']=$this->Category_model->job_completed_list();
			$data['inprogress_list']=$this->Category_model->job_inprogress_list();
			$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
			$this->template->write_view('content','assign_list',$data);
			$this->template->render();
		}




		//$data['job_list']=$this->Category_model->job_assigned_list();
		/*$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','assign_list',$data);
		$this->template->render();*/
		
	}

	public function service_export(){
		
		$data=$this->input->post();

		
        $fileName = 'data-'.time().'.xlsx';  
		// load excel library
        $this->load->library('Excel');
        	
        $empInfo = $this->Category_model->report_management($data);

        // print_r($empInfo);exit;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
		
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Job ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Level');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Category');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Service');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Brand Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Customer Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Customer Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Customer Mobile');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Service Address');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Vendor Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Vendor Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Vendor Mobile');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Service Cost');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Comments');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Status');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Followup Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Followup Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'Followup Comments');

     
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {

        	if(!isset($element['job_ref_id']))
        		$element['job_ref_id']='';
        	if(!isset($element['level']))
        		$element['level']='';
            if(!isset($element['category']))
        		$element['category']='';
        	if(!isset($element['service']))
        		$element['service']='';
        	if(!isset($element['brand_name']))
        		$element['brand_name']='';
        	if(!isset($element['customer_name']))
        		$element['customer_name']='';
        	if(!isset($element['customer_email']))
        		$element['customer_email']='';
        	if(!isset($element['customer_mobile']))
        		$element['customer_mobile']='';
        	if(!isset($element['service_address']))
        		$element['service_address']='';
        	if(!isset($element['vendor_name']))
        		$element['vendor_name']='';
        	if(!isset($element['vendor_email']))
        		$element['vendor_email']='';
        	if(!isset($element['vendor_mobile']))
        		$element['vendor_mobile']='';
        	if(!isset($element['service_cost']))
        		$element['service_cost']='';
        	if(!isset($element['commands']))
        		$element['commands']='';
        	if(!isset($element['date']))
        		$element['date']='';
        	if(!isset($element['time']))
        		$element['time']='';
        	if(!isset($element['status']))
        		$element['status']='';
        	if(!isset($element['followup_date']))
        		$element['followup_date']='';
        	if(!isset($element['followup_time']))
        		$element['followup_time']='';
        	if(!isset($element['followup_comments']))
        		$element['followup_comments']='';


            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['job_ref_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['level']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['category']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['service']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['brand_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['customer_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['customer_email']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['customer_mobile']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['service_address']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['vendor_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['vendor_email']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['vendor_mobile']);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['service_cost']);
			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['commands']);
			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['date']);
			$objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['time']);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $element['status']);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['followup_date']);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $element['followup_time']);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $element['followup_comments']);
        
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        header('Content-type: application/vnd.ms-excel');
		// It will be called file.xls
		header('Content-Disposition: attachment; filename='.$fileName);
		// Write file to the browser
		// local download file
		$objWriter->save('php://output');
        //$objWriter->save($_SERVER["DOCUMENT_ROOT"].'/khaika/tmp/'.$fileName);
       // $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/tmp/'.$fileName);
		// download file
        //header("Content-Type: application/vnd.ms-excel");
       // redirect($this->config->item('base_url').'tmp/'.$fileName);      
	}


	public function service_exports()
	{
		$this->load->library('Excel');
		$this->load->helper('string');
       
		//$file_name='customer'.$otp;
		$file_name = 'data-'.time();  
		$data= $this->Category_model->service_report();
		//print_r($data);exit;
		$this->excel->setActiveSheetIndex(0);
	    //$res=iconv("charset=utf-16", "CP1252", $data);
        $this->excel->stream($file_name.'.xls', $data);
        header("location: " . $this->config->item('base_url') . "tmp/".$file_name.".xls");
	}


	public function offer()
	{
		 
		$data['Activebar']="offer";
		$data['product_cat']=$this->Category_model->viewcategorylist();
		$data['offerlist']=$this->Admin_model->getofferlist();
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','offer',$data);
		$this->template->render();
	}

	public function addoffer()
	{
		 
 		$input=$this->input->post();
 		$result=$this->Admin_model->addoffer($input);
 		if($result==1){
 			$this->session->set_flashdata('success', "Offer Added Successfully.");
 		}
 		else{
 			$this->session->set_flashdata('error', "Offer Added Failed.");
 		}
 		echo $result;
		
	}

	public function update_offer(){
		$input=$this->input->post();
 		$result=$this->Admin_model->updateoffer($input);
 		if($result==6){
 			$this->session->set_flashdata('success', "Offer Updated Successfully.");
 		}
 		else{
 			$this->session->set_flashdata('error', "Offer Updated Failed.");
 		}
 		echo $result;
		
	}
	public function deleteoffer(){
    	$post=$this->input->post();
    	echo $this->Admin_model->deleteoffer($post);

    }

     public function view_invoice($id=''){
    	$data['Activebar']="job_assign";
    	$id=base64_decode(base64_decode($id));
    	$data['service']=$this->Category_model->get_service_details($id);
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','invoice',$data);
		$this->template->render();
    }


    public function brand(){
		$data['Activebar']="brand";
		$data['fetch_data']=$this->Category_model->viewcategorylist();
		$data['brandlist']=$this->Category_model->getbrandlist();
		$data['categorybrandlist']=$this->Category_model->getcategorybrandlist();		 
		$this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		$this->template->write_view('content','brand',$data);
		$this->template->render();
    }

    public function addbrand()
	{
 		$input=$this->input->post();
 		$result=$this->Admin_model->addbrand($input);
 		if($result==1){
 			$this->session->set_flashdata('success', "Brand Added Successfully.");
 		}
 		echo $result;
	}

	public function updatebrand()
	{
 		$input=$this->input->post();
 		$result=$this->Admin_model->updatebrand($input);
 		if($result==3){
 			$this->session->set_flashdata('success', "Brand Updated Successfully.");
 		}
 		echo $result;
	}

	public function deletebrand()
	{
 		$input=$this->input->post();
 		echo $result=$this->Admin_model->deletebrand($input);
	}

	public function getnewbooking(){
		$result=$this->Admin_model->getnewbooking();
		if(count($result)>0){
			$admin=$this->Admin_model->getsuperadmin();
			$admin_name=$admin[0]['username'];
			$name = $result[0]['name'];
			$image = $this->config->item('base_url').'uploads/user.png';
			$arr = array('name' => $name, 'image' => $image, 'admin_name' => $admin_name,'booking_id' => $result[0]['booking_id']);
			//print_r($arr);exit;
			echo json_encode($arr);
		}
		
	}

	public function updatenotifystatus(){
		$input=$this->input->post();
		$result=$this->Admin_model->updatenotifystatus($input);
		echo $result;
	}


//Suba End Work

// Jeeva work
/*
|---------------------------------------------------------------------------------------------------------
|                     Main Category 
|---------------------------------------------------------------------------------------------------------
*/


	public function category()
	{
		 
 		 $data['Activebar']="category";
 		 $data['fetch_data']=$this->Category_model->viewcategorylist();
		 
	     $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','category',$data);
		 $this->template->render();
	}

	public function addcategory()
	{
		 
 		$input=$this->input->post();
		$result=$this->Category_model->addcategory($input);
		if($result==1){
			$session_alert = array('toastr_value' => "1");	   
		    $this->session->set_userdata('tostartnotify',$session_alert);
		    echo '1';
		}else if($result==2){
			$session_alert = array('toastr_value' => "2");	   
		    $this->session->set_userdata('tostartnotify',$session_alert);
		      echo '2';
		}
		 //   redirect($this->config->item('base_url').'Admin/category');    
	}


	public function updatecategory()
	{
		 
 		$input=$this->input->post();
 	  	$result=$this->Category_model->updatecategory($input);
		if($result==1){
			$session_alert = array('toastr_value' => "11");	
			 $this->session->set_userdata('tostartnotify',$session_alert);
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}


	public function delete_category()
	{
 		$input=$this->input->post();
	   	$result=$this->Category_model->delete_category($input);
		if($result==1){
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}


/*
|---------------------------------------------------------------------------------------------------------
|                     Sub Category 
|---------------------------------------------------------------------------------------------------------
*/

	public function subcategory()
	{
		 
 		 $data['Activebar']="subcategory";
 		 $data['fetch_data']=$this->Category_model->viewcategorylist();
 		 $data['subcategory_fetch_data']=$this->Category_model->viewsubcategorylist();
 		 $data['servicetypes']=$this->Category_model->viewservicelist();		 
	     $this->template->set_master_template('../../themes/'.$this->config->item("active_template").'/dashboard_templete.php');
		 $this->template->write_view('content','subcategory',$data);
		 $this->template->render();
	}
	

   public function addsubcategory()
	{
		 
 		$input=$this->input->post();
 		$result=$this->Category_model->addsubcategory($input);
		if($result==1){
			$session_alert = array('toastr_value' => "1");	
			$this->session->set_userdata('tostartnotify',$session_alert);
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
	}


	public function updatesubcategory()
	{
		 
 		$input=$this->input->post();
	   	$result=$this->Category_model->updatesubcategory($input);
		if($result==1){
			 $session_alert = array('toastr_value' => "11");	
			 $this->session->set_userdata('tostartnotify',$session_alert);
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}

    public function delete_subcategory()
	{
		 
 		$input=$this->input->post();
	   	$result=$this->Category_model->delete_subcategory($input);
		if($result==1){
			echo '1';
		}else if($result==2){
			echo '2';
	    }  
		       
	}

	






}
