<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';
//echo $this->config->item('base_url').'uploads/1.png';
?> 
<style type="text/css">
   [type=checkbox]:checked, [type=checkbox]:not(:checked){
    position: static;
    opacity: 1;
   }

.outer
{
    width:100%;
    /*text-align: center;*/
}
.inner
{
    display: inline-block;
}
/*.size_vendor{
    width: 351px !important;
}*/
</style>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Service Request Lists</h2>
                
            </div> 
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">

                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <!-- <th><input type="checkbox" class="checkAll" name=""></th> -->
                                    <th>Assign to Vendors</th>
                                    <th>Job Id</th>
                                    <th>Customer Name</th>
                                    <th>Product Category</th>
                                    <th>Brand Name</th>
                                    <th>Service</th>
                                    <th>Assign Date</th>
                                    <th>Reassign Date</th>
                                    <th>EmailId</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th>Comments</th>
                                    
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                           <!--  <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Job Id</th>
                                    <th>Customer Name</th>
                                    <th>Product Category</th>
                                    <th>Brand Name</th>
                                    <th>Service</th>
                                    <th>Assign Date</th>
                                    <th>Reassign Date</th>
                                    <th>EmailId</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th>Comments</th>
                                </tr>
                            </tfoot> -->
                            <tbody>
                                <?php 
                                if(count($book_list)>0){
                                    foreach ($book_list as $key => $value) {
                                        if($value['book_status']!=5){
                                            $request = $this->db->get_where('service', array('request_id' => $value['booking_id']))->result_array();
                                        //echo '<pre>';print_r($value);
                                ?>
                                <tr <?php if($value['reassign_status']==1){?> style="background-color: aliceblue;" <?php } ?>>
                                    <!-- <td> <input type="checkbox" name="mail[]" value="<?= $value['id'];?>"> </td> -->
                                    <td >
                                        <div class="outer" style="float: left;width: 400px;">

                                        <div class="inner">
                                            <select id="level<?=$value['booking_id']?>" class="form-control"
                                                <?php if(isset($value['level']) && $value['level']!=''){?>  <?php }?>
                                             >
                                                <option value="Low" <?php if(isset($value['level']) && $value['level']=='Low'){?> selected <?php }?>>Low</option>
                                                <option value="Medium"  <?php if(isset($value['level']) && $value['level']=='Medium'){?> selected <?php }?>>Medium</option>
                                                <option value="High"  <?php if(isset($value['level']) && $value['level']=='High'){?> selected <?php }?>>High</option>
                                            </select>
                                        </div>

                                        <div class="inner">
                                        <?php 
                                        $cron_vendor_id='';
                                        $cron=$this->Category_model->get_cron_vendor($value['booking_id']);
                                        //print_r($cron);
                                        if(count($cron)>0){
                                            $cron_vendor_id=$cron[0]['vendor_id'];
                                        }

                                        $vendors=$this->Category_model->get_service_based_vendors($value['category_id']);


                                     
                                        ?>
                                        <?php if($cron_vendor_id=='' ){?>
                                        <select id="chkval<?=$value['booking_id']?>" class="chkval" multiple="multiple">
                                            <?php if(count($vendors)){
                                                foreach ($vendors as $key1 => $value1) {
                                                    $services=explode(',',$value1['service_type']);
                                                    if(in_array($value['service_name'], $services)){?>
                                                        <option value="<?=$value1['id']?>"><?=$value1['name']?></option>
                                                    <?php }
                                                   
                                                }

                                            }else{?>
                                                <option value="">No Vendors</option>
                                            <?php }?>
                                            
                                        </select> 

                                    <?php } else{

                                        if($value['reassign_status']==1){?>

                                        <select id="chkval<?=$value['booking_id']?>" class="chkval" multiple="multiple" >
                                            <?php if(count($vendors)){

                                                $reassign=$this->Category_model->get_service_request_vendor($value['booking_id']);
                                                $reassign_vendor='';
                                                if(count($reassign)>0){
                                                    $reassign_vendor=$reassign[0]['vendor_id'];
                                                }
                                                $reassign_vendor_id='';
                                                if(count($request)>0){
                                                    $reassign_vendor_id=$request[0]['reassign_vendor_id'];
                                                }

                                                foreach ($vendors as $key1 => $value1) {
                                                    if($value1['id']!=$reassign_vendor){
                                                   
                                                    ?>
                                                        
                                                        <option value="<?=$value1['id']?>" <?php if($value1['id']==$reassign_vendor_id){?> selected <?php } ?> ><?=$value1['name']?></option>
                                                   
                                               <?php  } }

                                            }else{?>
                                                <option value="">No Vendors</option>
                                            <?php }?>
                                            
                                        </select> 


                                        <?php }else{?>

                                         <select id="chkval<?=$value['booking_id']?>" class="chkval" multiple="multiple" disabled>
                                            <?php if(count($vendors)){
                                                foreach ($vendors as $key1 => $value1) {?>
                                                   
                                                        <option value="<?=$value1['id']?>" <?php if($value1['id']==$cron_vendor_id){?> selected <?php }?>  ><?=$value1['name']?></option>
                                                   
                                               <?php  }

                                            }else{?>
                                                <option value="">No Vendors</option>
                                            <?php }?>
                                            
                                        </select> 


                                    <?php }}?>
                                    </div>
                                    
                                    <div class="inner">
                                        <?php if($value['book_status']==0 || $value['book_status']==7){?>
                                         <button id="request" onclick="send_request('<?=$value['booking_id']?>');" class="btn btn-primary">Send Request</button>
                                     <?php } else{?> <button type="button" class="btn btn-success" disabled="">Already Sent</button>  <?php }
                                     ?>
                                 </div>
                                 </div>
                                    </td>
                                     <td><?php echo 'JOB#'.$value['booking_id'];?></td>
                                    <td><?php echo $value['name'];?></td>
                                     <?php $category_name=$this->Category_model->getcategory($value['category_id']);
                                        $categoryname='';
                                        if(count($category_name)>0){
                                            $categoryname=$category_name[0]['category_name'];
                                        }

                                        $service_name=$this->Category_model->getservice($value['service_id']);

                                        $servicename='';
                                        if(count($service_name)>0){
                                            $servicename=$service_name[0]['service_type'];
                                        }
                                       
                                    ?>
                                    <td><?php echo $categoryname;?></td>
                                    <td><?php $brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
                                      $brand_name='';
                                      if(count($brand)>0){
                                        $brand_name=$brand[0]['brand_name'];
                                      }?>
                                      <?php echo $brand_name;?>
                                    </td>
                                    <td><?php echo $servicename;?></td>
                                    <td><?=$value['date'];?></td>
                                    <td><?php 
                                    $reassign_date='';$service_cost='';
                                    if(count($request)>0){
                                        $reassign_date=$request[0]['followup_date'];
                                        //$service_cost=$request[0]['total_cost'];
                                    }
                                    ?><?=$reassign_date;?></td>
                                    
                                    <td><?php echo $value['email'];?></td>
                                    <?php //style="white-space:pre-wrap; word-wrap:break-word"?>

                                    <td ><?php echo $value['address'];?></td>
                                    <td><?php echo $value['mobile'];?></td>
                                    <td><?php echo $value['commands'];?></td>
                                   

                                    <!-- <td class="actions"><a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut<?=$value['id']?>"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="Remove" data-id="<?php echo $value['id']?>"><i class="icon-trash" aria-hidden="true"></i> </a> | <a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut-editcustomer<?=$value['id']?>"><span class="fa fa-pencil"></span></a></td> -->
                                  
                                </tr>
                                <script type="text/javascript">
                                    <?php if($value['reassign_status']==1){
                                        $val='Reassign to';
                                    }else{
                                        $val='Assign to';
                                    }
                                        ?> 
                                    var val='<?php echo $val;?>'; 
                                    $('.chkval').multiselect({
                                        includeSelectAllOption: true,
                                        nonSelectedText:val
                                    });
                                </script>
                                
                               <?php } } }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">

    $(function() {

        $('.select2').select2({
            minimumInputLength:1
        });
       
    });


   $(document).ready(function () {
      $('.checkAll').on('click', function () {//alert('aaaaa');
        $(this).closest('table').find('tbody :checkbox')
          .prop('checked', this.checked)
          .closest('tr').toggleClass('selected', this.checked);
           $("#Send_Mail").toggle(this.checked);
         /* $('#Send_Mail').css('display','block');
          alert($(".checkAll").toggle(this.checked));*/
      });

      $('tbody :checkbox').on('click', function () {
        $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row
      
        $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
        
      });


});


var url1='<?php echo $url;?>';
function send_request(book_id){
    var check=$('#chkval'+book_id).val();//alert(id);alert(check);
    var level=$('#level'+book_id).val();
    if(check!=''){
         $.ajax({
            type: "POST",
            url: url1+"/send_request",
            data: { vendor_ids:check,book_id:book_id,level:level }, 
            success: function(data)
            {//alert(data);
                if(data==1){
                    //swal("Service Request Send Successfully", "success");
                    location.reload();
                }
            }
        });
    }
    else{
        swal({   title: "Please Select Service"  });
        $('#chkval'+book_id).addClass("highlight");
        return false;
    }
   
}   
    
$('.dataTable').DataTable({
    "ordering": false
});
    </script>



  