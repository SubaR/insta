<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
?>

<script>
    // notification popup
    toastr.options.closeButton = true;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.showDuration = 1000;

    <?php if($this->session->flashdata('error')){ ?>
      toastr['error']('<?=$this->session->flashdata("error");?>');
    <?php } elseif ($this->session->flashdata('success')) { ?>
        toastr['success']('<?=$this->session->flashdata("success");?>');
    <?php } elseif ($this->session->flashdata('info')) { ?>
        toastr['info']('<?=$this->session->flashdata("info");?>');
   <?php  } elseif ($this->session->flashdata('warning')) { ?>
        toastr['warning']('<?=$this->session->flashdata("warning");?>');
    <?php } ?>

</script>
