<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 

    $customer=$this->Admin_model->get_customer_count();
    $customer_count=count($customer);

    $vendor=$this->Admin_model->get_vendor_count();
    $vendor_count=count($vendor);

    $jobs=$this->Admin_model->get_completed_jobs_count();
    $jobs_count=count($jobs);

    $category=$this->Admin_model->get_category_count();
    $category_count=count($category);

    $service=$this->Admin_model->get_service_count();
    $service_count=count($service);

    $service_req=$this->Admin_model->get_service_req_count();
    $service_req_cnt=count($service_req);

    $inprogress=$this->Admin_model->get_service_inprogress_count();
    $inprogress_cnt=count($inprogress);

    $today_earning=$this->Admin_model->get_today_earning();
  

?>

   <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Dashboard</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>            
                  
                </div>
            </div>

            <div class="row clearfix">
               
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/customer" >
                                <div class="number">
                                    <h6>CUSTOMER</h6>
                                    <span><?=$customer_count;?></span>
                                </div>
                            </a>
                           
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/vendor" >
                                <div class="number">
                                    <h6>VENDOR</h6>
                                    <span><?=$vendor_count;?></span>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/category" >
                                <div class="number">
                                    <h6>CATEGORY</h6>
                                    <span><?=$category_count;?></span>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/servicelist" >
                                <div class="number">
                                    <h6>SERVICES</h6>
                                    <span><?=$service_count;?></span>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>




             <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/service_request" >
                                <div class="number">
                                    <h6>JOB REQUEST</h6>
                                    <span><?=$service_req_cnt;?></span>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/assign_list/<?=base64_encode(base64_encode('3'))?>" >
                                <div class="number">
                                    <h6>INPROGRESS JOBS</h6>
                                    <span><?=$inprogress_cnt;?></span>
                                </div>
                            </a>
                           
                        </div>
                        
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <a href="<?=$base_url;?>/assign_list/<?=base64_encode(base64_encode('2'))?>" >
                                <div class="number">
                                    <h6>COMPLETED JOBS</h6>
                                    <span><?=$jobs_count;?></span>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card overflowhidden number-chart">
                        <div class="body">
                            <div class="number">
                                <h6>TODAY EARNINGS</h6>
                                <span> ₹ <?=$today_earning;?></span>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>

          


          
        </div> 