<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';
$home_url=$this->config->item('base_url');
$this->load->model('Admin_model');

/*echo '<pre>';print_r($cus_id);
exit;*/


?>
<style type="text/css">

	.input-group-append, .input-group-prepend, .asColorPicker-trigger {
    background: #ffffff;
    color: #c9c8c8;
    width: auto;
    border: none;
}
	body,html{
		height: 100%;
		margin: 0;
		background: #FFFFFF;
       	/*background: -webkit-linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);
       background: linear-gradient(to right, #91EAE4, #86A8E7, #7F7FD5);*/
	}

	.chat{
		/*margin-top: auto;*/
		/*margin-bottom: auto;*/
		

	}
	.card{
		margin-bottom:0 !important;
		border-radius: 15px !important;
		background-color: rgba(0,0,0,0.4) !important;
	}
	.contacts_body{

		padding:  0.75rem 0 !important;
		overflow-y: auto;
		white-space: nowrap;

	}
	.msg_card_body{
		overflow-y: auto;
		max-height:80vh;
		/*overflow-x:hidden;*/
		overflow:auto;

	    width: auto;
	    height: 290px;

	}
	.card .body{
		padding: 55px 0px !important;
	}

	.contacts_card{
		/*height: 533px;*/

		overflow:auto;
		height:100%;
		/*max-height:80vh;*/

	}
	/*.contacts_card{
		background-color: #47477B !important;
	}
	#chat_div{
		background-color: #47477B !important;
	}*/
	/*.msg_container_base{
	  background: #e5e5e5;
	  margin: 0;
	  padding: 0 10px 10px;
	  max-height:80vh;
	  overflow-x:hidden;
	}
*/
	.card-header{
		border-radius: 15px 15px 0 0 !important;
		border-bottom: 0 !important;
	}
	.card-footer{
		border-radius: 0 0 15px 15px !important;
			border-top: 0 !important;
	}
	.container{
		align-content: center;
	}
	.search{
		border-radius: 15px 0 0 15px !important;
		background-color: rgba(0,0,0,0.3) !important;
		border:0 !important;
		color:white !important;
	}
	.search:focus{
	     box-shadow:none !important;
       outline:0px !important;
	}
	.type_msg{
		background-color: rgba(0,0,0,0.3) !important;
		border:0 !important;
		color:white !important;
		height: 60px !important;
		overflow-y: auto;
		resize: none;
	}
		.type_msg:focus{
	     box-shadow:none !important;
       outline:0px !important;
	}
	.attach_btn{
		border-radius: 15px 0 0 15px !important;
		background-color: rgba(0,0,0,0.3) !important;
		border:0 !important;
		color: white !important;
		cursor: pointer;
	}
	.send_btn{
		border-radius: 0 15px 15px 0 !important;
		background-color: rgba(0,0,0,0.3) !important;
		border:0 !important;
		color: white !important;
		cursor: pointer;
	}
	.search_btn{
		border-radius: 0 15px 15px 0 !important;
		background-color: rgba(0,0,0,0.3) !important;
		border:0 !important;
		color: white !important;
		cursor: pointer;
	}
	.contacts{
		list-style: none;
		padding: 0;
	}
	.contacts li{
		width: 100% !important;
		padding: 5px 10px;
		margin-bottom: 15px !important;
	}
	.active1{
		background-color: rgba(0,0,0,0.3);
	}
	.user_img{
		height: 70px;
		width: 70px;
		border:1.5px solid #f5f6fa;
	
	}
	.user_img_msg{
		height: 40px;
		width: 40px;
		border:1.5px solid #f5f6fa;
	
	}
	.img_cont{
		position: relative;
		height: 70px;
		width: 70px;
	}
	.img_cont_msg{
		height: 40px;
		width: 40px;
	}
	.online_icon{
		position: absolute;
		height: 15px;
		width:15px;
		background-color: #4cd137;
		border-radius: 50%;
		bottom: 0.2em;
		right: 0.4em;
		border:1.5px solid white;
	}
	.offline{
		background-color: #c23616 !important;
	}
	.user_info{
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 15px;
	}
	.user_info span{
		font-size: 20px;
		color: white;
	}
	.user_info p{
	font-size: 10px;
	color: rgba(255,255,255,0.6);
	}
	.video_cam{
		margin-left: 50px;
		margin-top: 5px;
	}
	.video_cam span{
		color: white;
		font-size: 20px;
		cursor: pointer;
		margin-right: 20px;
	}
	.msg_cotainer{
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 10px;
		border-radius: 25px;
		background-color: #82ccdd;
		padding: 10px;
		position: relative;
	}
	.msg_cotainer_send{
		margin-top: auto;
		margin-bottom: auto;
		margin-right: 10px;
		border-radius: 25px;
		background-color: #78e08f;
		padding: 10px;
		position: relative;
	}
	.msg_time{
		position: absolute;
		left: 0;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
	}
	.msg_time_send{
		position: absolute;
		right:0;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
		width: max-content;
	}

	.msg_time_receive{
		position: absolute;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
		width: max-content;
	}
	.msg_head{
		position: relative;
	}
	#action_menu_btn{
		position: absolute;
		right: 10px;
		top: 10px;
		color: white;
		cursor: pointer;
		font-size: 20px;
	}
	.action_menu{
		z-index: 1;
		position: absolute;
		padding: 15px 0;
		background-color: rgba(0,0,0,0.5);
		color: white;
		border-radius: 15px;
		top: 30px;
		right: 15px;
		display: none;
	}
	.action_menu ul{
		list-style: none;
		padding: 0;
	margin: 0;
	}
	.action_menu ul li{
		width: 100%;
		padding: 10px 15px;
		margin-bottom: 5px;
	}
	.action_menu ul li i{
		padding-right: 10px;
	
	}
	.action_menu ul li:hover{
		cursor: pointer;
		background-color: rgba(0,0,0,0.2);
	}
	@media(max-width: 576px){
		.contacts_card{
			margin-bottom: 15px !important;
			overflow:auto;
		}
	}

	.tab-basic .nav-item .nav-link {
		color: #0a0a0a;
	}
	.nocontact{
		margin: 60% 70% 50% 24% !important;
		color: white;
	}
	.nomessage{
		margin: 34% 34% 33% 35% !important;
	}
	/*.tab-basic .nav-item .nav-link.active {
		background-color: #7F7ED5;
	}*/

	.msg_cotainer_group{
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 10px;
		border-radius: 25px;
		background-color: #7F7ED5;
		padding: 10px;
		margin-left: 35%;
		left: 50%;
		font-weight: 700;
	}
    .send_btn
    {
    	border-radius: 50px !important;
    padding: 0 10px;
    margin: 10px 0px;
    }
    .input-group-append{
    	padding: 0px 10px;
    }
    
</style>



<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
	

 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Vendor Chat List</h2>
                
            </div> 
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 chat_body">
            <div class="card">
               <!--  <div class="header">
                </div> -->

                <div class="body ">


                	<div class="container-fluid h-100">
			
			<div class="row justify-content-center h-100">

				<div class="col-md-4 col-xl-3 chat">

					<div class="card mb-sm-3 mb-md-0 contacts_card">
					<div class="card-header"><!-- <h3 style="color: white;">Vendors</h3> -->
						<!-- <div class="input-group" id="search-box">
							<input id="search-boxes" type="text" placeholder="Search..." name="" class="form-control search">
							<div class="input-group-prepend">
								<span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
							</div>
						</div> -->
					</div>

					

					<div class="card-body contacts_body">
						<ui class="contacts" id="contact">
						<?php 
						if(count($vendor)>0){

						foreach($vendor as $key=>$value){
							//echo '<pre>';print_r($value);
							$photo=$home_url.'/uploads/user.png';
							if($value['photo']!=''){
								$photo=$home_url.'/vendor/'.$value['id'].'/'.$value['photo'];
							}
							?>
						<li <?php if($key == 0){ ?> class="active1" <?php } else{ ?> class="" <?php }?> data-id="<?=$value['id'];?>">
							<div class="d-flex bd-highlight">
								<div class="img_cont">
									<img src="<?=$photo;?>" class="rounded-circle user_img">
									<span <?php if($value['token'] !=''){?> class="online_icon" <?php }?>></span>
								</div>
								<div class="user_info" onclick="">
									<span><?=$value['name'];?></span>
									<p><?=$value['name'];?>  <?php if($value['token'] !=''){?> is online <?php } else{?> is offline<?php }?></p>
								</div>
							</div>
						</li>
						<?php  } } else{ echo '<h3 class="nocontact" style="color:white;">No Contacts</h3>';}?>
						</ui>
					</div>
					<!-- <div class="card-footer"></div> -->
				</div></div>
				<div class="col-md-8 col-xl-8 chat" id="chat_div">
					<div class="card">
						<?php if(count($vendor)>0){
							foreach($vendor as $key2=>$value2){
						if($key2==0){
							$message=$this->Admin_model->get_message($value2['id']);

							?>
							<input type="hidden" name="receiver_id" id="receiver_id" value="<?=$value2['id'];?>">
							
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight">
								<?php 
									$photo1=$home_url.'/uploads/user.png';
									if($value2['photo']!=''){
										$photo1=$home_url.'/vendor/'.$value2['id'].'/'.$value2['photo'];
									}
								?>
								<div class="img_cont">
									<img src="<?=$photo1;?>" class="rounded-circle user_img">
									<span <?php 
									if($value2['token'] !=''){?> class="online_icon" <?php }?>></span>
								</div>
								<div class="user_info">
									<span><?=$value2['name'];?></span>
									<?php $msg_count=$this->Admin_model->get_msg_count($value2['id']);?>
									<p id="msg_count"><?=$msg_count?></p>
								</div>
								<!-- <div class="video_cam">
									<span><i class="fas fa-video"></i></span>
									<span><i class="fas fa-phone"></i></span>
								</div> -->
							</div>
							<span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
							
						</div>
						<div class="card-body msg_card_body" id="container_msg">
							
							<?php 

								if(count($message)==0){

									$date=date('Y-m-d');
									?>

									<div class="d-flex justify-content-end mb-4"  id="<?=$date.'_0';?>">
										<input type="hidden" name="receiver_id" id="receiver_id" value="<?=$vendor[0]['id']?>">
									</div>

								<?php } else{

									$admin = $this->db->get('admin')->result_array();
									$sender_id=$admin[0]['id'];
									$this->load->helper('date');
							        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
									$cur_datetime = $date->format('Y-m-d H:i A');
									$cur_date=$date->format('Y-m-d');
									$i=0; foreach($message as $key1 => $value1){
									$messages=$this->Admin_model->get_message($value2['id'],$value1['date']);

									if($i== $key1 && $cur_date == $value1['date']){?>
										<span class="msg_cotainer_group"> <?php echo $dt='Today';?></span>
									<?php }
									else if($i== $key1){?>
										<span class="msg_cotainer_group"> <?php echo $dt=date('F d, Y',strtotime($value1['date']));?></span>
										
									<?php }
									foreach ($messages as $key3 => $value3) {

										$time=date('h:i A',strtotime($value3['date_time']));

									?>
								
							<?php if( $value3['type']=='user'){?>
							<div class="d-flex justify-content-start mb-4" id="<?=$value1['date'].'_'.$key3;?>">
								<span class="img_cont_msg">
									<img src="<?=$home_url?>/uploads/user.png" class="rounded-circle user_img_msg">
								</span>
								<span class="msg_cotainer">
									<?=$value3['message'];?>
									<span class="msg_time_receive"><?=$time;?>, <?php echo $this->Admin_model->timeago($value3['timestmp']);?></span>
								</span>
							</div>
						<?php }?>
					
													
							
							<?php if($value3['type']=='admin'){?>
							<div class="d-flex justify-content-end mb-4"  id="<?=$value1['date'].'_'.$key3;?>">
								<span class="msg_cotainer_send">
									<?=$value3['message'];?>
									<span class="msg_time_send"><?=$time;?>, <?php echo $this->Admin_model->timeago($value3['timestmp']);?></span>
								</span>
								<span class="img_cont_msg">
							<img src="<?=$home_url?>/uploads/user.png" class="rounded-circle user_img_msg">
								</span>
							</div>
							<?php }?>
						<?php } $i++;} }?>

						</div>
						<div class="card-footer">
							<div class="input-group">
								<!-- <div class="input-group-append">
									<span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
								</div> -->
								<textarea name="" id="msg" class="form-control type_msg" placeholder="Type your message..."></textarea>
								<div class="input-group-append" onclick="send_msg();">
									<span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
								</div>
							</div>
						</div>

					<?php } } }else{echo '<h3 class="nomessage" style="color:white;">No Messages</h3>';}?>
					
					</div>
				</div>
			</div>
		</div>


                </div>
            </div>
        </div>
    </div>


</div>



<script type="text/javascript">
var url1='<?php echo $url;?>';
//$(".msg_card_body").stop().animate({ scrollTop: $(".msg_card_body")[0].scrollHeight}, 1000);
	$('div.msg_card_body').scrollTop($('div.msg_card_body')[0].scrollHeight);
	//$('div.contacts_card').scrollTop($('div.contacts_card')[0].scrollHeight);

$(document).ready(function(){
	$('#action_menu_btn').click(function(){
		$('.action_menu').toggle();
	});

	
	/*$('.input-group-append').click(function(){*/
		
	//});

});

function isEven(n) {
	   return n % 2 == 0;
	}

	function isOdd(n) {
	   return Math.abs(n % 2) == 1;
	}

	var current =0;
	function send_msg(){
		var xx=$("#container_msg div").last().attr("id");
		var index = xx.lastIndexOf("_");
		var result = xx.substr(index+1);
		
		if(result){
			var a=isEven(result);
			if(a==true){
				var divid=Number(result) + 1;
			}else{
				var divid=Number(result) + 1;
			}
		}
		
		var message=$('#msg').val();
		var receiver_id = $('#receiver_id').val();
		var formdata="message="+message+"&receiver_id="+receiver_id+"";
		//alert(url1);
	 $.ajax({
            type: "POST",
            url: url1+"/insertchat",
            data: formdata, 
            success: function(data)
            {
            	
                var objJSON = JSON.parse(data);
                var id1=objJSON.dates+'_'+divid;

                if(objJSON){
                	$( '<div class="d-flex justify-content-end mb-4"  id="'+id1+'"><span class="msg_cotainer_send">'+objJSON.message+'<span class="msg_time_send">'+objJSON.time+', Today</span></span><span class="img_cont_msg"><img src="<?=$home_url?>/uploads/user.png" class="rounded-circle user_img_msg"></span></div>' ).insertAfter($('#'+xx));

                	$('#msg_count').html(objJSON.count);
					$('#msg').val('');

					$(".msg_card_body").stop().animate({ scrollTop: $(".msg_card_body")[0].scrollHeight}, 1000);
                }
            }

        });
		}
$('#msg').keydown(function (e) {
 if(e.keyCode == 13)  // the enter key code
  {
   /* $('.input-group-append').click();*/
   send_msg();
    return false;  
  }
}); 



$('.contacts_body').on('click', 'li', function() {
	var id=$(this).attr('data-id');
	var formdata="id="+id;
	$('.contacts_body li.active1').removeClass('active1');
    $(this).addClass('active1');
	$.ajax({
        type: "POST",
        url: url1+"/getmessage",
        data: formdata, 
        success: function(data)
        {//alert(data);
            if(data){
				$('#chat_div').html(data);

				$.ajax({
			        type: "POST",
			        url: url1+"/getmessage_count",
			        data: formdata, 
			        success: function(data)
			        {//alert(data);
			            if(data){
							$('#msg_count').html(data);
			            }
			        }

			    });

            }
        }

    });
});

/*setInterval(
function ()
{
	$('#chat_div').load('chat.php').fadeIn("slow");
}, 3000);*/

function notifyMe(){
	var interest =$("#contact li.active1").attr('data-id')
	var formdata="id="+interest;
	$.ajax({
        type: "POST",
        url: url1+"/getmessages",
        data: formdata, 
        success: function(data)
        {//alert(data);
        	//console.log(data);
            if(data){
				$('#container_msg').html(data);
            }
        }

    });
}


//setInterval(notifyMe, 2000);


/*const searchBar = document.querySelector('div#search-box > input');

searchBar.addEventListener('keyup',function(e){
    let term = e.target.value.toLowerCase();
    let words = document.querySelectorAll('ul.contacts > li');
    console.log(words);
    Array.from(words).forEach(function(word){console.log(word);
        let title = word.textContent;alert(title);
        if(title.toLowerCase().includes(term)){
            word.parentElement.style.display = 'block';
        } else {
            word.parentElement.style.display = 'none';
        }
    });
});*/


/*$("#search-boxes").on('keyup change',function(){

	var searchText = this.value;
  var searchTerms = searchText.replace(/ /g,"|");
  var searchRegex = new RegExp(searchTerms, "i");
  
  var hasFilter = searchText.length > 0;
  
  var list = $("#contact")
  
  list.toggleClass("filtered", hasFilter);
  //$("#filter-results").toggleClass("filtered", hasFilter);
  
  list.find("li").each(function(i, el){
  	var el = $(el)
  	var elText = el.text();
    var match = searchRegex.test(elText);
    
    el.toggleClass("found", match)
  })
  
  
});*/

/* $("#search-boxes").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#contact li").filter(function() {
      if($(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)){
      	var id=$(this).attr('data-id');
      	var search_val=$('#search-boxes').val();
      	$(this).addClass('active1');
     	if(id!=''){
			$('.contacts_body li.active1').removeClass('active1');
		    $('#'+id).addClass('active1');
		    $(".contacts_body li:nth-child("+li+")").trigger("click");
		}
      }
     
    });
  });*/
 /*$("#search-boxes").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $('.contacts_body li.active1').removeClass('active1');

    // $("#contact > li").each(function() {
    //         if ($(this).text().search(value) > -1) {
    //            var id=$(this).attr('data-id');alert(id);
    //            $(this).addClass('active1');

    //            $('#contact li #'+id).addClass('active');
    //            $(".contacts_body li:nth-child("+id+")").trigger("click");
    //         }
    //         else {
    //             $(this).hide();
    //         }
    //     });

  //   $("#contact li").filter(function() {

  //     if($(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)){
  //     	var id=$(this).attr('data-id');//alert(id);
  //     	var search_val=$('#search-boxes').val();
		// alert($('.contacts li').css('display'));

  // //    	if(id!=''){
			
		// //     $('#'+id).addClass('active1');
		// //     $(".contacts_body li:nth-child("+li+")").trigger("click");
		// // }
  //     }
     
  //   });



  });
*/




$('#search-box').click(function(){
    var searchString = $('#search-box').val(),
        foundLi = $('li:contains("' + searchString + '")');
	    //foundLi.addClass('found');
	    $('#contacts_body').animate({ scrollTop: foundLi.offset().top});
	});




<?php if(isset($cus_id) && $cus_id!=''){
	$li_id=$cus_id;?>
	$(document).ready(function(){
	var li='<?php echo $li_id;?>';
	if(li!=''){
		$('.contacts_body li.active1').removeClass('active1');
	    $('#'+li).addClass('active1');
	    $(".contacts_body li:nth-child("+li+")").trigger("click");
	}
	
});
<?php }?>

</script>