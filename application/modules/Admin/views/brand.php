<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    $image_url=$this->config->item('base_url').'uploads';
?>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
<div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Brand List</h2>
                    <!-- <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a></li>   
                        <li class="breadcrumb-item active">View Page</li>
                    </ul> -->
                </div>            
              
            </div>
        </div>
    
         <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="header">
                            <h2><small><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Brand</button></small> </h2>                            
                        </div>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Brand Creation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            
                          </div>
                           <form id="add-brand-form" method="post" action="#" novalidate enctype='multipart/form-data'> 
                              <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <!-- <div class="header">
                                                    <h2>Brand Creation</h2>
                                                </div> -->
                                                <div class="body">

                                                      <div class="form-group">
                                                        <label>Select Category</label>
                                                        <select name="selectcat" id="selectcat" required class="form-control">
                                                          <option value="">Select Option</option>
                                                       <?php  foreach ($fetch_data as $cat) {  ?>
                                                          <option value="<?= $cat['id'] ?>" ><?= $cat['category_name'] ?></option>
                                                        <?php } ?>
                                                        </select>
                                                        </div>  

                                                        <div class="form-group" >
                                                      <label for="email_id">Brand Name</label>
                                                          <div class="mb-3">
                                                              <select class="form-control show-tick ms select2" name="brand_name[]" id="brand_name" multiple data-placeholder="Select"  required="">
                                                                 <option value=""></option>
                                                                <?php if(count($brandlist)>0){
                                                                  foreach($brandlist as $key => $value){?>
                                                                <option value="<?=$value['id']?>"><?=$value['brand_name']?></option>
                                                                <?php } }?>
                                                              </select>
                                                          </div>
                                                   </div>     
                            
                                                </div>
                                            </div>
                                        </div>

                              </div>
                              <div class="modal-footer">
                               <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                               <button type="submit" id="brandcategory" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                              </div>
                          </form>
                        </div>

                      </div>
                    </div>



                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Category Name</th>
                                            <th>Brand Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
  <?php
     $sl=1;  
     foreach ($categorybrandlist as $display) {
      

       $catname=$this->Category_model->getcategory($display['category_id']);
       $brand=$this->Category_model->getbrandname($display['brand_id']);


   ?>
                     
              
                                     <tr id="<?=$display['id']?>" > 

                                      <td><?= $sl?></td>
                                      <td><?php if(count($catname)>0){  echo $catname[0]['category_name'];  }else{  } ?>   </td>
                                      <td><?= $brand; ?>   </td>
                                      
                                      <td> <a data-toggle="modal" data-id="<?=$display['id']?>" 
                             data-target="#modal-view<?= $display['id'] ?>" class="btn btn-info" ><i class="fa fa-pencil"></i>Update</a>  | <button  data-id="<?=$display['id']?>" class="btn btn-danger button-remove" ><i class="fa fa-trash"></i>Delete</button></td>
                                       </tr>
                       
                               
                               <div class="modal modal-defalut fade" id="modal-view<?=$display['id']?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                     <h4 class="modal-title"> Category Details Update</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </div>
                                     <form id="update-brand-data<?=$display['id']?>" action="#" method="post" enctype='multipart/form-data'> 
                                   <div class="modal-body">
                                         <div class="row">
                                            <div class="col-md-12" >

                                            <div class="form-group">
                                              <label>Select Category</label>
                                              <select name="upselectcat" id="upselectcat<?= $display['id'] ?>" required class="form-control" >
                                                <option value="" >Select Option</option>
                                             <?php  foreach ($fetch_data as $cat) {  ?>
                                                <option <?php if($cat['id']==$display['category_id']){ echo "Selected"; }  ?>  value="<?= $cat['id'] ?>" ><?= $cat['category_name'] ?></option>
                                              <?php } ?>
                                              </select>
                                              </div>  


                                            <?php $brand_id=$display['brand_id'];
                                                  $brand_ids=explode(',',$brand_id);
                                            ?>
                                            <div class="form-group" >
                                                <label for="email_id">Brand Name</label>
                                                    <div class="mb-3">
                                                        <select class="form-control show-tick ms select2" name="upbrand_name[]" id="upbrand_name<?= $display['id'] ?>" multiple data-placeholder="Select"  required="">
                                                           <option value=""></option>
                                                          <?php if(count($brandlist)>0){
                                                            foreach($brandlist as $key => $value){?>
                                                          <option value="<?=$value['id']?>" <?php if(in_array($value['id'],$brand_ids)){?> selected <?php }?>><?=$value['brand_name']?></option>
                                                          <?php } }?>
                                                        </select>
                                                    </div>
                                            </div>  


                                               <input type="hidden" id="hiddenid" name="hiddenid" value="<?= $display['id'] ?>">

                                             </div>
                                          </div>

                                    </div>
                                      <div class="modal-footer modal-footer-uniform">
                                      <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-bold btn-pure btn-primary float-right"  >Save changes</button>
                                      </div>  
                                      </form> 


                                       <script>
                                  $(document).ready(function(){ 
                    $('#update-brand-data<?=$display['id']?>').parsley();

                    $('#update-brand-data<?=$display['id'];?>').parsley().destroy();


                    $('#update-brand-data<?=$display['id'];?>').parsley().on('form:submit', function() {//alert('aaaaaa');

                        var form_data=$("#update-brand-data<?=$display['id']?>").serialize();
                        var url1='<?php echo $base_url;?>';
                       
                        $.ajax({

                            type: "POST",
                            url: url1+"/updatebrand",
                            data: form_data, 
                            success: function(data)
                            {
                                alert(data);
                                $('.parsley-required').remove();

                                if(data==1){
                                    $("#upselectcat<?=$display['id'];?>").focus();
                                    $("#upselectcat<?=$display['id'];?>").removeClass('parsley-success');
                                    $("#upselectcat<?=$display['id'];?>").addClass('parsley-error');
                                    $("#upselectcat<?=$display['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    return false;
                                }
                                else if(data == 2){
                                    $("#upbrand_name<?=$display['id'];?>").focus();
                                    $("#upbrand_name<?=$display['id'];?>").removeClass('parsley-success');
                                    $("#upbrand_name<?=$display['id'];?>").addClass('parsley-error');
                                    $("#upbrand_name<?=$display['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#upbrand_name<?=$display['id'];?>").focus();
                                    return false;
                                }
                                else if(data == 3){
                                    swal("Success!", "Your file has been updated.", "success");
                                    location.reload();
                                }
                               
                            }

                        }); 
                        return false; // Don't submit form for this demo
                    });
 
                                      
                                });  
                            
                        
                                </script>

                                  </div>
                              </div>   
                            </div>

                       

                                    


    <?php  $sl++;  } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>

 </div>



<script type="text/javascript">
   var url1='<?php echo $base_url;?>';
    $(function() {
     

      $('.select2').select2();

      $('#add-brand-form').parsley();
      $('#add-brand-form').parsley().on('form:submit', function() {
            var form_data=$("#add-brand-form").serialize();
            $.ajax({

                type: "POST",
                url: url1+"/addbrand",
                data: form_data, 
                success: function(data)
                {//alert(data);
                    if(data==1){
                       location.reload();
                    }
                    
                }

            }); 
            return false; // Don't submit form for this demo
        });
      
  });
  
  

 



  
    $('.button-remove').on('click', function(){
   
        var brand_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");
      
        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
                 $.ajax({

                type: "POST",
                url: url1+"/deletebrand",
                data: {del_id:brand_id}, 
                success: function(data)
                {

                    if(data==1){
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                         $("#"+id).remove();
                    }else{
                         swal("Deleted Fail!", "Your imaginary file has been not deleted.", "error");
                    }
                }
            });
        });
    })



 </script>