<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
?>

					<div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        
					    <div class="body">
                            <form class="form-auth-small" action="<?=$base_url;?>/login_action" method="post">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="email" class="form-control" name="email" id="signin-email" value="" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" name="password" id="signin-password" value="" placeholder="Password" required>
                                </div>
                                
                                <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                                
                            </form>
                        </div>
                        </div>

<!-- Toaster Alert Page By boopathi -->
<?php $this->load->view('Admin/toast'); ?>  
<!-- End Toaster Alert Page By boopathi -->