<?php

    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    $image_url=$this->config->item('base_url').'uploads'; 

?>
<div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Category Details List</h2>
                   <!--  <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a></li>   
                        <li class="breadcrumb-item active">View Page</li>
                    </ul> -->
                </div>            
              
            </div>
        </div>
    
         <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="header">
                            <h2><small><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Category</button></small> </h2>                            
                        </div>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                             <h4 class="modal-title">Category Creation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                          </div>
                           <form id="add-form" method="post" action="<?= $base_url.'/addcategory' ?>" novalidate enctype='multipart/form-data' > 
                              <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                               <!--  <div class="header">
                                                    <h2>Category Creation</h2>
                                                </div> -->
                                                <div class="body">
                                                   
                                                        <div class="form-group">
                                                            <label>Category Name</label>
                                                            <input type="text" name="category_name" id="category_name" class="form-control" required="">
                                                        </div> 

                                                        <div class="form-group">
                                                          <label>Service Amount</label>
                                                            <div class="input-group">
                                                              <div class="input-group-prepend">
                                                                <span class="input-group-text">₹</span>
                                                              </div>
                                                              <input type="text" name="serviceamount" id="serviceamount" class="form-control del_space" required placeholder="Enter Service Amount">
                                                          </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label>Image</label>
                                                            <input type="file" name="photos" id="photos" accept=".png, .jpg, .jpeg" title="Images To Be Uploaded" class="form-control" > 

                                                        </div>                              

                                                </div>
                                            </div>
                                        </div>

                              </div>
                              <div class="modal-footer">
                               <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                               <button type="button" id="categorysubmit" onClick="Form_Submit()" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                              </div>
                          </form>
                        </div>

                      </div>
                    </div>



                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Category Name</th>
                                            <th>Cost</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
  <?php
     $sl=1;  
     foreach ($fetch_data as $display) {
   ?>
                                  
                                     <tr id="<?=$display['id']?>" > 

                                      <td><?= $sl?></td>
                                      <td><?= $display['category_name'] ?>   </td>
                                      <td><?= $display['category_amount'] ?>   </td>
                                      <td><img src="<?= $image_url.'/'.$display['icon'] ?>" style="max-width:45px; max-height:45px; " >  </td>

                                      <td> <a data-toggle="modal" data-id="<?=$display['id']?>" 
                             data-target="#modal-view<?=$display['id']?>" class="btn btn-info" ><i class="fa fa-pencil"></i>Update</a>  | <button  data-id="<?=$display['id']?>" class="btn btn-danger button-remove" ><i class="fa fa-trash"></i>Delete</button></td>
                          
                        <form id="form-data" action="#" method="post" enctype='multipart/form-data'> 
                               
                               <div class="modal modal-defalut fade" id="modal-view<?=$display['id']?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                     <h4 class="modal-title"> Category Details Update</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </div>
                                   <div class="modal-body">
                                         <div class="row">
                                            <div class="col-md-12" >


                                            <div class="form-group">
                                                <label>Category Name</label>
                                                <input type="text" name="upcategory_name" id="upcategory_name<?=$display['id']?>" autocomplete="" required="" value="<?=$display['category_name']?>" class="form-control" required="">
                                            </div> 

                                            <div class="form-group">
                                                <label>Service Amount</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text">₹</span>
                                                    </div>
                                                    <input type="text" name="upservice_amount<?= $display['id'] ?>" id="upservice_amount<?= $display['id'] ?>" class="form-control del_space" required placeholder="Enter Service Name" value="<?= $display['category_amount'] ?>" >
                                              </div>
                                            </div>     

                                              <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="upphotos" id="upphotos<?=$display['id']?>" accept=".png, .jpg, .jpeg" title="Images To Be Uploaded" class="form-control" onchange="ProfileURL(this);"> 

                                                    <?php if($display['icon']){?>
                                                    <img src="<?= $image_url.'/'.$display['icon'] ?>" style="width:90px; height: 100px;border-radius: initial;padding-top: 10px;" id="profileview"/>
                                                  <?php }?>
                                               </div>   

                                            <input type="hidden" id="hiddenid" name="hiddenid" value="<?=$display['id']?>">

                                             </div>
                                          </div>

                                    </div>
                                      <div class="modal-footer modal-footer-uniform">
                                      <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" data-id="<?=$display['id']?>" class="btn btn-bold btn-pure btn-primary float-right" onClick="Update_Submit(<?=$display['id']?>)" >Save changes</button>
                                      </div>      
                                  </div>
                              </div>   
                            </div>



                                     </tr>


    <?php  $sl++;  } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>

            


 </div>



<script type="text/javascript">
  $(function() {
      $('#form-data').parsley();
      $('#add-data').parsley();
  });

var url1='<?php echo $base_url;?>';

 function Form_Submit()
 {
        $('.parsley-required').remove();

        var category_name = $('#category_name').val();
        var amount=$('#serviceamount').val();

        var formData = new FormData();

        formData.append('category_name', category_name);
        formData.append('serviceamount', amount);

if ($('#photos')[0].files[0] != undefined) {

   formData.append('photos', $('#photos')[0].files[0]); 

}


 if(category_name !=''){

            $.ajax({

                type: "POST",
                url: url1+"/addcategory",
                data: formData, 

                contentType: false,      
                cache: false,       
                processData:false,   


                success: function(data)
                {

                  if(data==1){
                   
                     location.reload();

                  }else{

                    toastr.error("Insert Fail..!!");
                   
                  }

                }

            }); 


           

           }else{
                $("#category_name").focus();
                $("#category_name").removeClass('parsley-success');
                $("#category_name").addClass('parsley-error');
                $("#category_name").parsley().addError('required', { message: 'Category Title Empty'});
                return false;
         }  
          

    }



 
  function Update_Submit(getid)
  {
      $('.parsley-required').remove();
      var upData = $('#upcategory_name'+getid).val();
      var upPhoto = $('#upphotos'+getid).val();
      var ser_amount=$('#upservice_amount'+getid).val();
      var formData = new FormData();
      formData.append('hidden_id', getid);
      formData.append('update_title', upData);
      formData.append('serviceamount', ser_amount);      

if ($('#upphotos'+getid)[0].files[0] != undefined) {
     formData.append('upphotos', $('#upphotos'+getid)[0].files[0]); 
}


        if(upData !=''){

                        $.ajax({
                                 type: "POST",
                                 url: url1+"/updatecategory",
                                 data: formData, 

                                contentType: false,       // The content type used when sending data to the server.
                                cache: false,             // To unable request pages to be cached
                                processData:false,        // To send DOMDocument or non processed data file

                                 success: function(data)
                                 {

                                       if(data==1){
                                            
                                               location.reload();

                                        }else{ 
                               
                                            swal("Update Failed!", "Something Went Wrong. Retry.", "error");


                                        }

                                 }
                        });  

         }else{
                $("#upcategory_name"+getid).focus();
                $("#upcategory_name"+getid).removeClass('parsley-success');
                $("#upcategory_name"+getid).addClass('parsley-error');
                $("#upcategory_name"+getid).parsley().addError('required', { message: 'Empty'});
                return false;
         }


  }



  $('.button-remove').on('click', function(){
   
        var customer_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");
      
        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
                 $.ajax({

                type: "POST",
                url: url1+"/delete_category",
                data: {del_id:customer_id}, 
                success: function(data)
                {

                    if(data==1){
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                         $("#"+id).remove();
                    }else{
                         swal("Deleted Fail!", "Your imaginary file has been not deleted.", "error");
                    }
                }
            });
        });
    })


function ProfileURL(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
    
    $('#profileview').attr('src', e.target.result).width(90).height(100);
   /*  $('#profileview').after('<input type="button" class="delbtn removebtn" value="remove">'); */
    };

    reader.readAsDataURL(input.files[0]);
    }
}


            
</script>