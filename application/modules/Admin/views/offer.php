<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    $image_url=$this->config->item('base_url').'uploads';
?>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
<div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Offer List</h2>
                   <!--  <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= $base_url ?>"><i class="icon-home"></i></a></li>   
                        <li class="breadcrumb-item active">View Page</li>
                    </ul> -->
                </div>            
              
            </div>
        </div>
    
         <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="header">
                            <h2><small><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Offers</button></small> </h2>                           
                        </div>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                           <!-- <div class="header">
                              <h4>Offer Creation</h4>
                           </div> -->
                           <h4>Offer Creation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          
                          </div>
                           <form id="add-offer-form" method="post" action="#" > 
                              <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">

                                                     <div class="form-group">
                                                          <label>Offer Name</label>
                                                          <input type="text" name="offer_name" id="offer_name" class="form-control del_space" required placeholder="Enter Offer Name">
                                                      </div> 

                                                      <div class="form-group">
                                                        <label>Offer</label>
                                                          <div class="input-group">
                                                            <div class="input-group-prepend">
                                                              <span class="input-group-text">%</span>
                                                            </div>
                                                            <input type="text" name="offer_per" id="offer_per" class="form-control del_space" required placeholder="Enter Offer Price">
                                                        </div>
                                                      </div> 

                                                      <div class="form-group">
                                                          <label>Start Date</label>
                                                          <!-- <input type="text" name="start_date" id="start_date" class="form-control del_space" required placeholder=""  autocomplete="off"> -->

                                                           <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                              <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" required>
                                                              <div class="input-group-append">
                                                                  <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class="form-group">
                                                          <label>End Date</label>
                                                          <!-- <input type="text" name="end_date" id="end_date" class="form-control del_space" required placeholder=""> -->

                                                          <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                              <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" required>
                                                              <div class="input-group-append">
                                                                  <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                              </div>
                                                          </div>
                                                      </div>  

                                                      <div class="form-group" >
                                                      <label for="email_id">Product Category</label>
                                                          <div class="mb-3">
                                                              <select class="form-control show-tick ms select2" name="product_cate[]" id="product_cate" multiple data-placeholder="Select"  required="">
                                                                 <option value=""></option>
                                                                <?php if(count($product_cat)>0){
                                                                  foreach($product_cat as $key => $value){?>
                                                                <option value="<?=$value['id']?>"><?=$value['category_name']?></option>
                                                                <?php } }?>
                                                              </select>
                                                          </div>
                                                   </div>     
                     

                                                </div>
                                            </div>
                                        </div>

                              </div>
                              <div class="modal-footer">
                               <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                               <button type="submit" id="offer_category" onClick="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                              </div>
                          </form>
                        </div>

                      </div>
                    </div>



                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Offer Name</th>
                                            <th>Offer Percentage (%)</th>
                                            <th>Offer Date</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                        if(count($offerlist)>0){
                                         $sl=1;  
                                         foreach ($offerlist as $row) {
                                       ?>                    
                                    <tr id="<?=$row['id']?>" > 
                                      <td><?= $sl?></td>
                                      <td><?php  echo $row['offer_name']; ?>   </td>
                                      <td><?php  echo $row['offer_per'];  ?>   </td>
                                      <td><?php  echo $row['start_date'].' TO '.$row['end_date'];  ?>   </td>
                                      <td>
                                        <?php  $cate=$this->Category_model->getcategory($row['pro_cate_id']);
                                        $category_name='';
                                        if(count($cate)>0){
                                          $category_name=$cate[0]['category_name'];
                                        }

                                        echo $category_name;  ?>  
                                         </td>
                                      <td> <a data-toggle="modal" data-id="<?=$row['id']?>" 
                             data-target="#modal-view<?= $row['id'] ?>" class="btn btn-info" ><i class="fa fa-pencil"></i>Update</a>  | <button  data-id="<?=$row['id']?>" class="btn btn-danger button-remove" ><i class="fa fa-trash"></i>Delete</button></td>
                             </tr>



                        
                               
                               <div class="modal modal-defalut fade" id="modal-view<?=$row['id']?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                     <h4 class="modal-title"> Offer Details Update</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </div>
                                     <form id="update-offer-data<?=$row['id']?>" action="#" method="post" >
                                   <div class="modal-body">
                                         <div class="row">
                                            <div class="col-md-12" >

                                            <div class="form-group">
                                                <label>Offer Name</label>
                                                <input type="text" name="offer_name" id="offer_name<?= $row['id'] ?>" class="form-control del_space" required placeholder="Enter Offer Name" value="<?= $row['offer_name'] ?>" >
                                            </div> 

                                             <div class="form-group">
                                                <label>Offer</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text">%</span>
                                                    </div>
                                                    <input type="text" name="offer_per" id="offer_per<?= $row['id'] ?>" class="form-control del_space" required placeholder="Enter Offer" value="<?= $row['offer_per'] ?>" >
                                              </div>
                                            </div>  

                                              <div class="form-group">
                                                    <label>Start Date</label>
                                                    <!-- <input type="text" name="start_date" id="start_date<?= $row['id'] ?>" value="<?=$row['start_date']?>" class="form-control del_space" required placeholder=""> -->

                                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                          <input type="text" class="form-control del_space" name="start_date" id="start_date<?= $row['id'] ?>" autocomplete="off" required value="<?=$row['start_date']?>" >
                                                          <div class="input-group-append">
                                                              <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                          </div>
                                                      </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>End Date</label>
                                                   <!--  <input type="text" name="end_date" id="end_date<?= $row['id'] ?>" value="<?=$row['end_date']?>" class="form-control del_space" required placeholder=""> -->
                                                     <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                          <input type="text" class="form-control del_space" name="end_date" id="end_date<?= $row['id'] ?>" autocomplete="off" required value="<?=$row['end_date']?>" >
                                                          <div class="input-group-append">
                                                              <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                          </div>
                                                      </div>
                                                </div>

                                                 <div class="form-group" >
                                                    <label for="email_id">Product Category</label>
                                                        <div class="mb-3">
                                                            <select class="form-control " name="product_cate" id="product_cate<?= $row['id'] ?>"  data-placeholder="Select"  required="">
                                                               <option value=""></option>
                                                              <?php if(count($product_cat)>0){
                                                                foreach($product_cat as $key => $value){?>
                                                              <option value="<?=$value['id']?>" <?php if($value['id']==$row['pro_cate_id']){?> selected <?php } ?> ><?=$value['category_name']?></option>
                                                              <?php } }?>
                                                            </select>
                                                        </div>
                                                 </div>       

                                               <input type="hidden" id="hiddenid" name="hiddenid" value="<?= $row['id'] ?>">

                                             </div>
                                          </div>

                                    </div>
                                      <div class="modal-footer">
                                       <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                                <button type="submit"  class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                                      </div> 
                                      </form>    
                                  </div>
                              </div>   
                            </div>

                       <!--  </form> -->
                       <script>
                                  $(document).ready(function(){ 
                    $('#update-offer-data<?=$row['id']?>').parsley();

                    $('#update-offer-data<?=$row['id'];?>').parsley().destroy();


                    $('#update-offer-data<?=$row['id'];?>').parsley().on('form:submit', function() {

                        var form_data=$("#update-offer-data<?=$row['id']?>").serialize();
                        var url1='<?php echo $base_url;?>';
                       
                        $.ajax({

                            type: "POST",
                            url: url1+"/update_offer",
                            data: form_data, 
                            success: function(data)
                            {
                                //alert(data);
                                $('.parsley-required').remove();

                                if(data==1){
                                    $("#offer_name<?=$row['id'];?>").focus();
                                    $("#offer_name<?=$row['id'];?>").removeClass('parsley-success');
                                    $("#offer_name<?=$row['id'];?>").addClass('parsley-error');
                                    $("#offer_name<?=$row['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    return false;
                                }
                                else if(data == 2){
                                    $("#offer_per<?=$row['id'];?>").focus();
                                    $("#offer_per<?=$row['id'];?>").removeClass('parsley-success');
                                    $("#offer_per<?=$row['id'];?>").addClass('parsley-error');
                                    $("#offer_per<?=$row['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#offer_per<?=$row['id'];?>").focus();
                                    return false;
                                }
                                else if(data == 3){
                                    $("#product_cate<?=$row['id'];?>").focus();
                                    $("#product_cate<?=$row['id'];?>").removeClass('parsley-success');
                                    $("#product_cate<?=$row['id'];?>").addClass('parsley-error');
                                    $("#product_cate<?=$row['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#product_cate<?=$row['id'];?>").focus();
                                    return false;
                                }
                                else if(data == 4){
                                    $("#start_date<?=$row['id'];?>").focus();
                                    $("#start_date<?=$row['id'];?>").removeClass('parsley-success');
                                    $("#start_date<?=$row['id'];?>").addClass('parsley-error');
                                    $("#start_date<?=$row['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#start_date<?=$row['id'];?>").focus();
                                    return false;
                                }
                                else if(data == 5){
                                    $("#end_date<?=$row['id'];?>").focus();
                                    $("#end_date<?=$row['id'];?>").removeClass('parsley-success');
                                    $("#end_date<?=$row['id'];?>").addClass('parsley-error');
                                    $("#end_date<?=$row['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#end_date<?=$row['id'];?>").focus();
                                    return false;
                                }
                                if(data==6){
                                   //swal("Success!", "Your file has been updated.", "success");
                                   location.reload();
                                }
                                else if(data == 0){
                                    //swal("Failure!", "Your imaginary file has been deleted.", "success");
                                    location.reload();
                                }
                               
                            }

                        }); 
                        return false; // Don't submit form for this demo
                    });
 
                                      
                                });  
                            
                        
                                </script>

                                    


    <?php  $sl++;  } } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>

 </div>

 <script type="text/javascript">
   var url1='<?php echo $base_url;?>';
 $(function() {
     

      $('.select2').select2();

      $('#add-offer-form').parsley();
      $('#add-offer-form').parsley().on('form:submit', function() {
            var form_data=$("#add-offer-form").serialize();
            $.ajax({

                type: "POST",
                url: url1+"/addoffer",
                data: form_data, 
                success: function(data)
                {//alert(data);
                    if(data==1){
                       location.reload();
                    }
                    
                }

            }); 
            return false; // Don't submit form for this demo
        });
      
  });


 


/*
|---------------------------------------------------------------------------------------------------------
|                     Delete Form Ajax 
|---------------------------------------------------------------------------------------------------------
*/

   $('.button-remove').on('click', function(){
   
        var Delete_id=$(this).attr('data-id');

        var id = $(this).parents("tr").attr("id");

        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
          

                $.ajax({

                type: "POST",
                url: url1+"/deleteoffer",
                data: {del_id:Delete_id}, 
                success: function(data)
                {

                    if(data==1){
                        toastr.success("Deleted Successfully!"); 
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                         $("#"+id).remove();
                    }else{
                        toastr.error("Record Removed Fail");
                         swal("Deleted Fail!", "Your imaginary file has been not deleted.", "error");
                    }
                }

            });
        });
    })

   $('.date').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          /*todayHighlight: true*/
    })

 </script>