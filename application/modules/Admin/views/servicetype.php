<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    $image_url=$this->config->item('base_url').'uploads';
?>
<div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Service List</h2>
                   <!--  <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= $base_url ?>"><i class="icon-home"></i></a></li>   
                        <li class="breadcrumb-item active">View Page</li>
                    </ul> -->
                </div>            
              
            </div>
        </div>
    
         <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="header">
                            <h2><small><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Service</button></small> </h2>                           
                        </div>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                          <!--  <div class="header">
                              
                           </div> -->
                            <h4>Service Creation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          
                          </div>
                           <form id="add-form" method="post" action="#" novalidate enctype='multipart/form-data'> 
                              <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">

                                                     <div class="form-group">
                                                          <label>Service Type</label>
                                                          <input type="text" name="servicetype" id="servicetype" class="form-control del_space" required placeholder="Enter Service Name">
                                                      </div> 

                                                     <!--  <div class="form-group">
                                                        <label>Service Amount</label>
                                                          <div class="input-group">
                                                            <div class="input-group-prepend">
                                                              <span class="input-group-text">₹</span>
                                                            </div>
                                                            <input type="text" name="serviceamount" id="serviceamount" class="form-control del_space" required placeholder="Enter Service Amount">
                                                        </div>
                                                      </div>     -->
                     

                                                </div>
                                            </div>
                                        </div>

                              </div>
                              <div class="modal-footer">
                               <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                               <button type="button" id="subcategory" onClick="Form_Submit()" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                              </div>
                          </form>
                        </div>

                      </div>
                    </div>



                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Service Name</th>
                                          <!--   <th>Service Amount</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                        if(count($fetchlist)>0){
                                         $sl=1;  
                                         foreach ($fetchlist as $row) {
                                       ?>                    
                                    <tr id="<?=$row['id']?>" > 
                                      <td><?= $sl?></td>
                                      <td><?php if(count($row)>0){  echo $row['service_type'];  }else{  } ?>   </td>
                                      <!-- <td><?php if(count($row)>0){  echo $row['service_amount'];  }else{  } ?>   </td> -->
                                      <td> <a data-toggle="modal" data-id="<?=$row['id']?>" 
                             data-target="#modal-view<?= $row['id'] ?>" class="btn btn-info" ><i class="fa fa-pencil"></i>Update</a>  | <button  data-id="<?=$row['id']?>" class="btn btn-danger button-remove" ><i class="fa fa-trash"></i>Delete</button></td>
                          
                        <form id="update-data" action="#" method="post" enctype='multipart/form-data'> 
                               
                               <div class="modal modal-defalut fade" id="modal-view<?=$row['id']?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                     <h4 class="modal-title"> Service Details Update</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </div>
                                   <div class="modal-body">
                                         <div class="row">
                                            <div class="col-md-12" >

                                            <div class="form-group">
                                                <label>Service Type</label>
                                                <input type="text" name="upservice_type<?= $row['id'] ?>" id="upservice_type<?= $row['id'] ?>" class="form-control del_space" required placeholder="Enter Service Name" value="<?= $row['service_type'] ?>" >
                                            </div> 

                                            <!--  <div class="form-group">
                                                <label>Service Amount</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text">₹</span>
                                                    </div>
                                                    <input type="text" name="upservice_amount<?= $row['id'] ?>" id="upservice_amount<?= $row['id'] ?>" class="form-control del_space" required placeholder="Enter Service Name" value="<?= $row['service_amount'] ?>" >
                                              </div>
                                            </div>   -->

                                               <input type="hidden" id="hiddenid" name="hiddenid" value="<?= $row['id'] ?>">

                                             </div>
                                          </div>

                                    </div>
                                      <div class="modal-footer modal-footer-uniform">
                                      <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" data-id="<?=$row['id']?>" class="btn btn-bold btn-pure btn-primary float-right" onClick="Update_Submit(<?=$row['id']?>)" >Save changes</button>
                                      </div>      
                                  </div>
                              </div>   
                            </div>

                        </form>

                                     </tr>


    <?php  $sl++;  } } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>

 </div>

 <script type="text/javascript">
 $(function() {
      $('#add-form').parsley();
  });


  var url1='<?php echo $base_url;?>';


/*
|---------------------------------------------------------------------------------------------------------
|                     Insert Form Ajax 
|---------------------------------------------------------------------------------------------------------
*/

 function Form_Submit()
 {
        $('.parsley-required').remove();
       var name = $('#servicetype').val();
        /*var amount=$('#serviceamount').val();*/
        var formData = new FormData();
        formData.append('servicetype', name);
       /* formData.append('serviceamount', amount);*/
        

        if(name !='' /*&& amount!=''*/){
            $.ajax({
                type: "POST",
                url: url1+"/addservicetype",
                data: formData, 
                contentType: false,      
                cache: false,       
                processData:false,   


                success: function(data)
                {

                  if(data==1){
                   
                     location.reload();

                  }else if(data=='RepeatData'){ 

                           swal("Try Different.", "Dublicate Data Are Not Allowed .", "error");

                   }else{

                    toastr.error("Insert Fail..!!");
                   
                  }

                }

            }); 


           }else{

                $("#servicetype").focus();
                $("#servicetype").removeClass('parsley-success');
                $("#servicetype").addClass('parsley-error');
                $("#servicetype").parsley().addError('required', { message: 'Service Name Empty'});
                return false;
         }  
          

    }


/*
|---------------------------------------------------------------------------------------------------------
|                     Update Form Ajax 
|---------------------------------------------------------------------------------------------------------
*/

  function Update_Submit(getid)
  {
      $('#update-data').parsley();
      $('.parsley-required').remove();
     
      var main_name = $('#upservice_type'+getid).val();
      /*var ser_amount=$('#upservice_amount'+getid).val();*/
      var formData = new FormData();  
        formData.append('hidden_id', getid);
        formData.append('servicetype', main_name); 
        /*formData.append('serviceamount', ser_amount);      */

      if(main_name !=''/* && ser_amount!=''*/){
             
                        $.ajax({
                                 type: "POST",
                                 url: url1+"/updateservicetype",
                                 data: formData, 

                                contentType: false,       // The content type used when sending data to the server.
                                cache: false,             // To unable request pages to be cached
                                processData:false,        // To send DOMDocument or non processed data file

                                 success: function(data)
                                 {

                                       if(data==1){
                                            
                                               location.reload();

                                        }else{ 
                                            

                                            toastr.error("Insert Fail..!!");
                                            swal("Update Failed!", "Something Went Wrong. Retry.", "error");


                                        }

                                 }
                        });  


       

         }else{
                $("#upservice_type"+getid).focus();
                $("#upservice_type"+getid).removeClass('parsley-success');
                $("#upservice_type"+getid).addClass('parsley-error');
                $("#upservice_type"+getid).parsley().addError('required', { message: 'Service Name Empty'});
                return false;
         }  


  }

/*
|---------------------------------------------------------------------------------------------------------
|                     Delete Form Ajax 
|---------------------------------------------------------------------------------------------------------
*/

   $('.button-remove').on('click', function(){
   
        var Delete_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");

        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
          

                $.ajax({

                type: "POST",
                url: url1+"/deleteservicetype",
                data: {del_id:Delete_id}, 
                success: function(data)
                {

                    if(data==1){
                        toastr.success("Deleted Successfully!"); 
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                         $("#"+id).remove();
                    }else{
                        toastr.error("Record Removed Fail");
                         swal("Deleted Fail!", "Your imaginary file has been not deleted.", "error");
                    }
                }

            });
        });
    })

 </script>