<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';
//echo $this->config->item('base_url').'uploads/1.png';
?>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Vendor Lists</h2>
                
            </div> 
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Vendors</button>                           
                </div>



                  <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="max-width:1000px;">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                
                                <h4 class="modal-title" style="width: 100%;">Vendor Creation <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                            </div>
                        <form id="customer-form" method="post" action="#" > 
                            <div class="modal-body">
                                <div class="col-lg-12">
                                    <div class="card">
                                        
                                        <div class="body">
                                          <div class="row clearfix">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Vendor Name</label>
                                            <input type="text" name="customer_name" id="customer_name" class="form-control" required>
                                            </div>                              
                                        
                                         <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder="" required step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" maxlength="10"/>
                                        </div>  
                                       
                                        <!-- <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Confirm Password *" name="confirm_password" data-parsley-equalto="#password" required>
                                        </div> -->

                                         <div class="form-group" >
                                          <label for="email_id">Service Type</label>
                                              <div class="mb-3">
                                                  <select class="form-control show-tick ms select2" name="service_type[]" id="serv_type" multiple data-placeholder="Select"  required="">
                                                     <option value=""></option>
                                                    <?php if(count($servicetypes)>0){
                                                      foreach($servicetypes as $key => $value){?>
                                                    <option value="<?=$value['service_type']?>"><?=$value['service_type']?></option>
                                                    <?php } }?>
                                                  </select>
                                              </div>
                                       </div> 

                                       
                                        </div> 
                                        <div class="col-md-6">
                                        
                                         <div class="form-group">
                                            <label for="email_id">EmailId</label>
                                            <input type="email" name="email_id" id="email_id" class="form-control" required >
                                        </div> 
                                       <!--  <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>
                                    
                                        </div> --> 
                                        
                                         <div class="form-group">
                                            <label>Select Category</label>
                                            <select name="selectcat[]" id="selectcat" required class="form-control show-tick ms select2" multiple data-placeholder="Select"  >
                                              <option value="">Select Option</option>
                                           <?php  foreach ($fetch_data as $cat) {  ?>
                                              <option value="<?= $cat['id'] ?>" ><?= $cat['category_name'] ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                       
                                         <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" id="address" rows="5" cols="30" required></textarea>
                                        </div> 
                                                                                   
                                        </div>
                                      </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                            </div>
                        </form>
                        </div>

                    </div>
                </div>



                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>EmailId</th>
                                    <!-- <th>Address</th> -->
                                    <th>Mobile</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                     <th>Name</th>
                                    <th>EmailId</th>
                                    <th>Mobile</th>
                                    <th></th>
                                </tr>
                            </tfoot> -->
                            <tbody>
                                <?php 
                                if(count($vendors)>0){
                                    foreach ($vendors as $key => $value) {
                                ?>
                                <tr  id="<?=$value['id']?>">
                                    <td><?php echo $value['name'];?></td>
                                    <td><?php echo $value['email'];?></td>
                                   <!--  <td><?php echo $value['address'];?></td> -->
                                    <td><?php echo $value['mobile'];?></td>
                                    <td class="actions"><a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut<?=$value['id']?>"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="Remove" data-id="<?php echo $value['id']?>"><i class="icon-trash" aria-hidden="true"></i> </a> | <a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut-editcustomer<?=$value['id']?>"><span class="fa fa-pencil"></span></a></td>
                                  
                                </tr>


                                 <div class="modal modal-defalut fade" id="modal-defalut<?=$value['id']?>">

                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                 <h4 class="modal-title">View Vendor Details</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               
                                            </div>
                                       <!--  <form id="" method="post" action="#" >  -->
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                       <!--  <div class="header">
                                                            <h2>View Vendor Details</h2>
                                                        </div> -->
                                                        <div class="body">
                                                        <div class="form-group">
                                                            <label>Vendor Name</label>
                                                            <input type="text"  class="form-control" value="<?php echo $value['name']?>" readonly>
                                                        </div>                              

                                                        <div class="form-group">
                                                            <label for="email_id">EmailId</label>
                                                            <input type="email"  class="form-control" value="<?php echo $value['email']?>"  readonly>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea class="form-control" rows="5" cols="30" readonly><?php echo $value['address']?></textarea>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Mobile</label>
                                                            <input type="text"  class="form-control" placeholder=""  step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" readonly value="<?php echo $value['mobile']?>" />
                                                        </div>

                                                         <div class="form-group">
                                                            <?php if($value['category']!=''){
                                                                //print_r();
                                                                $arr=explode(',',$value['category']);
                                                                $cate_name=array();
                                                                foreach ($arr as $key4 => $value4) {
                                                                    $cat = $this->db->get_where('category', array('id' => $value4))->result_array();
                                                                   
                                                                    if(count($cat)>0){
                                                                        $cate_name[]=$cat[0]['category_name'];
                                                                    }

                                                                }

                                                                $cate_names=implode(',',$cate_name);

                                                               
                                                                
                                                            }?>
                                                            <label>Category</label>
                                                            <input type="text"  class="form-control" value="<?php echo $cate_names;?>" readonly>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Service</label>
                                                            <input type="text"  class="form-control" value="<?php echo $value['service_type']?>" readonly>
                                                        </div>     

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                               <!--  <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button> -->
                                            </div>
                                       <!--  </form> -->
                                        </div>

                                    </div>
                                </div>
                                

                                                  <!-- Modal -->
                                <div class="modal modal-defalut fade" id="modal-defalut-editcustomer<?=$value['id']?>">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title">Edit Vendor Details</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                
                                            </div>
                                        <form id="customer-form-edit-<?=$value['id']?>" method="post" action="#" >
                                        <input type="hidden" name="vendor_id" value="<?=$value['id']?>" /> 
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <!-- <div class="header">
                                                            <h2>Vendor Edit</h2>
                                                        </div> -->
                                                        <div class="body">
                                                            <div class="form-group">
                                                            <label>User Name</label>
                                                            <input type="text" name="customer_name" id="customer_<?=$value['id']?>" class="form-control" value="<?php echo $value['name']?>" required>
                                                        </div>                              

                                                        <div class="form-group">
                                                            <label for="email_id">EmailId</label>
                                                            <input type="email" name="email_id" id="email_id" class="form-control" value="<?php echo $value['email']?>" required readonly>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea class="form-control" name="address" id="address_<?=$value['id']?>" rows="5" cols="30" required><?php echo $value['address']?></textarea>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Mobile</label>
                                                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder=""  step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" readonly value="<?php echo $value['mobile']?>" />
                                                        </div>  

                                                        <div class="form-group">
                                                            <?php  if($value['category']!=''){
                                                                        $category=explode(',',$value['category']);
                                                                    }
                                                                    ?>
                                                            <label>Select Category</label>
                                                            <select name="selectcat[]" id="selectcat_<?=$value['id']?>" required class="form-control show-tick ms select2" multiple data-placeholder="Select">
                                                              <!-- <option value="">Select Option</option> -->
                                                           <?php  foreach ($fetch_data as $cat) {  ?>
                                                              <option value="<?= $cat['id'] ?>" <?php if(in_array($cat['id'],$category)){?> selected <?php }?> ><?= $cat['category_name'] ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                        <?php $servicetype=array();
                                                        if($value['service_type']!=''){
                                                            $servicetype=explode(',',$value['service_type']);
                                                        }
                                                        //print_r('aaaa'.$value['service_type']); ?>
                                                        <div class="form-group" >
                                                          <label for="email_id">Service Type</label>
                                                              <div class="mb-3">
                                                                  <select class="form-control show-tick ms select2" name="service_type[]" id="serv_type_<?=$value['id']?>" multiple data-placeholder="Select"  required="">
                                                                     <option value=""></option>
                                                                    <?php if(count($servicetypes)>0){
                                                                      foreach($servicetypes as $keys => $values){

                                                                        ?>
                                                                    <option value="<?=$values['service_type']?>" <?php if(in_array($values['service_type'],$servicetype)){?> selected <?php }?>  ><?=$values['service_type']?></option>
                                                                    <?php } }?>
                                                                  </select>
                                                              </div>
                                                       </div> 

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                                <button type="submit"  class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                                            </div>
                                        </form>
                                        </div>

                                    </div>
                                </div>

                                <script>
                                  $(document).ready(function(){  
                    $('#customer-form-edit-<?=$value['id']?>').parsley();

                  //  $('#customer-form-edit-<?=$value['id'];?>').parsley().destroy();


                    $('#customer-form-edit-<?=$value['id'];?>').parsley().on('form:submit', function() {

                        var form_data=$("#customer-form-edit-<?=$value['id']?>").serialize();
                       //alert(form_data);
                        var url1='<?php echo $url;?>';
                        $.ajax({

                            type: "POST",
                            url: url1+"/update_vendor",
                            data: form_data, 
                            success: function(data)
                            {
                               // alert(data);
                                $('.parsley-required').remove();

                                if(data==1){
                                    $("#customer_<?=$value['id'];?>").focus();
                                    $("#customer_<?=$value['id'];?>").removeClass('parsley-success');
                                    $("#customer_<?=$value['id'];?>").addClass('parsley-error');
                                    $("#customer_<?=$value['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    return false;
                                }
                                else if(data == 2){
                                    $("#address_<?=$value['id'];?>").focus();
                                    $("#address_<?=$value['id'];?>").removeClass('parsley-success');
                                    $("#address_<?=$value['id'];?>").addClass('parsley-error');
                                    $("#address_<?=$value['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#address_<?=$value['id'];?>").focus();
                                    return false;
                                }
                                else if(data == 4){
                                    $("#serv_type_<?=$value['id'];?>").focus();
                                    $("#serv_type_<?=$value['id'];?>").removeClass('parsley-success');
                                    $("#serv_type_<?=$value['id'];?>").addClass('parsley-error');
                                    $("#serv_type_<?=$value['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                    $("#serv_type_<?=$value['id'];?>").focus();
                                    return false;
                                }
                                if(data==3){
                                   swal("Success!", "Your file has been updated.", "success");
                                   location.reload();
                                }
                                else if(data == 0){
                                    swal("Failure!", "Your imaginary file has been deleted.", "success");
                                    location.reload();
                                }
                               
                            }

                        }); 
                        return false; // Don't submit form for this demo
                    });
 
                                       /* $('#customer-form-edit<?=$value['id']?>').on('submit', function(event){
                                        event.preventDefault();
                                        if($('#customer-form-edit<?=$value['id']?>').parsley().isValid())
                                        {
                                        var url1='<?php echo $url;?>';
                                        var form_data=$("#customer-form-edit<?=$value['id']?>").serialize();
                                        $.ajax({
                                        type: "POST",
                                        url: url1+"/update_vendor",
                                        data: form_data, 
                                        success: function(data)
                                        {

                                        location.reload();
                                        }

                                        }); 
                                        }
                                        });*/
                                });  
                            
                        
                                </script>
                                
                               <?php } }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



 <script type="text/javascript">

    $(function() {

        $('.select2').select2();
        // validation needs name of the element
        $('#privilege').multiselect();
        var url1='<?php echo $url;?>';
        // initialize after multiselect
        $('#customer-form').parsley();

        $('#customer-form').parsley().on('form:submit', function() {
            var form_data=$("#customer-form").serialize();
            $.ajax({

                type: "POST",
                url: url1+"/create_vendor",
                data: form_data, 
                success: function(data)
                {//alert(data);
                    if(data==1){
                       
                        $("#email_id").focus();
                        $("#email_id").removeClass('parsley-success');
                        $("#email_id").addClass('parsley-error');
                        $("#email_id").parsley().addError('myError', {message: 'Email Id Already Registered'});
                        return false;
                    }
                    else if(data == 2){
                        $("#mobile").focus();
                        $("#mobile").removeClass('parsley-success');
                        $("#mobile").addClass('parsley-error');
                        $("#mobile").parsley().addError('myError', {message: 'Mobile Number Already Registered..!'});
                        $("#mobile").focus();
                        return false;
                    }
                    else if(data == 3){
                      location.reload();
                    }
                   
                }

            }); 
            return false; // Don't submit form for this demo
        });
    });


    $('.button-remove').on('click', function(){
        var url1='<?php echo $url;?>';
        var customer_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");
        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
                $.ajax({

                    type: "POST",
                    url: url1+"/delete_vendor",
                    data: { customer_id:customer_id }, 
                    success: function(data)
                    {
                        if(data==1){
                            $("#"+id).remove();
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            
                            //location.reload();
                        }
                    }
                });
            });
    });
       


    

    </script>


