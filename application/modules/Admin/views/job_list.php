<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';
//echo $this->config->item('base_url').'uploads/1.png';

$followup_count=count($followup_list);
$completed_count=count($completed_list);
//print_r('aaaaa');exit;
?> 


<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Job Lists</h2>

                <div id="exTab1" class="container" class="row" style="margin-bottom:30px;"> 
                  <ul  class="nav nav-tabs tab-basic" role="tablist" style="width:100%;">
                    <li class="nav-item"><a class="nav-link <?php if($type=='followup') { ?> active show <?php } ?>" href="<?=$url;?>/job_list/<?=base64_encode(base64_encode('1'))?>"  >Followup List (<?=$followup_count;?>)</a></li>
                    <li class="nav-item"><a class="nav-link <?php if($type=='completed') { ?> active <?php } ?>" href="<?=$url;?>/job_list/<?=base64_encode(base64_encode('2'))?>"  >Completed List (<?=$completed_count?>)</a></li>
                  </ul>
                </div>

                
            </div> 
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">

                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <!-- <th><input type="checkbox" class="checkAll" name=""></th> -->
                                    <th>Job Id</th>
                                    <th>Status</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile</th>
                                    <th>Customer Address</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Mobile</th>
                                    <th>Comments</th>
                                    
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Job Id</th>
                                    <th>Status</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile</th>
                                    <th>Customer Address</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Mobile</th>
                                    <th>Comments</th>
                                    
                                    <!-- <th></th> -->
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php 
                                if(count($job_list)>0){
                                    foreach ($job_list as $key => $value) {
                                       // echo '<pre>';print_r($value);
                                ?>
                                <tr>
                                    <td><?php echo $value['job_ref'];?></td>
                                     <td>
                                        <?php if($value['job_status'] == 'Followup'){?>
                                        <a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>" class="btn btn-danger"><?php echo $value['job_status'];?></a><?php } else if($value['job_status'] == 'Completed'){?>
                                        <a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>" class="btn btn-success"><?php echo $value['job_status'];?></a><?php }?></td>
                                    <td><?php echo $value['customer_name'];?></td>
                                    <td><?php echo $value['customer_mobile'];?></td>
                                    <td><?php echo $value['customer_address'];?></td>
                                    <td><?php echo $value['vendor_name'];?></td>
                                    <td><?php echo $value['vendor_mobile'];?></td>
                                    <td><?php echo $value['commands'];?></td>
                                </tr>

                                <div class="modal modal-defalut fade" id="modal-defalut<?=$value['job_service_id']?>">

                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                 <h4 class="modal-title">View Service Details</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               
                                            </div>
                                       <!--  <form id="" method="post" action="#" >  -->
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <div class="header">
                                                            <!-- <h2>View Job Details</h2> -->
                                                        </div>
                                                        <div class="body">

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Vendor Name</label></div>
                                                            <div class="col-md-6"><?php echo $value['vendor_name'];?></div>
                                                        </div>

                                                         <div class="row">
                                                            
                                                            <div class="col-md-6"> <label>Vendor Image</label></div>
                                                            <div class="col-md-6"><img height="100" width="100" src="<?= $this->config->item('base_url') ."vendor/".$value['vendor_id'].'/'.$value['vendor_photo'] ;?>"></div>
                                                        </div>

                                                        

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Customer Name</label></div>
                                                            <div class="col-md-6"><?php echo $value['customer_name'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Customer Address</label></div>
                                                            <div class="col-md-6"><?php echo $value['customer_address'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Product Category</label></div>
                                                            <div class="col-md-6"><?php echo $value['category_name'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Service Type</label></div>
                                                            <div class="col-md-6"><?php echo $value['service'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Service Cost</label></div>
                                                            <div class="col-md-6"><?php echo $value['amount'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Comments</label></div>
                                                            <div class="col-md-6"><?php echo $value['commands'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Level</label></div>
                                                            <div class="col-md-6"><?php echo $value['level'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Date</label></div>
                                                            <div class="col-md-6"><?php echo $value['date'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Time</label></div>
                                                            <div class="col-md-6"><?php echo $value['time'];?></div>
                                                        </div>

                                                        <?php if($value['status']==7){?>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Date</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_date'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Time</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_time'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Comments</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_comments'];?></div>
                                                        </div>
                                                        <?php }?>


                                                         
                                                           

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                               <!--  <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button> -->
                                            </div>
                                       <!--  </form> -->
                                        </div>

                                    </div>
                                </div>
                                
                               <?php } }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





   