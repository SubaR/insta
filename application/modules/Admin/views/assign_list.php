<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';

$followup_count=count($followup_list);
$completed_count=count($completed_list);
$inprogress_count=count($inprogress_list);

?> 
<style type="text/css">
   [type=checkbox]:checked, [type=checkbox]:not(:checked){
    position: static;
    opacity: 1;
   }

.outer
{
    width:100%;
    /*text-align: center;*/
}
.inner
{
    display: inline-block;
}
#exTab1 {width:max-content;}

/*.size_vendor{
    width: 351px !important;
}*/
</style>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Job Assigned List</h2>

                <div id="exTab1" class="container" class="row" style="margin-bottom:30px;"> 
                  <ul  class="nav nav-tabs tab-basic" role="tablist" style="width:100%;">
                    <li class="nav-item"><a class="nav-link <?php if($type=='followup') { ?> active show <?php } ?>" href="<?=$url;?>/assign_list/<?=base64_encode(base64_encode('1'))?>"  >Followup List (<?=$followup_count;?>)</a></li>

                    <li class="nav-item"><a class="nav-link <?php if($type=='inprogress') { ?> active <?php } ?>" href="<?=$url;?>/assign_list/<?=base64_encode(base64_encode('3'))?>"  >Inprogress List (<?=$inprogress_count?>)</a></li>

                    <li class="nav-item"><a class="nav-link <?php if($type=='completed') { ?> active <?php } ?>" href="<?=$url;?>/assign_list/<?=base64_encode(base64_encode('2'))?>"  >Completed List (<?=$completed_count?>)</a></li>
                  </ul>
                </div>
                
            </div> 
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <form method="POST" action="<?=$url;?>/service_export" class="date_size">
                    
                   <table border="0" cellspacing="5" cellpadding="5">
                        <tbody>
                            <tr>
                             <!--    <td>
                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                        <input type="text" name="from" class="form-control">
                                        <div class="input-group-append">                                            
                                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                        <input type="text" name="to" class="form-control">
                                        <div class="input-group-append">                                            
                                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </td> -->

                                <td>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                        <input type="text" class="form-control" name="start_date" autocomplete="off">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <input type="hidden" name="export_type" value="<?=$type?>">
                                    <div class="form-group">
                                        <label>End Date</label>
                                         <!--  <input type="text" name="end_date" id="end_date" class="form-control del_space" required placeholder=""> -->

                                        <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                            <input type="text" class="form-control" name="end_date" autocomplete="off">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td> 

                                    <button type="submit" style="background-color: #5983e8;color: #FFF;margin-top: 0.6rem;" class="btn btn-default mg-r-5">Export</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <!-- <th><input type="checkbox" class="checkAll" name=""></th> -->
                                    <th>Job Id</th>
                                    <th>Level</th>
                                    <th>Status</th>
                                    <th>Assigned to </th>
                                    <th>ReAssigned to </th>
                                    <th>Customer Name</th>
                                    <th>Product Category</th>
                                    <th>Service</th>
                                    <th>Brand Name</th>
                                    <th>Service Cost</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                           <!--  <tfoot>
                                <tr>
                                    
                                    <th>Job Id</th>
                                    <th>Status</th>
                                    <th>Level</th>
                                    <th>Assigned To</th>
                                    <th>ReAssigned to </th>
                                    <th>Customer Name</th>
                                    <th>Product Category</th>
                                    <th>Service</th>
                                    <th>Brand Name</th>
                                    <th>Service Cost</th>
                                    <th></th>
                                    
                                </tr>
                            </tfoot> -->
                            <tbody>
                                <?php 
                                if(count($job_list)>0){
                                    $i=0;
                                    foreach ($job_list as $key => $value) {
                                        //echo '<pre>';print_r($value);
                                ?>
                                <tr>
                                   
                                    <td><?php  
                                    echo $value['job_ref'];?></td>
                                    <td>
                                        <?php if($value['level']=='Medium'){?><button type="button" class="btn btn-outline-warning"><?=$value['level'];?></button><?php } else if($value['level']=='Low'){?> <button type="button" class="btn btn-outline-primary"><?=$value['level'];?></button><?php }else if($value['level']=='High'){?> <button type="button" class="btn btn-outline-info"><?=$value['level'];?></button><?php }?>
                                    </td>


                                    <td> <?php if($value['status']==1 ){?> 
                                        <a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>"><span style="background:#c0392b;color:#fff;padding:3px;">Accepted</span> </a><?php } else if($value['status']==2 || $value['status']==4 ){?> <a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>"><span style="background:#f3ad06;color:#fff;padding:3px;">Inprogress</span></a><?php } else if($value['status']==5){?><a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>"><span style="background:#2ecc71;color:#fff;padding:3px;">Completed</span></a> <?php } else if($value['status']==7){?> <a href="#" data-toggle="modal" data-target="#modal-defalut<?=$value['job_service_id']?>"><span style="background:#c82333;color:#fff;padding:3px;">Followup</span></a> <?php }?> </td>



                                     <?php 
                                    $vendor_name='';
                                    $vendor=$this->Category_model->getvendor($value['vend_id']);
                                    if(count($vendor)>0){
                                        $vendor_name=$vendor[0]['name'];
                                    }

                                    $revendor_name='';
                                    $revendor=$this->Category_model->getvendor($value['reassign_vendor_id']);
                                    if(count($revendor)>0){
                                        $revendor_name=$revendor[0]['name'];
                                    }

                                    ?>
                                    <td> <?=$vendor_name;?></td>
                                     <td><?=$revendor_name;?></td>
                                    <td><?php echo $value['customer_name'];?></td>

                                     <?php $category_name=$this->Category_model->getcategory($value['category_id']);
                                        $categoryname='';
                                        if(count($category_name)>0){
                                            $categoryname=$category_name[0]['category_name'];
                                        }
                                       
                                    ?>
                                    <td><?php echo $categoryname;?></td>
                                    <td><?php echo $value['service'];?></td>
                                    <td><?php echo $value['brand_name'];?></td>
                                     <td><?php echo $value['amount'];?></td>
                                    <td><a href="<?=$url;?>/view_invoice/<?=base64_encode(base64_encode($value['job_service_id']));?>" class="btn"> <i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                  
                                </tr>

                                <div class="modal modal-defalut fade" id="modal-defalut<?=$value['job_service_id']?>">

                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                 <h4 class="modal-title">View Service Details</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               
                                            </div>
                                       <!--  <form id="" method="post" action="#" >  -->
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <div class="header">
                                                            <!-- <h2>View Job Details</h2> -->
                                                        </div>
                                                        <div class="body">

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Vendor Name</label></div>
                                                            <div class="col-md-6"><?php echo $value['vendor_name'];?></div>
                                                        </div>

                                                         <div class="row">
                                                            
                                                            <div class="col-md-6"> <label>Vendor Image</label></div>
                                                            <div class="col-md-6"><img height="100" width="100" src="<?= $this->config->item('base_url') ."vendor/".$value['vendor_id'].'/'.$value['vendor_photo'] ;?>"></div>
                                                        </div>

                                                        

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Customer Name</label></div>
                                                            <div class="col-md-6"><?php echo $value['customer_name'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Customer Address</label></div>
                                                            <div class="col-md-6"><?php echo $value['customer_address'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Product Category</label></div>
                                                            <div class="col-md-6"><?php echo $value['category_name'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Service Type</label></div>
                                                            <div class="col-md-6"><?php echo $value['service'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Service Cost</label></div>
                                                            <div class="col-md-6"><?php echo $value['amount'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Brand Name</label></div>
                                                            <div class="col-md-6"><?php echo $value['brand_name'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Comments</label></div>
                                                            <div class="col-md-6"><?php echo $value['commands'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Level</label></div>
                                                            <div class="col-md-6"><?php echo $value['level'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Date</label></div>
                                                            <div class="col-md-6"><?php echo $value['date'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Time</label></div>
                                                            <div class="col-md-6"><?php echo $value['time'];?></div>
                                                        </div>

                                                        <?php if($value['status']==7){?>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Date</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_date'];?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Time</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_time'];?></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6"> <label>Followup Comments</label></div>
                                                            <div class="col-md-6"><?php echo $value['followup_comments'];?></div>
                                                        </div>
                                                        <?php }?>


                                                         
                                                           

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                               <!--  <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button> -->
                                            </div>
                                       <!--  </form> -->
                                        </div>

                                    </div>
                                </div>
                                
                               <?php $i++;} }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





   <script type="text/javascript">
     $('.dataTable').DataTable({
    "ordering": false
});
    $('.date').datepicker({
          autoclose: true,
          format: "dd-mm-yyyy",
          /*todayHighlight: true*/
    })
    /*$(document).ready(function(){
        var table = $('#table_id').DataTable({
            "bSort": false
        });
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
          
            var startDate = new Date(data[3]); 
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) { return true;}
            if (startDate >= min && startDate <= max  ) { return true; }

            return false;
        }
        );

        $("#min").datepicker({ onSelect: function () { 
            table.draw();
             }, changeMonth: true, changeYear: true,format: 'yyyy-mm-dd' });

            $("#max").datepicker({ onSelect: function () {
                table.draw(); 
            }, changeMonth: true, changeYear: true,format: 'yyyy-mm-dd' });
          
        
            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                table.draw(); 
            });
   });*/
    




</script>
