<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$url=$this->config->item('base_url').'Admin';

?>
 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-sm-12">                        
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Customer List</h2>
               <!--  <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                    <li class="breadcrumb-item">Table</li>
                    <li class="breadcrumb-item active">Jquery Datatable</li>
                </ul> -->
            </div> 
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add User</button>                           
                </div>



                  <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Customer Creation</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                
                            </div>
                        <form id="customer-form" method="post" action="#" > 
                            <div class="modal-body">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <!-- <div class="header">
                                            <h2>Customer Creation</h2>
                                        </div> -->
                                        <div class="body">
                                            <div class="form-group">
                                            <label>User Name</label>
                                            <input type="text" name="customer_name" id="customer_name" class="form-control" required>
                                        </div>                              

                                        <div class="form-group">
                                            <label for="email_id">EmailId</label>
                                            <input type="email" name="email_id" id="email_id" class="form-control" required >
                                        </div>  
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" id="address" rows="5" cols="30" required></textarea>
                                        </div>  
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder="" required step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" maxlength="10" />
                                        </div>  
                                       <!--  <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>
                                    
                                        </div> 
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Confirm Password *" name="confirm_password" data-parsley-equalto="#password" required>
                                        </div> -->                              

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                            </div>
                        </form>
                        </div>

                    </div>
                </div>



                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>EmailId</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                     <th>Name</th>
                                    <th>EmailId</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th></th>
                                </tr>
                            </tfoot> -->
                            <tbody>
                                <?php 
                                if(count($customer)>0){
                                    foreach ($customer as $key => $value) {
                                ?>
                                <tr id="<?=$value['id']?>">
                                    <td><?php echo $value['name'];?></td>
                                    <td><?php echo $value['email'];?></td>
                                    <td><?php echo $value['address'];?></td>
                                    <td><?php echo $value['mobile'];?></td>
                                    <td class="actions"><a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut<?=$value['id']?>"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="Remove" data-id="<?php echo $value['id']?>"><i class="icon-trash" aria-hidden="true"></i> </a> | <a href="#" class="btn btn-link" data-toggle="modal" data-target="#modal-defalut-editcustomer<?=$value['id']?>"><span class="fa fa-pencil"></span></a></td>
                                  
                                </tr>
                                <div class="modal modal-defalut fade" id="modal-defalut<?=$value['id']?>">

                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                 <h4 class="modal-title">View User Details</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               
                                            </div>
                                       <!--  <form id="" method="post" action="#" >  -->
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                        <!-- <div class="header">
                                                            <h2>View User Details</h2>
                                                        </div> -->
                                                        <div class="body">
                                                            <div class="form-group">
                                                            <label>User Name</label>
                                                            <input type="text"  class="form-control" value="<?php echo $value['name']?>" readonly>
                                                        </div>                              

                                                        <div class="form-group">
                                                            <label for="email_id">EmailId</label>
                                                            <input type="email"  class="form-control" value="<?php echo $value['email']?>"  readonly>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea class="form-control" rows="5" cols="30" readonly><?php echo $value['address']?></textarea>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Mobile</label>
                                                            <input type="text"  class="form-control" placeholder=""  step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" readonly value="<?php echo $value['mobile']?>" />
                                                        </div>  

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                               <!--  <button type="submit" id="" class="btn btn-bold btn-pure btn-primary float-right">Submit</button> -->
                                            </div>
                                       <!--  </form> -->
                                        </div>

                                    </div>
                                </div>

                                                  <!-- Modal -->
                                <div class="modal modal-defalut fade" id="modal-defalut-editcustomer<?=$value['id']?>">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                 <h4 class="modal-title">Edit Customer</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                               
                                            </div>
                                        <form id="customer-form-edit-<?=$value['id'];?>" method="post" action="#" > 
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="card">
                                                       <!--  <div class="header">
                                                            <h2>Customer Edit</h2>
                                                        </div> -->
                                                        <div class="body">
                                                            <div class="form-group">
                                                            <label>User Name</label>
                                                            <input type="text" name="customer_name" id="customer_<?=$value['id']?>"  class="form-control" value="<?php echo $value['name']?>" required>
                                                        </div>                              

                                                        <div class="form-group">
                                                            <label for="email_id">EmailId</label>
                                                            <input type="email" name="email_id"  class="form-control" value="<?php echo $value['email']?>" required readonly>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea class="form-control" id="address_<?=$value['id']?>" name="address"  rows="5" cols="30" required><?php echo $value['address']?></textarea>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Mobile</label>
                                                            <input type="text" name="mobile"  class="form-control" placeholder=""  step="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" readonly value="<?php echo $value['mobile']?>" />
                                                        </div>  

                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="customer_id" value="<?=$value['id']?>">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-bold btn-pure btn-primary float-right">Update</button>
                                            </div>
                                        </form>
                                        </div>

                                    </div>
                                </div>
                                <script type="text/javascript">
                                     $(function() {
                                         
                                        $('#customer-form-edit-<?=$value['id'];?>').parsley();
                                        $('#customer-form-edit-<?=$value['id'];?>').parsley().destroy();
                                        $('#customer-form-edit-<?=$value['id'];?>').parsley().on('form:submit', function() {

                                            var form_data=$("#customer-form-edit-<?=$value['id'];?>").serialize();
                                            var url1='<?php echo $url;?>';
                                            $.ajax({

                                                type: "POST",
                                                url: url1+"/update_customer",
                                                data: form_data, 
                                                success: function(data)
                                                {
                                                    $('.parsley-required').remove();

                                                    if(data==1){
                                                        $("#customer_<?=$value['id'];?>").focus();
                                                        $("#customer_<?=$value['id'];?>").removeClass('parsley-success');
                                                        $("#customer_<?=$value['id'];?>").addClass('parsley-error');
                                                        $("#customer_<?=$value['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                                        return false;
                                                    }
                                                    else if(data == 2){
                                                        $("#address_<?=$value['id'];?>").focus();
                                                        $("#address_<?=$value['id'];?>").removeClass('parsley-success');
                                                        $("#address_<?=$value['id'];?>").addClass('parsley-error');
                                                        $("#address_<?=$value['id'];?>").parsley().addError('required', {message: 'This value is required'});
                                                        $("#address_<?=$value['id'];?>").focus();
                                                        return false;
                                                    }
                                                    if(data==3){
                                                       swal("Success!", "Your file has been updated.", "success");
                                                       location.reload();
                                                    }
                                                    else if(data == 0){
                                                        swal("Failure!", "Your imaginary file has been deleted.", "success");
                                                        location.reload();
                                                    }
                                                   
                                                }

                                            }); 
                                            return false; // Don't submit form for this demo
                                        });
                                    });
                                </script>

                                
                               <?php } }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <script>
    $(function() {
        // validation needs name of the element
        $('#privilege').multiselect();
        var url1='<?php echo $url;?>';
        // initialize after multiselect
        $('#customer-form').parsley();

        $('#customer-form').parsley().on('form:submit', function() {
            var form_data=$("#customer-form").serialize();
            $.ajax({

                type: "POST",
                url: url1+"/create_customer",
                data: form_data, 
                success: function(data)
                {
                    $('.parsley-required').remove();
                    if(data==1){
                       
                        $("#email_id").focus();
                        $("#email_id").removeClass('parsley-success');
                        $("#email_id").addClass('parsley-error');
                        $("#email_id").parsley().addError('required', {message: 'Email Id Already Registered'});
                        return false;
                    }
                    else if(data == 2){
                        $("#mobile").focus();
                        $("#mobile").removeClass('parsley-success');
                        $("#mobile").addClass('parsley-error');
                        $("#mobile").parsley().addError('required', {message: 'Mobile Number Already Registered..!'});
                        return false;
                    }
                    else if(data == 3){
                      location.reload();
                    }
                   
                }

            }); 
            return false; // Don't submit form for this demo
        });
  
    });


    


    $('.button-remove').on('click', function(){
        var url1='<?php echo $url;?>';
        var customer_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
           
            $.ajax({

                type: "POST",
                url: url1+"/delete_customer",
                data: {customer_id:customer_id}, 
                success: function(data)
                {
                    if(data==1){
                        $("#"+id).remove();
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        
                    }
                }
            });
        });
    })
       


    

    </script>



   