<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    //print_r($users);exit;
?>
<div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?=ucfirst($users['name']);?> Profile</h2>
                        
                    </div>            
                    
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <ul class="nav nav-tabs">                                
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Settings">Profile Details</a></li>
                                
                                
                            </ul>
                        </div>
                        <div class="tab-content">
    
                            <div class="tab-pane active" id="Settings">

                               

                                <div class="body">
                                    <h6>Basic Information</h6>
                                    <form action="<?=$base_url;?>/updateprofile" method="post" id="ProfileForm">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">                                                
                                                <input type="text" class="form-control" name="name" value="<?=$users['name']?>" required>
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="text" class="form-control" name="email" value="<?=$users['email']?>" required>
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="text" class="form-control" name="mobile" value="<?=$users['mobile']?>" required>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button> 
                                </form>
                                </div>


                                <div class="body">
                                    
                                    <form action="<?=$base_url;?>/changepassword" method="post" id="Changepassword">
                                    
                                    <div class="col-lg-6 col-md-12">
                                            <h6>Change Password</h6>
                                            <div class="form-group">
                                                <input type="password" id="password" class="form-control" name="password" placeholder="New Password" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="cpassword" placeholder="Confirm New Password" required data-parsley-equalto="#password">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Change Password</button> 
                                        </div>
                                    </form>
                                </div>

                                

                                
                            </div>
    
                           
    
                            
    
                        </div>
                    </div>
                </div>
            </div>
</div>
    

    <script>
    $(function() {
        $('#ProfileForm').parsley();
        $('#Changepassword').parsley();
    });
    </script>



