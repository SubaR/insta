<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/Admin_template/';
$url=$this->config->item('base_url').'Admin';
$home_url=$this->config->item('base_url');
$this->load->model('Admin_model');
$lang=$this->session->userdata('language');

?>

<style type="text/css">
	.tab-basic .nav-item .nav-link {
		color: #0a0a0a;
	}
	.nocontact{
		margin: 60% 70% 50% 24% !important;
		color: white;
	}
	.nomessage{
		margin: 34% 34% 33% 35% !important;
	}
	.msg_time_receive{
		position: absolute;
		bottom: -15px;
		color: rgba(255,255,255,0.5);
		font-size: 10px;
		width: max-content;
	}
	.msg_cotainer_group{
		margin-top: auto;
		margin-bottom: auto;
		margin-left: 10px;
		border-radius: 25px;
		background-color: #7F7ED5;
		padding: 10px;
		margin-left: 35%;
		left: 50%;
		font-weight: 700;
	}

</style>
	<?php //print_r($customer);
	//$customer[0]['id']=9;

	
		$messages=$this->Admin_model->get_message($customer[0]['id']);
		$photo=$home_url.'/uploads/user.png';
		if($customer[0]['photo']!=''){
			$photo=$home_url.'/vendor/'.$customer[0]['id'].'/'.$customer[0]['photo'];
		}
		?>

		<?php 
			$admin = $this->db->get('admin')->result_array();
			$sender_id=$admin[0]['id'];
			$this->load->helper('date');
	        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
			$cur_datetime = $date->format('Y-m-d H:i A');
			$cur_date=$date->format('Y-m-d');
		?>
		<input type="hidden" name="receiver_id" id="receiver_id" value="<?=$customer[0]['id']?>">
		<?php $i=0;foreach($messages as $key1 => $value1){

			$message=$this->Admin_model->get_message($customer[0]['id'],$value1['date']);


			if($i== $key1 && $cur_date == $value1['date']){?>
				<span class="msg_cotainer_group"> <?php echo $dt='Today';?></span>
			<?php }
			else if($i== $key1){?>
				<span class="msg_cotainer_group"> <?php echo $dt=date('F d, Y',strtotime($value1['date']));?></span>
				
			<?php }
		
			foreach ($message as $key3 => $value3) {


			$time=date('h:i A',strtotime($value3['date_time']));
			if($value3['type']=='user'){?>
		<div class="d-flex justify-content-start mb-4" id="<?=$value1['date'].'_'.$key3;?>">
			<span class="img_cont_msg">
				<img src="<?=$photo;?>" class="rounded-circle user_img_msg">
			</span>
			<span class="msg_cotainer">
				<?=$value3['message'];?>
				<span class="msg_time_receive"><?=$time;?>, <?php echo $this->Admin_model->timeago($value3['timestmp']);?></span>
			</span>
		</div>
	<?php }?>
		
		<?php if($value3['type']=='admin'){?>
		<div class="d-flex justify-content-end mb-4" id="<?=$value1['date'].'_'.$key3;?>">
			<span class="msg_cotainer_send">
				<?=$value3['message'];?>
				<span class="msg_time_send"><?=$time;?>, <?php echo $this->Admin_model->timeago($value3['timestmp']);?></span>
			</span>
			<span class="img_cont_msg">
		<img src="<?=$home_url?>/uploads/user.png" class="rounded-circle user_img_msg">
			</span>
		</div>
		<?php }?>
		<?php }?>
		<?php $i++;}?>

	
<?php //}?>


