<?php
    $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
    $base_url=$this->config->item('base_url').'Admin'; 
    $image_url=$this->config->item('base_url').'uploads';
?>

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/select2/select2.css" />
<script src="<?=$theme_path;?>assets/vendor/select2/select2.min.js"></script> 
<div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-8 col-sm-12">                        
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Sub Category Details List</h2>
                    <!-- <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a></li>   
                        <li class="breadcrumb-item active">View Page</li>
                    </ul> -->
                </div>            
              
            </div>
        </div>
    
         <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="header">
                            <h2><small><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Sub Category</button></small> </h2>                            
                        </div>

                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Sub Category Creation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                          </div>
                           <form id="add-form" method="post" action="#" novalidate enctype='multipart/form-data'> 
                              <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <!-- <div class="header">
                                                    <h2>Sub Category Creation</h2>
                                                </div> -->
                                                <div class="body">

                                                      <div class="form-group">
                                                        <label>Select Category</label>
                                                        <select name="selectcat" id="selectcat" required class="form-control">
                                                          <option value="">Select Option</option>
                                                       <?php  foreach ($fetch_data as $cat) {  ?>
                                                          <option value="<?= $cat['id'] ?>" ><?= $cat['category_name'] ?></option>
                                                        <?php } ?>
                                                        </select>
                                                        </div>  

                                                        <div class="form-group">
                                                            <label>Sub Category Name</label>
                                                            <input type="text" name="subtitle" id="subtitle" class="form-control" required>
                                                        </div> 

                                                        
                                                          <div class="form-group" >
                                                              <label for="email_id">Service Type</label>
                                                                  <div class="mb-3">
                                                                      <select class="form-control show-tick ms select2" name="service_type[]" id="serv_type" multiple data-placeholder="Select"  required="">
                                                                         <option value=""></option>
                                                                        <?php if(count($servicetypes)>0){
                                                                          foreach($servicetypes as $key => $value){?>
                                                                        <option value="<?=$value['service_type']?>"><?=$value['service_type']?></option>
                                                                        <?php } }?>
                                                                      </select>
                                                                  </div>
                                                          </div> 
                                                     
                                                   
                                                         <div class="form-group">
                                                            <label>Image</label>
                                                            <input type="file" name="photos[]" id="photos" accept=".png, .jpg, .jpeg" title="Images To Be Uploaded" class="form-control" > 

                                                        </div>                              

                                                </div>
                                            </div>
                                        </div>

                              </div>
                              <div class="modal-footer">
                               <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                               <button type="button" id="subcategory" onClick="Form_Submit()" class="btn btn-bold btn-pure btn-primary float-right">Submit</button>
                              </div>
                          </form>
                        </div>

                      </div>
                    </div>



                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Category Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
  <?php
     $sl=1;  
     foreach ($subcategory_fetch_data as $display) {

       $catname=$this->Category_model->getcategory($display['category_id']);
   ?>
                     
              
                                     <tr id="<?=$display['id']?>" > 

                                      <td><?= $sl?></td>
                                      <td><?php if(count($catname)>0){  echo $catname[0]['category_name'];  }else{  } ?>   </td>
                                      <td><?= $display['sub_category_name'] ?>   </td>
                                      <td><img src="<?= $image_url.'/'.$display['image'] ?>" style="max-width:45px; max-height:45px; " >  </td>
                                      <td> <a data-toggle="modal" data-id="<?=$display['id']?>" 
                             data-target="#modal-view<?= $display['id'] ?>" class="btn btn-info" ><i class="fa fa-pencil"></i>Update</a>  | <button  data-id="<?=$display['id']?>" class="btn btn-danger button-remove" ><i class="fa fa-trash"></i>Delete</button></td>
                          
                        <form id="update-data" action="#" method="post" enctype='multipart/form-data'> 
                               
                               <div class="modal modal-defalut fade" id="modal-view<?=$display['id']?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                     <h4 class="modal-title"> Category Details Update</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </div>
                                   <div class="modal-body">
                                         <div class="row">
                                            <div class="col-md-12" >

                                            <div class="form-group">
                                              <label>Select Category</label>
                                              <select name="upselectcat" id="upselectcat<?= $display['id'] ?>" required class="form-control">
                                                <option value="" >Select Option</option>
                                             <?php  foreach ($fetch_data as $cat) {  ?>
                                                <option <?php if($cat['id']==$display['category_id']){ echo "Selected"; }  ?>  value="<?= $cat['id'] ?>" ><?= $cat['category_name'] ?></option>
                                              <?php } ?>
                                              </select>
                                              </div>  


                                            <div class="form-group">
                                                <label>Sub Category Name</label>
                                                <input type="text" name="upcategory_name" id="upcategory_name<?= $display['id'] ?>" autocomplete="" required="" value="<?= $display['sub_category_name'] ?>" class="form-control" required="">
                                            </div> 

                                            <?php $service=$display['service_type'];
                                                  $services=explode(',',$service);
                                            ?>
                                            <div class="form-group" >
                                                <label for="email_id">Service Type</label>
                                                    <div class="mb-3">
                                                        <select class="form-control show-tick ms select2" name="service_type1[]" id="upservice<?= $display['id'] ?>" multiple data-placeholder="Select"  required="">
                                                           <option value=""></option>
                                                          <?php if(count($servicetypes)>0){
                                                            foreach($servicetypes as $key => $value){?>
                                                          <option value="<?=$value['service_type']?>" <?php if(in_array($value['service_type'],$services)){?> selected <?php }?>><?=$value['service_type']?></option>
                                                          <?php } }?>
                                                        </select>
                                                    </div>
                                            </div>  

                                              <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="upphotos" id="upphotos<?=$display['id']?>" accept=".png, .jpg, .jpeg" title="Images To Be Uploaded" class="form-control" onchange="subcategoryimage(this);"> 
                                                    <?php if($display['image']){?>
                                                    <img src="<?= $image_url.'/'.$display['image'] ?>" style="width:90px; height: 100px;border-radius: initial;padding-top: 10px;" id="profileview"/>
                                                  <?php }?>
                                               </div>   

                                               <input type="hidden" id="hiddenid" name="hiddenid" value="<?= $display['id'] ?>">

                                             </div>
                                          </div>

                                    </div>
                                      <div class="modal-footer modal-footer-uniform">
                                      <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" data-id="<?=$display['id']?>" class="btn btn-bold btn-pure btn-primary float-right" onClick="Update_Submit(<?=$display['id']?>)" >Save changes</button>
                                      </div>      
                                  </div>
                              </div>   
                            </div>

                        </form>

                                     </tr>


    <?php  $sl++;  } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>

 </div>

 <script type="text/javascript">
 $(function() {
      $('#add-data').parsley();
      $('.select2').select2();
  });


  var url1='<?php echo $base_url;?>';
  
  

 function Form_Submit()
 {
        $('.parsley-required').remove();
       var main_cat = $('#selectcat').val();
       var sub_cat = $('#subtitle').val();
       var serv_type = $('#serv_type').val();

        var formData = new FormData();
        formData.append('selectcat', main_cat);
        formData.append('subtitle', sub_cat);
        formData.append('service_types', serv_type);
        formData.append('upphotos', $('#photos')[0].files[0]); 

 if(main_cat !=''){

   if(sub_cat !=''){

        if(serv_type!=''){
            $.ajax({

                type: "POST",
                url: url1+"/addsubcategory",
                data: formData, 

                contentType: false,      
                cache: false,       
                processData:false,   


                success: function(data)
                {

                  if(data==1){
                   
                     location.reload();

                  }else{

                    toastr.error("Insert Fail..!!");
                   
                  }

                }

            }); 
            }else{
                    $("#serv_type").focus();
                    $("#serv_type").removeClass('parsley-success');
                    $("#serv_type").addClass('parsley-error');
                    $("#serv_type").parsley().addError('required', { message: 'Service Type Empty'});
                    return false; 

              }

              }else{
                    $("#subtitle").focus();
                    $("#subtitle").removeClass('parsley-success');
                    $("#subtitle").addClass('parsley-error');
                    $("#subtitle").parsley().addError('required', { message: 'Sub Category Name Empty'});
                    return false;
             }  

           }else{
                $("#selectcat").focus();
                $("#selectcat").removeClass('parsley-success');
                $("#selectcat").addClass('parsley-error');
                $("#selectcat").parsley().addError('required', { message: 'Category Empty'});
                return false;
         }  


       
       }



  function Update_Submit(getid)
  {
      $('#update-data').parsley();
      $('.parsley-required').remove();
     
      var main_cat1 = $('#upselectcat'+getid).val();
      var sub_cat1 = $('#upcategory_name'+getid).val();
      var serv_type1 = $('#upservice'+getid).val();
      var formData = new FormData();  

        formData.append('hidden_id', getid);
        formData.append('selectcat', main_cat1);
        formData.append('subcategory', sub_cat1);
        formData.append('service_types1', serv_type1);

     
if ($('#upphotos'+getid)[0].files[0] != undefined) {
     formData.append('upphotos', $('#upphotos'+getid)[0].files[0]); 
}
        if(main_cat1 !='' ){
               if(sub_cat1 !=''){
                if(serv_type1 != ''){
                        $.ajax({
                                 type: "POST",
                                 url: url1+"/updatesubcategory",
                                 data: formData, 

                                contentType: false,       // The content type used when sending data to the server.
                                cache: false,             // To unable request pages to be cached
                                processData:false,        // To send DOMDocument or non processed data file

                                 success: function(data)
                                 {
                                       if(data==1){
                                            
                                               location.reload();

                                        }else{ 
                               
                                            swal("Update Failed!", "Something Went Wrong. Retry.", "error");


                                        }

                                 }
                        });  

                         }else{
                    $("#upservice"+getid).focus();
                    $("#upservice"+getid).removeClass('parsley-success');
                    $("#upservice"+getid).addClass('parsley-error');
                    $("#upservice"+getid).parsley().addError('required', { message: 'Service Name Empty'});
                    
                    return false; 

                }
              }else{
                    $("#upcategory_name"+getid).focus();
                    $("#upcategory_name"+getid).removeClass('parsley-success');
                    $("#upcategory_name"+getid).addClass('parsley-error');
                    $("#upcategory_name"+getid).parsley().addError('required', { message: 'Sub Category Name Empty'});
                    return false;
             }  

         }else{
                $("#upselectcat"+getid).focus();
                $("#upselectcat"+getid).removeClass('parsley-success');
                $("#upselectcat"+getid).addClass('parsley-error');
                $("#upselectcat"+getid).parsley().addError('required', { message: 'Category Empty'});
                return false;
         }  


  }



    $('.button-remove').on('click', function(){
   
        var customer_id=$(this).attr('data-id');
        var id = $(this).parents("tr").attr("id");
      
        swal({
           title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
                 $.ajax({

                type: "POST",
                url: url1+"/delete_subcategory",
                data: {del_id:customer_id}, 
                success: function(data)
                {

                    if(data==1){
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                         $("#"+id).remove();
                    }else{
                         swal("Deleted Fail!", "Your imaginary file has been not deleted.", "error");
                    }
                }
            });
        });
    })

function subcategoryimage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
    
    $('#profileview').attr('src', e.target.result).width(90).height(100);
   /*  $('#profileview').after('<input type="button" class="delbtn removebtn" value="remove">'); */
    };

    reader.readAsDataURL(input.files[0]);
    }
}

 </script>