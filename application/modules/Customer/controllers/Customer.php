<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	private $request_data;
	private $request_header;

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('string');
        $this->load->helper('date');
        $this->load->helper(array('form', 'url')); 
        $this->load->library('session');
		$this->load->model('Customer_model');
		header("Content-Type: application/json;charset=utf-8");
		$this->request_data=json_decode(file_get_contents('php://input'));
		$this->request_header=getallheaders();
	}

	/* Boopathi */
	public function login()
	{
		$res=$this->Customer_model->login($this->request_data);
		return $this->Customer_model->json_response(200,true,'',$res);	
	}

	public function send_sms()
	{
		$res=$this->Customer_model->send_sms();
		return $this->Customer_model->json_response(200,true,'',$res);	
	}

	public function otp_verify(){
		$res=$this->Customer_model->otp_verify($this->request_data);
		return $this->Customer_model->json_response(200,true,'',$res);	
	}

	public function register(){
		$res=$this->Customer_model->register($this->request_data);
		return $this->Customer_model->json_response(200,true,'',$res);	
	}

	public function otp_generate(){
		$res=$this->Customer_model->otp_generate($this->request_data);
		return $this->Customer_model->json_response(200,true,'',$res);	
	}

	public function update_fcm_token(){
	   $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
	    $results = $this->Customer_model->update_fcm_token($result[0]['id'],$this->request_data);
        return $this->Customer_model->json_response(200,true,'',$results);		
	}
	

	public function get_profile(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->get_profile($result[0]['id']);
       return $this->Customer_model->json_response(200,true,'',$results);	
	}

	public function update_profile(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->update_profile($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}

	public function locationupdate(){
	   $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->locationupdate($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}


	public function book_service(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->book_service($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}

	public function book_service_image(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->book_service_image($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}


	public function category_list(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->category_list($result[0]['id']);
       return $this->Customer_model->json_response(200,true,'',$results);
	}

	public function booking_list(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->booking_list($result[0]['id']);
       return $this->Customer_model->json_response(200,true,'',$results);
	}

	public function cancel_service(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->cancel_service($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}


	public function search_service(){
         $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->search_service($result[0]['id'],$this->request_data);
       return $this->Customer_model->json_response(200,true,'',$results);
	}



	public function logout(){
        $data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Customer_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Customer_model->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Customer_model->json_response(401,false,'',[]); 
		}
       $results = $this->Customer_model->logout($result[0]['id']);
       return $this->Customer_model->json_response(200,true,'',$results);
	}






}
