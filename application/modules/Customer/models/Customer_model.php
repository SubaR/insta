<?php

class Customer_model extends CI_Model
{
	private $timezone = 'Asia/Kolkata';
    private $otp_msg = 'Invalid Otp.Please try again.';

    public function __construct()
	{
		parent::__construct();

	}
	/* Single value match check in database */
    public function singlevaluefetch($table,$fields,$value){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where($fields,$value);
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}
	/* multiple Condition check */
	public function multiplevaluefetch($table,$condition){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}
	/* Fetch Current Date Time */
	public function GetCurrentDateTime($timezone,$type){
        //$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date = new DateTime('now', new DateTimeZone($timezone));
        if($type=='date'){ return $date->format('Y-m-d'); }
        else if($type=='time'){ return $date->format('H:i:s'); }
        else{ return $date->format('Y-m-d H:i:s'); }
	}
	/* Fetch Customer Details */
	public function GetCustomerDetails($fields,$value){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($fields,$value);
        $query = $this->db->get();
        $res=$query->result_array();
        $data=new stdClass();
        $data->name=$res[0]['name'];
        $data->email=$res[0]['email'];
        $data->mobile=$res[0]['mobile'];
        $data->country_code=$res[0]['country_code'];
        $data->address=$this->emptychecknull($res[0]['address']);
        $data->latitude=$res[0]['latitude'];
        $data->longitude=$res[0]['longitude'];
        $data->photo=$this->emptychecknull($res[0]['photo']);

        $data->token=$res[0]['token'];
        return $data;
	} 
	public function emptychecknull($value){
		if($value==null){ return ''; }
        else if($value=='' || $value==0){ return ''; }
        else if($value!=''){return $value;}
	}
	/* Customer Login */
	public function login($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$otp = random_string('numeric', 4);
		$value=$this->singlevaluefetch('customer','mobile',$data->mobile);
		if(count($value)==0){ $login_status=0;}
		else{  if($value[0]['name']!=''){ $login_status=1;  } else { $login_status=0; } }
		$condition = array('mobile'=>$data->mobile,'type'=>'customer');
		$otp_value=$this->multiplevaluefetch('otp',$condition);
		$insert_data=array('mobile'=>$data->mobile,'otp'=>$otp,'key'=>md5($date),'login_status'=>$login_status,'country_code'=>$data->country_code,'type'=>'customer');
        if(count($otp_value)==0){
             	$this->db->insert('otp', $insert_data);
        }
        else{
             	$this->db->where('mobile',$data->mobile);
             	$this->db->where('type','customer');
             	$this->db->update('otp', $insert_data);
        }
        //$msg='One Time Password (OTP) for your Instahome account authentication is: '.$otp.'';
        /*$companyname='Instahome';
        $msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';*/
        //$this->send_sms($data->mobile,$otp);
        $insert_data['status']=true;
        return $insert_data;
	}
	/* Customer Otp verify */
	public function otp_verify($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
        $condition = array('mobile'=>$data->mobile,'key'=>$data->key,'otp'=>$data->otp);
		$value=$this->multiplevaluefetch('otp',$condition);
		if(count($value)==0){
            $res=array('status'=>false,'message'=>$this->otp_msg,'mobile'=>$data->mobile,'key'=>$data->key,'otp'=>$data->otp);
            return $res;
		}
		else{
           $login_status=$value[0]['login_status']; // check already login or not
           if($login_status==1){
           	    $res['status']=true;
           	    $insert_data=array('token' => md5($date),'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
				$cond="mobile='".$data->mobile."' AND country_code='".$value[0]['country_code']."'";
				$this->db->where($cond);
				$this->db->update('customer', $insert_data);
                $res['users']=$this->GetCustomerDetails('mobile',$data->mobile);
                $res['login_status']='yes';

                $users=$this->GetCustomerDetails('mobile',$data->mobile);
                
                if($users->address!=''){
                	$res['profile_status']='1';
                }
                else if($users->address==''){
                	$res['profile_status']='0';
                }
                $res['auth_id']='';
                $res['message']='';
                return $res;
           }
           else{

           		$result = $this->db->get_where('customer', array('status' => 1, 'mobile' => $data->mobile,'country_code'=>$value[0]['country_code']))->result_array();
           		if(count($result)==0){
           			$insert_data=array('mobile' => $data->mobile,'country_code'=>$value[0]['country_code'],'token'=>md5($date),'name'=>'','email'=>'','status'=>1,'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
                	$this->db->insert('customer', $insert_data);
           		}

           		$insert_data=array('token' => md5($date),'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
				$cond="mobile='".$data->mobile."' AND country_code='".$value[0]['country_code']."'";
				$this->db->where($cond);
				$this->db->update('customer', $insert_data);
               
                $res['status']=true;
                $res['login_status']='no';
                $users=$this->GetCustomerDetails('mobile',$data->mobile);

                if($users->address!=''){
                	$res['profile_status']='1';
                }
                else if($users->address==''){
                	$res['profile_status']='0';
                }
                $res['users']=$this->GetCustomerDetails('mobile',$data->mobile);
                $res['auth_id']=$users->token;
                $res['message']='';
                return $res;
           }

		}

	}

	public function logout($id){
		$result = $this->db->get_where('customer', array('status' => 1, 'id'=>$id))->result_array();
		$condition = array('mobile'=>$result[0]['mobile']);
		$value=$this->multiplevaluefetch('otp',$condition);
		if(count($value)==1){
			$insert_data=array('login_status' => 0);
			$cond="mobile='".$value[0]['mobile']."' AND type='customer'";
			$this->db->where($cond);
			$this->db->update('otp', $insert_data);

		}
		$insert_data=array('platform'=>'','firebase_token'=>'','token'=>'');
		$this->db->where('id',$id);
		$this->db->update('customer', $insert_data);
		return array('status'=>true,'message'=>'Logout Successfully.');

	}


	public function register($data){
		$insert_data=array('name'=>$data->name,'email'=>$data->email);
		$this->db->where('token',$data->auth_id);
		$this->db->update('customer', $insert_data);
		$res['status']=true;
        $res['users']=$this->GetCustomerDetails('token',$data->auth_id);
        $res['login_status']='yes';
        return $res;
	}
	public function otp_generate($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$otp = random_string('numeric', 4);
		$value=$this->singlevaluefetch('customer','mobile',$data->mobile);
		if(count($value)==0){ $login_status=0;}
		else{  if($value[0]['name']!=''){ $login_status=1;  } else { $login_status=0; } }
		$otp_value=$this->singlevaluefetch('otp','mobile',$data->mobile);
		$insert_data=array('mobile'=>$data->mobile,'otp'=>$otp,'key'=>md5($date),'login_status'=>$login_status,'country_code'=>$data->country_code);
        if(count($otp_value)==0){
             	$this->db->insert('otp', $insert_data);
        }
        else{
             	$this->db->where('mobile',$data->mobile);
             	$this->db->update('otp', $insert_data);
        }
       	/*$companyname='Instahome';
        $msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';*/
        //$this->send_sms($data->mobile,$otp);
        $insert_data['status']=true;
        return $insert_data;
	}
	public function update_fcm_token($id,$data){
       $insert_data=array('platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
       $this->db->where('id',$id);
       $this->db->update('customer', $insert_data);
       return array('status'=>true,'message'=>'Successfully Update Fcm Token');
	}
	public function get_profile($id){
          $res['status']=true;
          $res['users']=$this->GetCustomerDetails('id',$id);
          $res['login_status']='yes';
          return $res;
	}
	public function update_profile($id,$data){
		$insert_data=array('name'=>$data->name,'country_code'=>$data->country_code);
       $this->db->where('id',$id);
       $this->db->update('customer', $insert_data);
       return array('status'=>true,'message'=>'Successfully Update Profile');
	}

	
	public function locationupdate($id,$data){

		$insert_data1=array('address'=>$data->address,'latitude'=>$data->latitude,'longitude'=>$data->longitude);
		$this->db->where('id',$id);
        $this->db->update('customer', $insert_data1);
		$check=$this->singlevaluefetch('customer_address','customer_id',$id);

		if(count($check)!=0){
           $insert_data=array('address'=>$data->address,'latitude'=>$data->latitude,'longitude'=>$data->longitude,'status'=>1);
           $this->db->where('customer_id',$id);
           $this->db->update('customer_address', $insert_data);
           $address_id=$check[0]['id'];
		}
		else{
			$insert_data=array('customer_id'=>$id,'address'=>$data->address,'latitude'=>$data->latitude,'longitude'=>$data->longitude,'status'=>1);
            $this->db->insert('customer_address', $insert_data);
            $address_id=$this->db->insert_id();
		}
        return array('status'=>true,'message'=>'Successfully Update Location','address_id' => $address_id);
	}

	public function send_sms($mobile,$otp){
		$message='';
		$senderId = "MSGIND";
		$authKey = "311145AnFHLHrgqnFF5e0da80eP1";
		$mobileNumber = $mobile;
		$companyname='Instahome';
		$msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';
		//Your message to send, Add URL encoding here.
		$message = urlencode($msg);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?authkey=".$authKey."&mobile=".$mobileNumber."&message=".$message."&sender=".$senderId."&otp_expiry=&otp_length=&country=&otp=".$otp."&email=&template=",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  //echo "cURL Error #:" . $err;
		} else {
		  //echo $response;
		}

	}


	public function book_service_image($id,$data){
		$data = $_POST;
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		if (!is_dir('service')) {
			mkdir('./service', 0777, true);
		}

		$profile_path ='';
		if (isset($_FILES) && $_FILES['service']['name'] != '') {
			$config['upload_path'] = './service';
			$config['allowed_types'] = '*';
			$config['max_size'] = '64000';
			$this->load->library('upload', $config);
			$upload_files = $_FILES;
			if ($upload_files['service']['name'] != '') {
				$temp = explode(".", $upload_files['service']['name']);
				$random_file = md5($date);
				$newfilename = $random_file . '.' . end($temp);
				$_FILES['service'] = array(
					'name' => $newfilename,
					'type' => $upload_files['service']['type'],
					'tmp_name' => $upload_files['service']['tmp_name'],
					'error' => $upload_files['service']['error'],
					'size' => 10
				);
				$this->upload->do_upload("service");
				$upload_data = $this->upload->data();
				$profile_path = $upload_data['file_name'];
			}
			if($profile_path!=''){
				$photo_array=array('request_id'=>$data['request_id'],'photo'=>$profile_path,'type'=>'request');
				$insert=$this->db->insert('photo', $photo_array);

				$data1=array('status'=>true,'message'=>'Booking Imgae Uploaded Successfully');
				return $data1;
			}
		}

	}


	public function book_service($id,$data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$time=$this->GetCurrentDateTime($this->timezone,'time');

		$add = $this->db->get_where('customer_address', array('customer_id' => $id))->result_array();
		$address_id=0;
		if(count($add)>0){
			$address_id=$add[0]['id'];
		}

		$date1 = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=$date1->format('H:i A');
		$cur_date=$date1->format('Y-m-d');

		$cur_time=$time;
        $data = $_POST;
		

       $insertdata=array('customer_id'=>$id,'service_name'=>$data['service_name'],'category_id'=>$data['category_id'],'service_id' => $data['service_id'],'commands'=>$data['commands'],'date'=>$data['date'],'time'=>$cur_time,'status'=>0,'address_id'=>$address_id,'sub_category_id' =>$data['sub_category_id'],'brand_id'=>$data['brand_id']);
        $this->db->insert('book_request', $insertdata);
        //print_r($profile_path);exit;
		$request_id = $this->db->insert_id();
		
		$customer=$this->singlevaluefetch('customer','id',$id);

		$title='New Service Request';
		$title1='You have received a new service request from '.$customer[0]['name'].' ';
		$type='new_service_request';
		$type1='admin';
		$notification_array= array('sender_id'=>$id,'title' => $title,'message' => 'Hi Admin ,'.$title1,'status'=>0,'receiver_id'=>'','type'=>$type1,'date'=>$cur_date,'time'=>$cur_time);
		$insert = $this->db->insert('notification',$notification_array); 

        $data1=array('status'=>true,'message'=>'Booking Request Sent Successfully','request_id'=>$request_id);
		return $data1;
		
		
	}
	

	/* message json Data */
	public function AuthorizedUser($request_header){
	   $data1=array_change_key_case($request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->singlevaluefetch('customer','token',trim($auth[1]));
		if(count($result)==0){
			return $this->json_response(401,false,'',[]); 
		}
		else{
			 return 1;
		}
	}
	public function json_response($status_code,$status,$message,$data){
        $json_data=$this->jsonencode($status_code,$status,$message,$data);
		$this->output->set_content_type('application/json');
		$this->output->set_status_header($status_code);
		$data1=$this->output->set_output($json_data);
		return $data1;
	}
	public function jsonencode($status_code,$status,$message,$data){
         
         switch ($status_code) {
		    case 200:
		        if($status==true){
                   	 return json_encode($data);
		        }
		        else{
                  return json_encode(array('status'=> false,'message'=>'Something Went Wrong for Network Connection'));
		        }
		        break;
		    case 400:
		        return json_encode(array('status'=>false,'message'=>'Please Provide Header Data'));
		        break;
		    case 401:
		        return json_encode(array('status'=>false,'message'=>'Un Authorized User'));
		        break;
		    default:
		        
		        break;
		}
	}


	/*
	 * Push notification Code
	 */ 
	
	public function push_notification($vendor_de,$msg,$title,$id,$type,$tokan){
	  
		$this->load->library('Firebase');
		$this->load->library('Push');
		$firebase = new Firebase();
		$push = new Push();   
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time = $date->format('Y-m-d H:i:s');
		$not_time = $date->format('H:i:s');
		if($vendor_de['platform']!='IOS') {
			   
			$url = 'https://fcm.googleapis.com/fcm/send';
			$priority="high";
			//$title = 'Congratulations!';
			$notification= array('title' => $title,'message' => $msg,'image'=>'','timestamp'=>$not_time );
			$payload = array();
			$message = $notification['message'];
			$push_type = isset($data['push_type']) ? $data['push_type'] : '';
			$push->setTitle($title);
			$push->setMessage($message);
			$push->setType($type);
			$push->setId($id);
			$push->setImage("");
			$push->setIsBackground(FALSE);
			$push->setPayload($payload);
			$json = '';
			$response = '';
			$json = $push->getPush();
			$response = $firebase->send($tokan, $json);
			return 1;
	    }
	   else
	   {
		   $ch = curl_init("https://fcm.googleapis.com/fcm/send");
			//The device token.
			//Creating the notification array.
		   $notification = array('title' =>$title , 'body' => $msg,'type'=>$type,'id'=>$id,'sound'=>'Notification.caf');
		   $arrayToSend = array('to' => $tokan, 'data' => $notification,'notification' => $notification,'priority'=>'high','content_available'=>true);
			//Generating JSON encoded string form the above array.
			$json = json_encode($arrayToSend);
			//Setup headers:
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'Authorization: key=AAAAYvNpRgQ:APA91bG9tIPhPuc-Dd8vWxz3rA_q-SZh_j0yIW1fhfQiUK8epIaVzjlLboA3FWzaEYQ0u_XETs-wOeab8CljAwrUTB7vBuWL6ND7BRYMWcWEeiTS1_a7UhLW-Sb_lnnJuMvNeZO5ANGO';
			//Setup curl, add headers and post parameters.
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);                                                           
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       
			//Send the request
			$response = curl_exec($ch);
		   return 1;
	    }
	
		
	} 

	

	public function category_list($id){
		$category = $this->db->get_where('category')->result_array();
		$i=0;

		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_date = $date->format('Y-m-d');

		if(count($category)>0){
			$res=array();
			foreach ($category as $key => $value) {
				$sub_category = $this->db->get_where('sub_category', array('category_id' => $value['id']))->result_array();
				//print_r($sub_category);
				$offer = $this->db->get_where('offer', array('pro_cate_id' => $value['id']))->result_array();
				$array = array();
				if(count($offer)>0){
					 $k=0;$arr=array();
					$Variable1 = strtotime($offer[0]['start_date']); 
					$Variable2 = strtotime($offer[0]['end_date']); 
					for ($currentDate = $Variable1; $currentDate <= $Variable2;  
						$currentDate += (86400)) { 

						$Store = date('Y-m-d', $currentDate); 
						$array[] = $Store; 
						$arr[]=$offer[0]['id'];
						$k++;
					}
				}
				$offer_percentage='0';
				if(count($array)>0){
					if(in_array($cur_date,$array)){
						if(count($offer)>0){
							$offer_percentage=$offer[0]['offer_per'];
						}
						
					}
				}

				$res[$i]['category_id']=$value['id'];
				$sub_category_list=array(); $sub=array();
				if(count($sub_category)>0){
					$sub_category_list=$sub_category;
					$j=0;
					foreach ($sub_category_list as $key1 => $value1) {
						$sub[$j]['id']=$value1['id'];
						$sub[$j]['category_id']=$value1['category_id'];
						$sub[$j]['sub_category_name']=$value1['sub_category_name'];
						//$sub[$j]['image']=$this->config->item('base_url').'uploads/'.$value1['image'];
						$k=0;$serv=array();
						if($value1['service_type']!=''){
							$service=explode(',',$value1['service_type']);
						
							if(count($service)>0){
								foreach ($service as $key2 => $value2) {
									$service_name = $this->db->get_where('servicetype', array('service_type' => $value2))->result_array();

									$serv[$k]['service_id']=$service_name[0]['id'];
									$serv[$k]['service_type']=$value2;
									$serv[$k]['service_amount']=$value['category_amount'];
									$offer_cost='0';
									if($offer_percentage!=''){
										$off=$value['category_amount'] * $offer_percentage/100;

										$offer_cost=$value['category_amount'] - $off;
									}
									
									$serv[$k]['offer_cost']=number_format($offer_cost,2);
									$k++;
								}
							}
						}
						$sub[$j]['offer_percentage']=$offer_percentage.'%';
						$sub[$j]['service_type']=$serv;
						$sub[$j]['status']=$value1['status'];
						$j++;
					}
				}

				$cat_brand = $this->db->get_where('category_brand', array('category_id' => $value['id'],'status'=>1))->result_array();
				$k=0;$brand=array();
				if(count($cat_brand)>0){
					$brand_ids=explode(',',$cat_brand[0]['brand_id']);
					foreach ($brand_ids as $key2 => $value2) {
						$brand[$k]['brand_id']=$value2;
						$brand_name = $this->db->get_where('brand', array('id' => $value2,'status'=>1))->result_array();
						$brand_name1='';
						if(count($brand_name)>0){
							$brand_name1=$brand_name[0]['brand_name'];
						}
						$brand[$k]['brand_name']=$brand_name1;
						$k++;
					}
				}




                $res[$i]['cateogry_name']=$value['category_name'];
                $res[$i]['category_amount']=$value['category_amount'];
                $res[$i]['image']=$this->config->item('base_url').'uploads/'.$value1['image'];
				$res[$i]['subcategory_list']=$sub;
				$res[$i]['brand_list']=$brand;
				$i++;
			}
		}
		$reponse['status']=true;
		$response['category_lists']=$res;
		return $response;
	}


	public function booking_list($id){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$booking = $this->db->order_by('id', 'DESC')->get_where('service',array('customer_id'=>$id))->result_array();
		$i=0;$j=0;$book=array();$book1=array();
		if(count($booking)>0){
			foreach ($booking as $key => $value) {//$value['status'] == 4 ||
				if( $value['status'] == 5){
					$cat = $this->db->get_where('category', array('id' => $value['category_id']))->result_array();
					$book[$i]['job_ref_id']=$value['invoice_ref_no'];
					$book[$i]['category_name']=$cat[0]['category_name'];
					$book[$i]['booking_month']=date('M',strtotime($value['date']));
					$book[$i]['booking_date']=date('d',strtotime($value['date']));
					$book[$i]['booking_time']=date('h:i A',strtotime($value['time']));
					$book[$i]['booking_status']=$value['status'];
					$book[$i]['start_otp']=$value['start_otp'];
					$book[$i]['end_otp']=$value['end_otp'];
					$book[$i]['cost']=$value['total_cost'];
					$i++;
				}
			}
		}


		$booking_request = $this->db->order_by('id', 'DESC')->get_where('book_request',array('customer_id'=>$id))->result_array();

		if(count($booking_request)>0){
			foreach ($booking_request as $key1 => $value1) {
				if($value1['status'] !=5){
					$cat = $this->db->get_where('category', array('id' => $value1['category_id']))->result_array();
					$book1[$j]['job_ref_id']='JOB#'.$value1['id'];
					$book1[$j]['category_name']=$cat[0]['category_name'];
					$book1[$j]['booking_month']=date('M',strtotime($value1['date']));
					$book1[$j]['booking_date']=date('d',strtotime($value1['date']));
					$book1[$j]['booking_time']=date('h:i A',strtotime($value1['time']));
					$book1[$j]['booking_status']=$value1['status'];
					$start_otp='';$end_otp='';$cost='0';
					if($value1['status']!=0){
						$serv = $this->db->get_where('service', array('request_id' => $value1['id']))->result_array();
						if(count($serv)>0){
							$start_otp=$serv[0]['start_otp'];
							$end_otp=$serv[0]['end_otp'];
							$cost=$serv[0]['total_cost'];
						}
						
					}
					$book1[$j]['start_otp']=$start_otp;
					$book1[$j]['end_otp']=$end_otp;
					$book1[$j]['cost']=$cost;
					$j++;
				}
			}
		}

		$booking_det['upcoming']=$book1;
		$booking_det['completed']=$book;
		$response['status']=true;
		$response['booking_details']=$booking_det;
		return $response;

	}


	public function search_service($id,$data){
		if($data->key_word!=''){

			$condition=" category_name LIKE '%".$data->key_word."%'  ORDER BY id DESC";
			$query =$this->db->select("*")->from("category")->where($condition)->get();
	        $category = $query->result_array();

	      	$i=0;$res=array();
			if(count($category)>0){
				
				foreach ($category as $key => $value) {
					$sub_category = $this->db->get_where('sub_category', array('category_id' => $value['id']))->result_array();

					$res[$i]['category_id']=$value['id'];
					$sub_category_list=array(); $sub=array();
					if(count($sub_category)>0){
						$sub_category_list=$sub_category;
						$j=0;
						foreach ($sub_category_list as $key1 => $value1) {
							$sub[$j]['id']=$value1['id'];
							$sub[$j]['category_id']=$value1['category_id'];
							$sub[$j]['sub_category_name']=$value1['sub_category_name'];
							//$sub[$j]['image']=$this->config->item('base_url').'uploads/'.$value1['image'];
							$k=0;$serv=array();
							if($value1['service_type']!=''){
								$service=explode(',',$value1['service_type']);
							
								if(count($service)>0){
									foreach ($service as $key2 => $value2) {
										$service_name = $this->db->get_where('servicetype', array('service_type' => $value2))->result_array();
										$serv[$k]['service_id']=$service_name[0]['id'];
										$serv[$k]['service_type']=$value2;
										$serv[$k]['service_amount']=$value['category_amount'];
										$k++;
									}
								}
							}
							
							$sub[$j]['service_type']=$serv;
							$sub[$j]['status']=$value1['status'];
							$j++;
						}
					}
	                $res[$i]['cateogry_name']=$value['category_name'];
	                $res[$i]['category_amount']=$value['category_amount'];
	                $res[$i]['image']=$this->config->item('base_url').'uploads/'.$value1['image'];
					$res[$i]['subcategory_list']=$sub;
					$i++;
				}
			}
			$response['status']=true;
			$response['category_lists']=$res;
			return $response;
	    }
	}

	

	

 
}
?>