<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MY_Controller {

	private $request_data;
	private $request_header;

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('string');
        $this->load->helper('date');
        $this->load->helper(array('form', 'url')); 
        $this->load->library('session');
		$this->load->model('Vendor_model');
		header("Content-Type: application/json;charset=utf-8");
		$this->request_data=json_decode(file_get_contents('php://input'));
		$this->request_header=getallheaders();
	}

	/* Boopathi */
	public function login()
	{
		$res=$this->Vendor_model->login($this->request_data);
		return $this->Vendor_model->json_response(200,true,'',$res);	
	}

	public function otp_verify(){
		$res=$this->Vendor_model->otp_verify($this->request_data);
		return $this->Vendor_model->json_response(200,true,'',$res);	
	}

	public function register(){
		$res=$this->Vendor_model->register($this->request_data);
		return $this->Vendor_model->json_response(200,true,'',$res);	
	}

	public function otp_generate(){
		$res=$this->Vendor_model->otp_generate($this->request_data);
		return $this->Vendor_model->json_response(200,true,'',$res);	
	}

	public function update_fcm_token(){
	   $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
	    $results = $this->Vendor_model->update_fcm_token($result[0]['id'],$this->request_data);
        return $this->Vendor_model->json_response(200,true,'',$results);		
	}
	

	public function get_profile(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       $results = $this->Vendor_model->get_profile($result[0]['id']);
       return $this->Vendor_model->json_response(200,true,'',$results);	
	}

	public function update_profile(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       $results = $this->Vendor_model->update_profile($result[0]['id'],$this->request_data);
       return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function update_profile_data(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       $results = $this->Vendor_model->update_profile_data($result[0]['id'],$this->request_data);
       return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function locationupdate(){
	   $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       $results = $this->Vendor_model->locationupdate($result[0]['id'],$this->request_data);
       return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function category_based_services(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->category_based_services($result[0]['id']);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function get_booking_details(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->get_booking_details($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function job_list(){
	   $data1=array_change_key_case($this->request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       $results = $this->Vendor_model->job_list($result[0]['id'],$this->request_data);
       return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function accept_job(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->accept_job($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function job_start_otp_verify(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->job_start_otp_verify($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}


	public function stop_job(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->stop_job($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function followup_action(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->followup_action($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function complete_action(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->complete_action($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function complete_action_image(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->complete_action_image($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function today_earnings(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->today_earnings($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function month_earnings(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->month_earnings($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function custom_earnings(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->custom_earnings($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}


	public function insert_chat(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->insert_chat_message($result[0]['id'],$this->request_data);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function chat(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->chat($result[0]['id']);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}

	public function logout(){
		$data1=array_change_key_case($this->request_header,CASE_LOWER);
       	if(empty($data1['authorization'])){
       	   return $this->Vendor_model->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->Vendor_model->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->Vendor_model->json_response(401,false,'',[]); 
		}
       	$results = $this->Vendor_model->logout($result[0]['id']);
       	return $this->Vendor_model->json_response(200,true,'',$results);
	}
	


	






}
