<?php

class Vendor_model extends CI_Model
{
	private $timezone = 'Asia/Kolkata';
    private $otp_msg = 'Invalid Otp.Please try again.';

    public function __construct()
	{
		parent::__construct();

	}
	/* Single value match check in database */
    public function singlevaluefetch($table,$fields,$value){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where($fields,$value);
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}
	/* multiple Condition check */
	public function multiplevaluefetch($table,$condition){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        $res=$query->result_array();
        return $res;
	}
	/* Fetch Current Date Time */
	public function GetCurrentDateTime($timezone,$type){
        //$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date = new DateTime('now', new DateTimeZone($timezone));
        if($type=='date'){ return $date->format('Y-m-d'); }
        else if($type=='time'){ return $date->format('H:i:s'); }
        else{ return $date->format('Y-m-d H:i:s'); }
	}
	/* Fetch Customer Details */
	public function GetCustomerDetails($fields,$value){
        $this->db->select('*');
        $this->db->from('vendor');
        $this->db->where($fields,$value);
        $query = $this->db->get();
        $res=$query->result_array();
        $data=new stdClass();
        $data->name=$res[0]['name'];
        $data->email=$res[0]['email'];
        $data->mobile=$res[0]['mobile'];
        $data->country_code=$res[0]['country_code'];
        $data->address=$this->emptychecknull($res[0]['address']);
        $data->latitude=$res[0]['latitude'];
        $data->longitude=$res[0]['longitude'];
        $photo='';
       	if($res[0]['photo']!=''){
       		$photo=$this->config->item('base_url').'vendor/'.$res[0]['id'].'/'.$res[0]['photo'];
       	}

        $data->photo=$photo;
        $data->category=explode(',',$res[0]['category']);
        $service=explode(',',$res[0]['service_type']);

       // print_r($service);exit;
        $data->service=$service;
        $data->token=$res[0]['token'];
        return $data;
	} 
	public function emptychecknull($value){
		if($value!=''){return $value;}
        else if($value==null){ return ''; }
        else if($value=='' || $value==0){ return ''; }
        
        
	}
	/* Customer Login */
	public function login($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$otp = random_string('numeric', 4);
		$value=$this->singlevaluefetch('vendor','mobile',$data->mobile);
		if(count($value)==0){ $login_status=0;}
		else{  if($value[0]['name']!=''){ $login_status=1;  } else { $login_status=0; } }
		$condition = array('mobile'=>$data->mobile,'type'=>'vendor');
		$otp_value=$this->multiplevaluefetch('otp',$condition);
		$insert_data=array('mobile'=>$data->mobile,'otp'=>$otp,'key'=>md5($date),'login_status'=>$login_status,'country_code'=>$data->country_code,'type'=>'vendor');
        if(count($otp_value)==0){
            $this->db->insert('otp', $insert_data);
        }
        else{
         	$this->db->where('mobile',$data->mobile);
         	$this->db->where('type','vendor');
         	$this->db->update('otp', $insert_data);
        }
        /*$companyname='Instahome';
        $msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';*/
        //$this->send_sms($data->mobile,$otp);
        $insert_data['status']=true;
        return $insert_data;
	}
	/* Customer Otp verify */
	public function otp_verify($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
        $condition = array('mobile'=>$data->mobile,'key'=>$data->key,'otp'=>$data->otp,'type'=>'vendor');
		$value=$this->multiplevaluefetch('otp',$condition);
		if(count($value)==0){
            $res=array('status'=>false,'message'=>$this->otp_msg,'mobile'=>$data->mobile,'key'=>$data->key,'otp'=>$data->otp);
            return $res;
		}
		else{
           $login_status=$value[0]['login_status']; // check already login or not
           if($login_status==1){
           	    $res['status']=true;
           	    $insert_data=array('token' => md5($date),'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
				$cond="mobile='".$data->mobile."' AND country_code='".$value[0]['country_code']."'";
				$this->db->where($cond);
				$this->db->update('vendor', $insert_data);
                $res['users']=$this->GetCustomerDetails('mobile',$data->mobile);
                $res['login_status']='yes';
                $profile = $this->db->get_where('vendor', array('status' => 1, 'mobile' => $data->mobile,'country_code'=>$value[0]['country_code']))->result_array();

                if(count($profile)>0 && $profile[0]['service_type']!=''){
                	$res['profile_status']=1;
                }
                else if(count($profile)>0 &&  $profile[0]['service_type']==''){
                	$res['profile_status']=0;
                }
                else{
                	$res['profile_status']=0;
                }
                $res['services']=$this->Getservicelist();
                $res['message']='';
                return $res;
           }
           else{
           		$result = $this->db->get_where('vendor', array('status' => 1, 'mobile' => $data->mobile,'country_code'=>$value[0]['country_code']))->result_array();
           		if(count($result)==0){
               		$insert_data=array('mobile' => $data->mobile,'country_code'=>$value[0]['country_code'],'token'=>md5($date),'name'=>$value[0]['name'],'email'=>$value[0]['email'],'status'=>1,'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
                	$this->db->insert('vendor', $insert_data);
                }
                $insert_data=array('token' => md5($date),'platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
				$cond="mobile='".$data->mobile."' AND country_code='".$value[0]['country_code']."'";
				$this->db->where($cond);
				$this->db->update('vendor', $insert_data);

                $profile = $this->db->get_where('vendor', array('status' => 1, 'mobile' => $data->mobile,'country_code'=>$value[0]['country_code']))->result_array();

                if(count($profile)>0 && $profile[0]['service_type']!=''){
                	$res['profile_status']=1;
                }
                else if(count($profile)>0 &&  $profile[0]['service_type']==''){
                	$res['profile_status']=0;
                }
                else{
                	$res['profile_status']=0;
                }
                $res['status']=true;
                $res['login_status']='no';
                $res['services']=$this->Getservicelist();
                $res['users']=$this->GetCustomerDetails('mobile',$data->mobile);
                $res['message']='';
                return $res;
           }

		}

	}


	public function logout($id){
		$result = $this->db->get_where('vendor', array( 'id'=>$id))->result_array();
		$condition = array('mobile'=>$result[0]['mobile']);
		$value=$this->multiplevaluefetch('otp',$condition);
		if(count($value)==1){
			$insert_data=array('login_status' => 0);
			$cond="mobile='".$value[0]['mobile']."' AND type='vendor'";
			$this->db->where($cond);
			$this->db->update('otp', $insert_data);

		}
		$insert_data=array('platform'=>'','firebase_token'=>'','token'=>'');
		$this->db->where('id',$id);
		$this->db->update('vendor', $insert_data);

		//$deleted= $this->mongo_db->where('mobile', $result[0]['mobile'], 'type', 'vendor')->delete('otp');

		return array('status'=>true,'message'=>'Logout Successfully');
	}
	public function register($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$otp = random_string('numeric', 4);
		$value=$this->singlevaluefetch('vendor','mobile',$data->mobile);
		if(count($value)==0){ $login_status=0;}
		else{  if($value[0]['name']!=''){ $login_status=1;  } else { $login_status=0; } }
		$condition = array('mobile'=>$data->mobile,'type'=>'vendor');
		$otp_value=$this->multiplevaluefetch('otp',$condition);
		$insert_data=array('mobile'=>$data->mobile,'otp'=>$otp,'key'=>md5($date),'login_status'=>$login_status,'country_code'=>$data->country_code,'type'=>'vendor','name'=>$data->name,'email'=>$data->email);
        if(count($otp_value)==0){
             	$this->db->insert('otp', $insert_data);
        }
        else{
             	$this->db->where('mobile',$data->mobile);
             	$this->db->where('type','vendor');
             	$this->db->update('otp', $insert_data);
        }
        /*$companyname='Instahome';
        $msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';*/
        //$this->send_sms($data->mobile,$otp);
        $insert_data['status']=true;
        return $insert_data;
	}
	public function otp_generate($data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		$otp = random_string('numeric', 4);
		$value=$this->singlevaluefetch('vendor','mobile',$data->mobile);
		if(count($value)==0){ $login_status=0;}
		else{  if($value[0]['name']!=''){ $login_status=1;  } else { $login_status=0; } }
		$otp_value=$this->singlevaluefetch('otp','mobile',$data->mobile);
		$insert_data=array('mobile'=>$data->mobile,'otp'=>$otp,'key'=>md5($date),'login_status'=>$login_status,'country_code'=>$data->country_code);
        if(count($otp_value)==0){
             	$this->db->insert('otp', $insert_data);
        }
        else{
             	$this->db->where('mobile',$data->mobile);
             	$this->db->update('otp', $insert_data);
        }
       /*	$companyname='Instahome';
        $msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';*/
        //$this->send_sms($data->mobile,$otp);
        $insert_data['status']=true;
        return $insert_data;
	}
	public function send_sms($mobile,$msg){
		$message='';
		$senderId = "MSGIND";
		$authKey = "311145AnFHLHrgqnFF5e0da80eP1";
		$mobileNumber = $mobile;
		$companyname='Instahome';
		$msg=''.$otp.' is your '.$companyname.' OTP. Do not share this code with anyone else.';
		//Your message to send, Add URL encoding here.
		$message = urlencode($msg);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://control.msg91.com/api/sendotp.php?authkey=".$authKey."&mobile=".$mobileNumber."&message=".$message."&sender=".$senderId."&otp_expiry=&otp_length=&country=&otp=".$otp."&email=&template=",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  //echo "cURL Error #:" . $err;
		} else {
		  //echo $response;
		}
	}
	public function update_fcm_token($id,$data){
       $insert_data=array('platform'=>$data->platform,'firebase_token'=>$data->firebase_token);
       $this->db->where('id',$id);
       $this->db->update('vendor', $insert_data);
       return array('status'=>true,'message'=>'Successfully Update Fcm Token');
	}
	public function get_profile($id){
		$res['status']=true;
		$res['users']=$this->GetCustomerDetails('id',$id);
		$res['login_status']='yes';
		return $res;
	}

	public function update_profile_data($id,$data){
		//rtrim($my_string, ',');
		$insert_data=array('name'=>$data->name,'email'=>$data->email,'address'=>$data->address,'latitude'=>$data->latitude,'longitude'=>$data->longitude,'category'=>$data->category);
        $this->db->where('id',$id);
        $update=$this->db->update('vendor', $insert_data);
        if($update){
        	return array('status'=>true,'message'=>'Successfully Update Profile');
        }
        
    }
	public function update_profile($id,$data){
		$data=$_POST;
		//rtrim($my_string, ',');
		$insert_data=array('name'=>$data['name'],'email'=>$data['email'],'address'=>$data['address'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude'],'category'=>$data['category']);
        $this->db->where('id',$id);
        $this->db->update('vendor', $insert_data);
        
	    $profile_path='';

		if (!is_dir('vendor')) {
			mkdir('./vendor', 0777, true);
		}
		if (!is_dir('vendor/'.$id)) {
			mkdir('./vendor/'.$id, 0777, true);
		}

	    $config['upload_path'] = './vendor/'.$id;
		$config['allowed_types'] = '*';
		$config['max_size']	= '64000';
		$this->load->library('upload', $config);
		$upload_files = $_FILES;
		if($upload_files['image']['name']!='')
		{
		    $temp = explode(".",$upload_files['image']['name']);
			$random_file="profile_".md5(uniqid(rand(), true));
			$newfilename = $random_file. '.' .end($temp);
			$_FILES['image'] = array(
				'name' => $newfilename,
				'type' => $upload_files['image']['type'],
				'tmp_name' => $upload_files['image']['tmp_name'],
				'error' => $upload_files['image']['error'],
				'size' => 10
			);
			$this->upload->do_upload("image");
			$upload_data = $this->upload->data();
		    $profile_path=$upload_data['file_name'];
			if($profile_path ==''){
					return 0;
			}else{
				$old_pic = $this->vendor_profile_pic_name($id);
				if($old_pic!=''){
					$get_file = $this->config->item('base_url') .'vendor/'.$id.'/'.$old_pic;
					if(file_exists($get_file)){ 
						unlink($get_file); 
					}
				}
			    $insert_array=array('photo'=>$profile_path);
			    $this->db->where('id',$id);
        		$this->db->update('vendor', $insert_array);
			}
		}
        return array('status'=>true,'message'=>'Successfully Update Profile');
	}


	 public function vendor_profile_pic_name($id)
	{
		$user = $this->db->get_where('vendor', array('id'=>$id))->result_array();
		if($user[0]['photo']!=''){
			$imagename=$user[0]['photo'];
		}
		else{
			$imagename="";
		}
		return $imagename;
	}

	
	public function locationupdate($id,$data){

		$insert_data1=array('address'=>$data->address,'latitude'=>$data->latitude,'longitude'=>$data->longitude);
		$this->db->where('id',$id);
        $this->db->update('vendor', $insert_data1);
        return array('status'=>true,'message'=>'Successfully Update Location');
	}
	

	/* message json Data */
	public function AuthorizedUser($request_header){
	   $data1=array_change_key_case($request_header,CASE_LOWER);
       if(empty($data1['authorization'])){
       	   return $this->json_response(400,false,'',[]); 
        }
        $auth=explode(' ', $data1['authorization']);
		$result = $this->singlevaluefetch('vendor','token',trim($auth[1]));
		if(count($result)==0){
			return $this->json_response(401,false,'',[]); 
		}
		else{
			 return 1;
		}
	}
	public function json_response($status_code,$status,$message,$data){
        $json_data=$this->jsonencode($status_code,$status,$message,$data);
		$this->output->set_content_type('application/json');
		$this->output->set_status_header($status_code);
		$data1=$this->output->set_output($json_data);
		return $data1;
	}
	public function jsonencode($status_code,$status,$message,$data){
         
         switch ($status_code) {
		    case 200:
		        if($status==true){
                   	 return json_encode($data);
		        }
		        else{
                  return json_encode(array('status'=> false,'message'=>'Something Went Wrong for Network Connection'));
		        }
		        break;
		    case 400:
		        return json_encode(array('status'=>false,'message'=>'Please Provide Header Data'));
		        break;
		    case 401:
		        return json_encode(array('status'=>false,'message'=>'Un Authorized User'));
		        break;
		    default:
		        
		        break;
		}
	}

	
	public function job_list($id,$data){
		$date=$this->GetCurrentDateTime($this->timezone,'datetime');
		
		//$jobs = $this->db->order_by('id', 'DESC')->get_where('service', array( 'vendor_id' => $id))->result_array();
		$condition="vendor_id='".$id."' OR reassign_vendor_id='".$id."' ";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		$jobs=$query->result_array();

		$accept_job=array();$job=new stdClass();
		if(count($jobs)>0){
			$i=0;
			foreach ($jobs as $key => $value) {
				if($value['status']==1 || $value['status']==6 ){
					$category = $this->db->get_where('category', array('id' => $value['category_id']))->result_array();
					$accept_job[$i]['category_name']=$category[0]['category_name'];
					$accept_job[$i]['date']=date('d-m-Y', strtotime($value['date']));
					$accept_job[$i]['time']=$value['time'];
					$accept_job[$i]['job_id']=$value['request_id'];
					$accept_job[$i]['job_reference_id']=$value['invoice_ref_no'];

					$customer=$this->singlevaluefetch('customer','id',$value['customer_id']);
					$accept_job[$i]['customer_name']=$customer[0]['name'];
					$accept_job[$i]['customer_mobile']=$customer[0]['mobile'];
					$accept_job[$i]['customer_address']=$customer[0]['address'];
					$accept_job[$i]['latitude']=$customer[0]['latitude'];
					$accept_job[$i]['longitude']=$customer[0]['longitude'];
					$accept_job[$i]['job_status']=$value['status'];
					$accept_job[$i]['service_id']=$value['id'];

					$condition="a.id='".$value['request_id']."'";
					$this->db->select('b.service_type,a.service_name,c.category_amount,c.id,a.level,a.date');
					$this->db->where($condition);
					$this->db->from('book_request a');
					$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
					$this->db->join('category c', 'c.id = a.category_id', 'left');
					$query = $this->db->get();
					$job_list = $query->result_array();
					$service_amount=$job_list[0]['category_amount'];
					$accept_job[$i]['service_amount']=$service_amount;
					$accept_job[$i]['service_amount_id']=$job_list[0]['id'];
					$accept_job[$i]['start_time']=$value['start_time'];
					$accept_job[$i]['end_time']=$value['end_time'];
					$accept_job[$i]['priority']=$job_list[0]['level'];

					$offer=$this->get_offer($value['category_id'],trim($job_list[0]['date']));
					//print_r('AAAAA'.$offer);
					$offer_cost='0';
					if($offer!=''){
						$off=$job_list[0]['category_amount'] * $offer/100;
						$offer_cost=$job_list[0]['category_amount'] - $off;
					}
					$accept_job[$i]['offer_cost']=number_format($offer_cost,2);

					$brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
					$brand_name='';
					if(count($brand)>0){
						$brand_name=$brand[0]['brand_name'];
					}

					$accept_job[$i]['brand_name']=$brand_name;
					if($value['reassign_vendor_id']!=0 && $value['reassign_vendor_id']==$id){
						$reassign_status=0;
						$followup_status=1;
					}
					else{
						$reassign_status=$value['reassign_status'];
						$followup_status=0;
					}
					$accept_job[$i]['reassign_status']=$reassign_status;
					$accept_job[$i]['followup_status']=$followup_status;

					$i++;
				}
			}
			
		}

		$new_jobs = $this->db->order_by('id', 'DESC')->get_where('cron_job_cache', array('status' => 0, 'vendor_id' => $id))->result_array();
		$j=0;
		$new_job=array();
		if(count($new_jobs)>0){
			foreach ($new_jobs as $key1 => $value1) {

				if($value1['status']==0){
					$condition="a.id='".$value1['request_id']."' AND b.status=0";
					$this->db->select('c.category_name,b.date,b.time,b.request_id,a.address_id,a.customer_id,a.status,a.level,c.id as category_id,a.service_id,a.id as book_id,a.brand_id,a.reassign_status,b.vendor_id');
					$this->db->where($condition);
					$this->db->from('book_request a');
					$this->db->join('cron_job_cache b', 'b.request_id = a.id', 'left');
					$this->db->join('category c', 'c.id = a.category_id', 'left');
					$query = $this->db->get();
					$job_list = $query->result_array();
					if(count($job_list)>0){
						$job=$job_list[0];
						$new_job[$j]['category_name']=$job['category_name'];
						$new_job[$j]['date']=date('m-d-Y',strtotime($job['date']));
						$new_job[$j]['time']=date('h:i A',strtotime($job['time']));

						$customer = $this->db->get_where('customer', array('id' => $value1['customer_id']))->result_array();
						//print_r($customer);
						$new_job[$j]['customer_name']=$customer[0]['name'];
						$new_job[$j]['customer_address']=$customer[0]['address'];
						$new_job[$j]['customer_mobile']=$customer[0]['mobile'];
						$new_job[$j]['job_id']=$value1['request_id'];
						$new_job[$j]['job_reference_id']='JOB#'.$value1['request_id'];
						$new_job[$j]['latitude']=$customer[0]['latitude'];
						$new_job[$j]['longitude']=$customer[0]['longitude'];
						$new_job[$j]['job_status']=$job['status'];
						$new_job[$j]['service_id']='';
						/*$new_job[$j]['service_amount']='0';*/
						$new_job[$j]['service_amount_id']='';
						$new_job[$j]['start_time']='';
						$new_job[$j]['end_time']='';
						$new_job[$j]['priority']=$job['level'];

						$condition="a.id='".$job['book_id']."'";
						$this->db->select('b.service_type,a.service_name,c.category_amount,c.id,a.level,a.date,a.brand_id');
						$this->db->where($condition);
						$this->db->from('book_request a');
						$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
						$this->db->join('category c', 'c.id = a.category_id', 'left');
						$query = $this->db->get();
						$job_list1 = $query->result_array();

						$offer1=$this->get_offer($job['category_id'],trim($job['date']));
						//print_r('BBBBB'.$offer1);
						$offer_cost='0';
						if($offer1!=''){
							$off=$job_list1[0]['category_amount'] * $offer1/100;
							$offer_cost=$job_list1[0]['category_amount'] - $off;
						}
						$new_job[$j]['service_amount']=$job_list1[0]['category_amount'];
						$new_job[$j]['offer_cost']=number_format($offer_cost,2);


						$brand = $this->db->get_where('brand', array('id' => $job['brand_id']))->result_array();
						$brand_name='';
						if(count($brand)>0){
							$brand_name=$brand[0]['brand_name'];
						}
						$new_job[$j]['brand_name']=$brand_name;
						if($job['reassign_status']==1){
							//print_r($job);
							$serv = $this->db->get_where('service', array('request_id' => $value1['request_id']))->result_array();
							if(count($serv)>0){
								if($job['vendor_id']==$id){
									$reassign_status=0;
									$followup_status=0;
								}
								else{
									$reassign_status=$job['reassign_status'];
									$followup_status=1;
								}

							}
							
						}
						else{
							$reassign_status=0;
							$followup_status=0;
						}
						
						
						$new_job[$j]['reassign_status']=$reassign_status;
						$new_job[$j]['followup_status']=$followup_status;

						$j++;
					}
					
				}
			}
		}


		//$inprogress = $this->db->order_by('id', 'DESC')->get_where('service', array( 'vendor_id' => $id))->result_array();

		$condition="vendor_id='".$id."' OR reassign_vendor_id='".$id."' ";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		$inprogress=$query->result_array();

		$inprogress_job=array();
		
		if(count($inprogress)>0){
			$k=0;
			foreach ($inprogress as $key1 => $value1) {
				if($value1['status']==2 || $value1['status']==4 || $value1['status']==7){
					$category = $this->db->get_where('category', array('id' => $value1['category_id']))->result_array();
					$inprogress_job[$k]['category_name']=$category[0]['category_name'];
					$inprogress_job[$k]['date']=date('d-m-Y', strtotime($value1['date']));
					$inprogress_job[$k]['time']=$value1['time'];
					$inprogress_job[$k]['job_id']=$value1['request_id'];
					$inprogress_job[$k]['job_reference_id']=$value1['invoice_ref_no'];

					$customer=$this->singlevaluefetch('customer','id',$value1['customer_id']);
					$inprogress_job[$k]['customer_name']=$customer[0]['name'];
					$inprogress_job[$k]['customer_mobile']=$customer[0]['mobile'];
					$inprogress_job[$k]['customer_address']=$customer[0]['address'];
					$inprogress_job[$k]['latitude']=$customer[0]['latitude'];
					$inprogress_job[$k]['longitude']=$customer[0]['longitude'];
					$inprogress_job[$k]['job_status']=$value1['status'];
					$inprogress_job[$k]['service_id']=$value1['id'];

					$condition="a.id='".$value1['request_id']."'";
					$this->db->select('b.service_type,a.service_name,c.category_amount,c.id,a.level,a.date');
					$this->db->where($condition);
					$this->db->from('book_request a');
					$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
					$this->db->join('category c', 'c.id = a.category_id', 'left');
					$query = $this->db->get();
					$job_list = $query->result_array();
					$service_amount=$job_list[0]['category_amount'];
					$inprogress_job[$k]['service_amount']=$service_amount;
					$inprogress_job[$k]['service_amount_id']=$job_list[0]['id'];
					$inprogress_job[$k]['start_time']=$value1['start_time'];
					$inprogress_job[$k]['end_time']=$value1['end_time'];
					$inprogress_job[$k]['priority']=$job_list[0]['level'];

					$offer2=$this->get_offer($value1['category_id'],trim($job_list[0]['date']));
					//print_r('CCCC'.$offer2);
					$offer_cost='0';
					if($offer2!=''){
						$off=$job_list[0]['category_amount'] * $offer2/100;
						$offer_cost=$job_list[0]['category_amount'] - $off;
						//print_r($offer1);
					}
					$inprogress_job[$k]['offer_cost']=number_format($offer_cost,2);

					$brand = $this->db->get_where('brand', array('id' => $value1['brand_id']))->result_array();
					$brand_name='';
					if(count($brand)>0){
						$brand_name=$brand[0]['brand_name'];
					}
					$inprogress_job[$k]['brand_name']=$brand_name;
					if($value1['reassign_vendor_id']!=0 && $value1['reassign_vendor_id']==$id){
						$reassign_status=0;
						$followup_status=1;
					}
					else{
						$reassign_status=$value1['reassign_status'];
						$followup_status=0;
					}
					
					$inprogress_job[$k]['reassign_status']=$reassign_status;
					$inprogress_job[$k]['followup_status']=$followup_status;

					$k++;
				}
			}
		}


		/*$complete = $this->db->order_by('id', 'DESC')->get_where('service', array( 'vendor_id' => $id))->result_array();*/

		$condition="vendor_id='".$id."' OR reassign_vendor_id='".$id."' ";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		$complete=$query->result_array();

		$complete_job=array();
		if(count($complete)>0){
			$m=0;
			foreach ($complete as $key2 => $value2) {
				if($value2['status']==5){
					$category = $this->db->get_where('category', array('id' => $value2['category_id']))->result_array();
					$complete_job[$m]['category_name']=$category[0]['category_name'];
					$complete_job[$m]['date']=date('d-m-Y', strtotime($value2['date']));
					$complete_job[$m]['time']=$value2['time'];
					$complete_job[$m]['job_id']=$value2['request_id'];
					$complete_job[$m]['job_reference_id']=$value2['invoice_ref_no'];

					$customer=$this->singlevaluefetch('customer','id',$value2['customer_id']);
					$complete_job[$m]['customer_name']=$customer[0]['name'];
					$complete_job[$m]['customer_mobile']=$customer[0]['mobile'];
					$complete_job[$m]['customer_address']=$customer[0]['address'];
					$complete_job[$m]['latitude']=$customer[0]['latitude'];
					$complete_job[$m]['longitude']=$customer[0]['longitude'];
					$complete_job[$m]['job_status']=$value2['status'];
					$complete_job[$m]['service_id']=$value2['id'];

					$condition="a.id='".$value2['request_id']."'";
					$this->db->select('b.service_type,a.service_name,c.category_amount,c.id,a.level,a.date');
					$this->db->where($condition);
					$this->db->from('book_request a');
					$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
					$this->db->join('category c', 'c.id = a.category_id', 'left');
					$query = $this->db->get();
					$job_list = $query->result_array();
					$service_amount=$job_list[0]['category_amount'];
					$complete_job[$m]['service_amount_id']=$job_list[0]['id'];
					$complete_job[$m]['start_time']=$value2['start_time'];
					$complete_job[$m]['end_time']=$value2['end_time'];
					$complete_job[$m]['priority']=$job_list[0]['level'];

					$offer3=$this->get_offer($value1['category_id'],trim($job_list[0]['date']));
					//print_r('DDDDD'.$offer3);
					$offer_cost='0';
					if($offer3!=''){
						$off=$job_list[0]['category_amount'] * $offer3/100;
						$offer_cost=$job_list[0]['category_amount'] - $off;
					}
					$complete_job[$m]['service_amount']=$job_list[0]['category_amount'];
					$complete_job[$m]['offer_cost']=number_format($offer_cost,2);
					$brand = $this->db->get_where('brand', array('id' => $value2['brand_id']))->result_array();
					$brand_name='';
					if(count($brand)>0){
						$brand_name=$brand[0]['brand_name'];
					}
					$complete_job[$m]['brand_name']=$brand_name;
					if($value2['reassign_vendor_id']!=0 && $value2['reassign_vendor_id']==$id){
						$reassign_status=0;
						$followup_status=1;
					}
					else{
						$reassign_status=$value2['reassign_status'];
						$followup_status=0;
					}
					$complete_job[$m]['reassign_status']=$reassign_status;
					$complete_job[$m]['followup_status']=$followup_status;

					$m++;
				}
			}
		}

		$response['status']=true;
		$response['new_job_list']=$new_job;
		$response['accepted_job_list']=$accept_job;
		$response['inprogress_job_list']=$inprogress_job;
		$response['complete_job_list']=$complete_job;
		return $response;
		
	}

	public function job_start_otp_verify($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		//$jobs = $this->db->get_where('service', array('id' => $data->service_id, 'vendor_id' => $id, 'start_otp_verify' => 0))->result_array();

		$condition="(vendor_id='".$id."' OR reassign_vendor_id='".$id."' ) AND id='".$data->service_id."' AND start_otp_verify=0";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$query = $this->db->get();
		$jobs=$query->result_array();

		$accept_job=new stdClass();
		if(count($jobs)>0){
			
			if($jobs[0]['start_otp']==$data->otp){
				$up_array=array('status'=>2);
				$updated1 = $this->db->where('id', $jobs[0]['request_id'])->set($up_array)->update('book_request');
				if($updated1){
					$cur_time=$date->format('H:i A'); 
					$insert_array=array('start_time'=>$cur_time,'start_otp_verify' => 1,'status' => 2);
					$updated = $this->db->where('id', $data->service_id)->set($insert_array)->update('service');

					/*$jobs1 = $this->db->get_where('service', array('status' => 2, 'vendor_id' => $id))->result_array();*/

					$condition1="(vendor_id='".$id."' OR reassign_vendor_id='".$id."' ) AND status=2";
					$this->db->select('*');
					$this->db->from('service');
					$this->db->where($condition1);
					$query = $this->db->get();
					$jobs1=$query->result_array();
					
					if(count($jobs1)>0){
						$i=0;
						foreach ($jobs1 as $key => $value) {
							if($value['status']==2){
								$category = $this->db->get_where('category', array('id' => $value['category_id']))->result_array();
								$accept_job->category_name=$category[0]['category_name'];
								$accept_job->date=date('d-m-Y', strtotime($value['date']));
								$accept_job->time=date('h:i A', strtotime($value['time']));
								$accept_job->job_id=$value['request_id'];
								$accept_job->job_reference_id=$value['invoice_ref_no'];

								$customer=$this->singlevaluefetch('customer','id',$value['customer_id']);
								$accept_job->customer_name=$customer[0]['name'];
								$accept_job->customer_mobile=$customer[0]['mobile'];
								$accept_job->customer_address=$customer[0]['address'];
								$accept_job->latitude=$customer[0]['latitude'];
								$accept_job->longitude=$customer[0]['longitude'];
								$accept_job->job_status=$value['status'];
								$accept_job->service_id=$value['id'];

								$condition="a.id='".$jobs[0]['request_id']."'";
								$this->db->select('b.service_type,a.service_name,a.level,c.category_amount,a.date');
								$this->db->where($condition);
								$this->db->from('book_request a');
								$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
								$this->db->join('category c', 'c.id = a.category_id', 'left');
								$query = $this->db->get();
								$book = $query->result_array();
								
								$accept_job->priority=$book[0]['level'];

								$offer=$this->get_offer($value['category_id'],trim($book[0]['date']));

								$offer_cost='0';
								if($offer!=''){
									$off=$book[0]['category_amount'] * $offer/100;
									$offer_cost=$book[0]['category_amount'] - $off;
								}
								$accept_job->service_amount=$book[0]['category_amount'];
								$accept_job->offer_cost=number_format($offer_cost,2);

								$brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
								$brand_name='';
								if(count($brand)>0){
									$brand_name=$brand[0]['brand_name'];
								}
								$accept_job->brand_name=$brand_name;

								if($value['reassign_vendor_id']!=0 && $value['reassign_vendor_id']==$id){
									$reassign_status=0;
									$followup_status=1;
								}
								else{
									$reassign_status=$value['reassign_status'];
									$followup_status=0;
								}
					
								$accept_job->reassign_status=$reassign_status;
								$accept_job->followup_status=$followup_status;

								$i++;
							}

						}
						
					}

					$customer=$this->db->get_where('customer', array('id' => $jobs[0]['customer_id']))->result_array();
					if(count($customer)>0){

						if($customer[0]['firebase_token']!=''){

							$this->load->library('Firebase');
						    $this->load->library('Push');
						    $firebase = new Firebase();
						    $push = new Push();   
						    $this->load->helper('date');
						    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
						    $cur_time = $date->format('d-m-Y');
						    $not_time = $date->format('H:i:s');
						    
						    $url = 'https://fcm.googleapis.com/fcm/send';
						    $priority="high";

						    $customer_name='';
						    if(count($customer)>0){
						    	$customer_name=$customer[0]['name'];
						    }
						    $vendor=$this->db->get_where('vendor', array('id' => $jobs[0]['vendor_id']))->result_array();
							$name='';
							if(count($vendor)>0){
								$name=$vendor[0]['name'];
							}

						    $title = 'Service Request Started';
							$title1='Your service request '.$jobs[0]['invoice_ref_no'].' started by '.$name.' ';
							$type='service_request_start';
							$type1='vendor';

							$registrationIds=$customer[0]['firebase_token'];

							$send_id=$id;
						    $rec_id=$customer[0]['id'];

						    $notification= array('title' => $title,'message' => 'Hi '.$customer_name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

						    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($customer_name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
					   
						    //print_r($registrationIds);
						    $payload = array();
						    $message = $notification['message'];
						    $push_type = isset($data->push_type) ? $data->push_type : '';
						    $push->setTitle($title);
						    $push->setMessage($message);
						    $push->setType($type);
						    $push->setId($jobs[0]['request_id']);
						    $push->setImage("");
						    $push->setIsBackground(FALSE);
						    $push->setPayload($payload);
						    $push_time = $date->format('Y-m-d G:i:s');
						    //$push->setTime($push_time);
						    $json = '';
						    $responses = '';
						    $json = $push->getPush();
						    $responses = $firebase->send($registrationIds, $json);
						  
						    if($responses){
					            $insert = $this->db->insert('notification',$notification_array); 
					        }

						}

					}

					if($updated){
						$response['status']=true;
						$response['job_details']=$accept_job;
						$response['message']='Otp Verified Successfully.';
					}
				}
				
			}
			else{
				$response['status']=false;
				$response['job_details']=$accept_job;
				$response['message']='Invalid Otp. Please try again.';
			}
		}
		else{
			$response['status']=false;
			$response['job_details']=$accept_job;
			$response['message']='Already Verified.';
		}
		return $response;
	}



	public function get_offer($cat_id,$date_off){

		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$offer = $this->db->get_where('offer', array('pro_cate_id' => $cat_id))->result_array();
		$offer_percentage='0';
		$array = array(); 
		if(count($offer)>0){
			$k=0;$arr=array();
			$Variable1 = strtotime($offer[0]['start_date']); 
			$Variable2 = strtotime($offer[0]['end_date']); 
			for ($currentDate = $Variable1; $currentDate <= $Variable2;  
				$currentDate += (86400)) { 

				$Store = date('Y-m-d', $currentDate); 
				$array[] = $Store; 
				$arr[]=$offer[0]['id'];
				$k++;
			}
		}
		//print_r($date_off);
		//2019-12-23AAAAA0
		//2019-12-23CCCC0
		//2019-12-20DDDDD10
		//2019-12-20DDDDD10
		
		if(count($array)>0){
			if(in_array($date_off,$array)){
				if(count($offer)>0){
					$offer_percentage=$offer[0]['offer_per'];
				}
				
			}//
		}

		return $offer_percentage;

	}

	public function stop_job($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		//$jobs = $this->db->get_where('service', array('id' => $data->service_id, 'vendor_id' => $id, 'end_otp_verify' => 0))->result_array();
		$condition="(vendor_id='".$id."' OR reassign_vendor_id='".$id."' ) AND id='".$data->service_id."' AND end_otp_verify=0";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$query = $this->db->get();
		$jobs=$query->result_array();

		$cur_time=strtotime($date->format('H:i A')); 
		$st_time=strtotime($jobs[0]['start_time']);
		$elapsed=$cur_time - $st_time;
		$elapsed_time = date("H:i", $elapsed);
		$accept_job=new stdClass();
		if(count($jobs)>0){
			if($jobs[0]['end_otp']==$data->otp){

				$up_array=array('status'=>4);
				$updated1 = $this->db->where('id', $jobs[0]['request_id'])->set($up_array)->update('book_request');
				if($updated1){
					$cur_time=$date->format('H:i A'); 
					$insert_array=array('end_time'=>$cur_time,'end_otp_verify' => 1,'status' => 4, 'duration' =>$elapsed_time);
					$updated = $this->db->where('id', $data->service_id)->set($insert_array)->update('service');


					/*$jobs1 = $this->db->get_where('service', array('status' => 4, 'vendor_id' => $id))->result_array();*/


					$condition1="(vendor_id='".$id."' OR reassign_vendor_id='".$id."' ) AND status=4";
					$this->db->select('*');
					$this->db->from('service');
					$this->db->where($condition1);
					$query = $this->db->get();
					$jobs1=$query->result_array();
					
					if(count($jobs1)>0){
						$i=0;
						foreach ($jobs1 as $key => $value) {
							if($value['status']==4){
								$category = $this->db->get_where('category', array('id' => $value['category_id']))->result_array();
								$accept_job->category_name=$category[0]['category_name'];
								$accept_job->date=date('d-m-Y', strtotime($value['date']));
								$accept_job->time=date('h:i A', strtotime($value['time']));
								$accept_job->job_id=$value['request_id'];
								$accept_job->job_reference_id=$value['invoice_ref_no'];

								$customer=$this->singlevaluefetch('customer','id',$value['customer_id']);
								$accept_job->customer_name=$customer[0]['name'];
								$accept_job->customer_mobile=$customer[0]['mobile'];
								$accept_job->customer_address=$customer[0]['address'];
								$accept_job->latitude=$customer[0]['latitude'];
								$accept_job->longitude=$customer[0]['longitude'];
								$accept_job->job_status=$value['status'];
								$accept_job->service_id=$value['id'];

								$condition="a.id='".$value['request_id']."'";
								$this->db->select('b.service_type,a.service_name,c.category_amount,c.id,a.level,a.date');
								$this->db->where($condition);
								$this->db->from('book_request a');
								$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
								$this->db->join('category c', 'c.id = a.category_id', 'left');
								$query = $this->db->get();
								$job_list = $query->result_array();
								$service_amount=$job_list[0]['category_amount'];
								$accept_job->service_amount=$service_amount;
								$accept_job->service_amount_id=$job_list[0]['id'];
								$accept_job->start_time=$value['start_time'];
								$accept_job->end_time=$value['end_time'];
								$accept_job->priority=$job_list[0]['level'];

								$offer=$this->get_offer($value['category_id'],trim($job_list[0]['date']));

								$offer_cost='0';
								if($offer!=''){
									$off=$job_list[0]['category_amount'] * $offer/100;
									$offer_cost=$job_list[0]['category_amount'] - $off;
								}
								$accept_job->service_amount=$job_list[0]['category_amount'];
								$accept_job->offer_cost=number_format($offer_cost,2);

								$brand = $this->db->get_where('brand', array('id' => $value['brand_id']))->result_array();
								$brand_name='';
								if(count($brand)>0){
									$brand_name=$brand[0]['brand_name'];
								}
								$accept_job->brand_name=$brand_name;


								$i++;
							}

						}
						
					}

					$customer=$this->db->get_where('customer', array('id' => $jobs[0]['customer_id']))->result_array();
					if(count($customer)>0){

						if($customer[0]['firebase_token']!=''){

							$this->load->library('Firebase');
						    $this->load->library('Push');
						    $firebase = new Firebase();
						    $push = new Push();   
						    $this->load->helper('date');
						    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
						    $cur_time = $date->format('d-m-Y');
						    $not_time = $date->format('H:i:s');
						    
						    $url = 'https://fcm.googleapis.com/fcm/send';
						    $priority="high";

						    $customer_name='';
						    if(count($customer)>0){
						    	$customer_name=$customer[0]['name'];
						    }
						    $vendor=$this->db->get_where('vendor', array('id' => $jobs[0]['vendor_id']))->result_array();
							$name='';
							if(count($vendor)>0){
								$name=$vendor[0]['name'];
							}

						    $title = 'Service Request Stoped';
							$title1='Your service request '.$jobs[0]['invoice_ref_no'].' started by '.$name.' ';
							$type='service_request_stop';
							$type1='vendor';

							$registrationIds=$customer[0]['firebase_token'];

							$send_id=$id;
						    $rec_id=$customer[0]['id'];

						    $notification= array('title' => $title,'message' => 'Hi '.$customer_name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

						    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($customer_name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
					   
						    //print_r($registrationIds);
						    $payload = array();
						    $message = $notification['message'];
						    $push_type = isset($data->push_type) ? $data->push_type : '';
						    $push->setTitle($title);
						    $push->setMessage($message);
						    $push->setType($type);
						    $push->setId($jobs[0]['request_id']);
						    $push->setImage("");
						    $push->setIsBackground(FALSE);
						    $push->setPayload($payload);
						    $push_time = $date->format('Y-m-d G:i:s');
						    //$push->setTime($push_time);
						    $json = '';
						    $responses = '';
						    $json = $push->getPush();
						    $responses = $firebase->send($registrationIds, $json);
						  	//print_r($responses);
						    if($responses){
					            $insert = $this->db->insert('notification',$notification_array); 
					        }

						}

					}

					if($updated){
						$response['status']=true;
						$response['job_details']=$accept_job;
						$response['message']='Job Completed Successfully.';
					}
				}
			}
			else{
				$response['status']=false;
				$response['job_details']=$accept_job;
				$response['message']='Invalid Otp. Please try again.';

			}
		}
		return $response;
	}


	public function followup_action($id,$data){
		$jobs = $this->db->get_where('service', array('id' => $data->service_id, 'vendor_id' => $id, 'status' => 2))->result_array();
		if(count($jobs)>0){
			if($data->job_action==1){//followup

				$up_array1=array('status'=>7,'reassign_status'=>$data->reassign_status);
				$updated2 = $this->db->where('id', $jobs[0]['request_id'])->set($up_array1)->update('book_request');
				if($updated2){
					$date = new DateTime('now', new DateTimeZone($this->timezone));
					$fol_up_time=date('h:i A', strtotime($data->followup_time));

					if($data->reassign_status==1){
						$up_array=array('followup_date'=>$data->followup_date,'followup_time'=>$fol_up_time,'followup_comments'=>$data->followup_comments,'status'=>7,'action_status' => 1,'reassign_status'=>$data->reassign_status,'start_otp_verify'=>0,'end_otp_verify'=>0);
					}
					else{
						$up_array=array('followup_date'=>$data->followup_date,'followup_time'=>$fol_up_time,'followup_comments'=>$data->followup_comments,'status'=>7,'action_status' => 1,'reassign_status'=>$data->reassign_status);
					}

					


					$updated1 = $this->db->where('id', $data->service_id)->set($up_array)->update('service');
					if($updated1){
						$customer=$this->db->get_where('customer', array('id' => $jobs[0]['customer_id']))->result_array();
						if(count($customer)>0){

							if($customer[0]['firebase_token']!=''){

								$this->load->library('Firebase');
							    $this->load->library('Push');
							    $firebase = new Firebase();
							    $push = new Push();   
							    $this->load->helper('date');
							    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
							    $cur_time = $date->format('d-m-Y');
							    $not_time = $date->format('H:i:s');
							    
							    $url = 'https://fcm.googleapis.com/fcm/send';
							    $priority="high";

							    $customer_name='';
							    if(count($customer)>0){
							    	$customer_name=$customer[0]['name'];
							    }
							    $vendor=$this->db->get_where('vendor', array('id' => $jobs[0]['vendor_id']))->result_array();
								$name='';
								if(count($vendor)>0){
									$name=$vendor[0]['name'];
								}

							    $title = 'Service Request Followed';
								$title1='Your service request '.$jobs[0]['invoice_ref_no'].' started by '.$name.' ';
								$type='service_request_follow';
								$type1='vendor';

								$registrationIds=$customer[0]['firebase_token'];

								$send_id=$id;
							    $rec_id=$customer[0]['id'];

							    $notification= array('title' => $title,'message' => 'Hi '.$customer_name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

							    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($customer_name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
						   
							    //print_r($registrationIds);
							    $payload = array();
							    $message = $notification['message'];
							    $push_type = isset($data->push_type) ? $data->push_type : '';
							    $push->setTitle($title);
							    $push->setMessage($message);
							    $push->setType($type);
							    $push->setId($jobs[0]['request_id']);
							    $push->setImage("");
							    $push->setIsBackground(FALSE);
							    $push->setPayload($payload);
							    $push_time = $date->format('Y-m-d G:i:s');
							    //$push->setTime($push_time);
							    $json = '';
							    $responses = '';
							    $json = $push->getPush();
							    $responses = $firebase->send($registrationIds, $json);
							  
							    if($responses){
						            $insert = $this->db->insert('notification',$notification_array); 
						        }

							}

						}
						$response['status']='true';
						$response['message']='Followedup Successfully';
						return $response;
					}
				}

				
			}

		}
	}

	public function edit_product_amount($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=strtotime($date->format('H:i A')); 
		$cur_time=strtotime($date->format('Y-m-d')); 
		$service = $this->db->get_where('category', array('id' => $data->service_amount_id, 'status' => 1))->result_array();
		if(count($service)>0){
			$up_array=array('category_amount'=>$data->service_amount);
			$updated1 = $this->db->where('id', $data->service_amount_id)->set($up_array)->update('servicetype');
			if($updated1){

				$insertdata=array('vendor_id'=>$id,'amount'=>$data->service_amount,'request_id'=>$data->request_id,'time'=>$cur_time,'date' => $cur_date,'status'=>1);
            	$insert=$this->db->insert('amount_history', $insertdata);

            	$response['status']=true;
            	$response['message']='Amount Updated Successfully.';
            	return $response;
			}
		}


	}

	public function complete_action_image($id,$data){
		$data=$_POST;
		$profile_path='';

		if (!is_dir('request_product')) {
			mkdir('./request_product', 0777, true);
		}
		if (!is_dir('request_product/'.$data['service_id'])) {
			mkdir('./request_product/'.$data['service_id'], 0777, true);
		}

	    $config['upload_path'] = './request_product/'.$data['service_id'];
		$config['allowed_types'] = '*';
		$config['max_size']	= '64000';
		$this->load->library('upload', $config);
		$upload_files = $_FILES;
		if($upload_files['image']['name']!='')
		{
		    $temp = explode(".",$upload_files['image']['name']);
			$random_file="profile_".md5(uniqid(rand(), true));
			$newfilename = $random_file. '.' .end($temp);
			$_FILES['image'] = array(
				'name' => $newfilename,
				'type' => $upload_files['image']['type'],
				'tmp_name' => $upload_files['image']['tmp_name'],
				'error' => $upload_files['image']['error'],
				'size' => 10
			);
			$this->upload->do_upload("image");
			$upload_data = $this->upload->data();
		    $profile_path=$upload_data['file_name'];
		    $insert_array=array('photo'=>$profile_path);
		    $this->db->where('id',$data['service_id']);
    		$update=$this->db->update('service', $insert_array);
    		if($update){
    			$response['status']=true;
				$response['message']='Service Image Updated Successfully.';
				return $response;
    		}
    		else{
    			$response['status']=false;
				$response['message']='Service Image Updated Failed.';
				return $response;
    		}
			
		}

	}



	public function complete_action($id,$data){
		$data=$_POST;

		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=strtotime($date->format('H:i A')); 
		$cur_date=strtotime($date->format('Y-m-d')); 
		$cur_month = $date->format('m');
		$cur_year = $date->format('Y');
		$cur_date1=$date->format('d-m-Y');
		//$amount=substr($str, 4);
		/*$service = $this->db->get_where('servicetype', array('id' => $data['service_amount_id'], 'status' => 1))->result_array();
		if(count($service)>0){
			$up_array=array('service_amount'=>$data['service_amount']);
			$updated1 = $this->db->where('id', $data['service_amount_id'])->set($up_array)->update('servicetype');
			if($updated1){
				$insertdata=array('vendor_id'=>$id,'amount'=>$data['service_amount'],'request_id'=>$data['request_id'],'time'=>$cur_time,'date' => $cur_date,'status'=>1);
            	$insert=$this->db->insert('amount_history', $insertdata);
			}
		}*/
	
		//$jobs = $this->db->get_where('service', array('id' => $data['service_id'], 'vendor_id' => $id, 'status' => 4))->result_array();

		$condition1="(vendor_id='".$id."' OR reassign_vendor_id='".$id."' ) AND status=4 AND id='".$data['service_id']."'";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition1);
		$query = $this->db->get();
		$jobs=$query->result_array();

		if(count($jobs)>0){
			if($data['job_action']==2){

				$up_array1=array('status'=>5);
				$updated2 = $this->db->where('id', $jobs[0]['request_id'])->set($up_array1)->update('book_request');

				if($updated2){

					$up_array=array('amount'=>$data['amount'],'commands'=>$data['comments'],'status'=>5,'action_status' => 2,'complete_date' => $cur_date1,'month'=>$cur_month,'year'=>$cur_year,'offer_cost'=>$data['offer_cost'],'total_cost'=>$data['total_cost'],'additional_cost'=>$data['additional_cost']);
					$updated1 = $this->db->where('id', $data['service_id'])->set($up_array)->update('service');
				}
				

				
				if($data['invoice_status'] == 'yes'){
					$customer = $this->db->get_where('customer', array('id' => $jobs[0]['customer_id']))->result_array();
					
					$vendor = $this->db->get_where('vendor', array('id' => $id))->result_array();
					$old_pic = $this->vendor_profile_pic_name($id);
					$vendor_image=$this->config->item('base_url') .'vendor/'.$id.'/'.$old_pic;
					$email_data['to_email']=$customer[0]['email'];
					$email_data['subject']="Instahome Service Invoice Summary";
				
					$category=$this->singlevaluefetch('category','id',$jobs[0]['category_id']);
					$category_name=$category[0]['category_name'];

					$condition="a.id='".$jobs[0]['request_id']."'";
					$this->db->select('b.service_type,a.service_name');
					$this->db->where($condition);
					$this->db->from('book_request a');
					$this->db->join('servicetype b', 'b.id = a.service_id', 'left');
					$query = $this->db->get();
					$job_list = $query->result_array();
					$service_name=$job_list[0]['service_name'];

					$content['content']=array('customer_name'=>$customer[0]['name'],'customer_email'=>$customer[0]['email'],'total_fare'=>"₹"." ".$data['amount'],'job_id'=>$jobs[0]['invoice_ref_no'],'duration'=>$jobs[0]['duration'],'service_address'=>$customer[0]['address'],'vendor_name'=>$vendor[0]['name'],'vendor_email'=>$vendor[0]['email'],'image'=>$vendor_image,'category_name'=>$category_name,'service_name'=>$service_name);

					$content['theme_path']=$this->config->item('theme_locations').$this->config->item('active_template').'/dashboard_templete.php';
					
					$msg=$this->load->view('Vendor/invoice',$content,true);
					$email_send=$this->send_email($email_data,$msg);
					//print_r($email_send);exit;
				}
				$customer=$this->db->get_where('customer', array('id' => $jobs[0]['customer_id']))->result_array();
				if(count($customer)>0){

					if($customer[0]['firebase_token']!=''){

						$this->load->library('Firebase');
					    $this->load->library('Push');
					    $firebase = new Firebase();
					    $push = new Push();   
					    $this->load->helper('date');
					    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
					    $cur_time = $date->format('d-m-Y');
					    $not_time = $date->format('H:i:s');
					    
					    $url = 'https://fcm.googleapis.com/fcm/send';
					    $priority="high";

					    $customer_name='';
					    if(count($customer)>0){
					    	$customer_name=$customer[0]['name'];
					    }
					    $vendor=$this->db->get_where('vendor', array('id' => $jobs[0]['vendor_id']))->result_array();
						$name='';
						if(count($vendor)>0){
							$name=$vendor[0]['name'];
						}

					    $title = 'Service Request Completed';
						$title1='Your service request '.$jobs[0]['invoice_ref_no'].' followed by '.$name.' ';
						$type='service_request_complete';
						$type1='vendor';

						$registrationIds=$customer[0]['firebase_token'];

						$send_id=$id;
					    $rec_id=$customer[0]['id'];

					    $notification= array('title' => $title,'message' => 'Hi '.$customer_name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

					    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($customer_name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
				   
					    //print_r($registrationIds);
					    $payload = array();
					    $message = $notification['message'];
					    $push_type = isset($data->push_type) ? $data->push_type : '';
					    $push->setTitle($title);
					    $push->setMessage($message);
					    $push->setType($type);
					    $push->setId($jobs[0]['request_id']);
					    $push->setImage("");
					    $push->setIsBackground(FALSE);
					    $push->setPayload($payload);
					    $push_time = $date->format('Y-m-d G:i:s');
					    //$push->setTime($push_time);
					    $json = '';
					    $responses = '';
					    $json = $push->getPush();
					    $responses = $firebase->send($registrationIds, $json);
					  
					    if($responses){
				            $insert = $this->db->insert('notification',$notification_array); 
				        }

					}

				}

				$response['status']=true;
				$response['message']='Service Completed Successfully.';
				return $response;
			}

		}
		else{
			$response['status']=false;
			$response['message']='';
			return $response;
		}
	}


	public function send_email($input,$msg)
	{
		$to_array=$input['to_email'];
		require(APPPATH.'phpmailer/PHPMailerAutoload.php');
		//print_r(APPPATH);exit;
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'suba.rudram@gmail.com';                 // SMTP username
		$mail->Password = 'suba13292';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                     // TCP port to connect to
		//Recipients
		$mail->setFrom('suba.rudram@gmail.com', 'Instahome Service');
		$mail->addAddress($to_array);     // Add a recipient
		$mail->addReplyTo('suba.rudram@gmail.com', 'Instahome Service');

		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $input['subject'];
		$mail->Body    = $msg;
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		$mail->send();
		//$this->email->print_debugger();exit;

		if(!$mail->send()){ //echo 'Mail Sent Fail , Error: '.$mail->ErrorInfo; 
			return 1;
		}
		else{ //echo 'Mail Sent Fail , Error: '.$mail->ErrorInfo; 
			return 1;
		}


	}


	public function accept_job($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=$date->format('h:i A');
		$cur_date=$date->format('Y-m-d');
		$book = $this->db->get_where('book_request', array('id' => $data->request_id,'status'=>6))->result_array();
		if(count($book)>0){
			$up_array=array('status'=>1);
			$updated1 = $this->db->where('id', $data->request_id)->set($up_array)->update('book_request');
			$book=$book[0];
			$start_otp=random_string('numeric',4);
			$end_otp=random_string('numeric',4);
			$job_id='JOB#'.$data->request_id;
			if($book['reassign_status']==1){
				$service = $this->db->get_where('service', array('request_id' =>$data->request_id,'reassign_status' => 1))->result_array();
				if(count($service)>0){
					$insertdata=array('start_otp'=>$start_otp,'end_otp'=>$end_otp,'time'=>$cur_time,'date' => $cur_date,'status'=>1,'reassign_vendor_id'=>$id);
					$this->db->where('id',$service[0]['id']);
		            $insert=$this->db->update('service', $insertdata);
		            $insert_id = $service[0]['id'];
				}

			}else{
				$insertdata=array('customer_id'=>$book['customer_id'],'request_id'=>$data->request_id,'vendor_id'=>$id,'category_id' => $book['category_id'],'start_otp'=>$start_otp,'end_otp'=>$end_otp,'time'=>$cur_time,'date' => $cur_date,'status'=>1,'invoice_ref_no'=>$job_id,'brand_id'=>$book['brand_id']);
	            $insert=$this->db->insert('service', $insertdata);
	            $insert_id = $this->db->insert_id();
			}
			
			
            if($insert){

            	$cron = $this->db->get_where('cron_job_cache', array('vendor_id' => $id,'request_id' => $data->request_id))->result_array();
            	if(count($cron)>0){
            		$up_array2=array('status'=>1);
            		$updated2 = $this->db->where('id', $cron[0]['id'])->set($up_array2)->update('cron_job_cache');
            	}

            	$cron_job = $this->db->get_where('cron_job_cache', array('request_id' => $data->request_id,'status' =>0))->result_array();
            	if(count($cron_job)>0){
            		foreach ($cron_job as $key => $value) {
            			$deleted= $this->mongo_db->where('id', $value['id'])->delete('cron_job_cache');
            		}
            	}

            	$response['status']=true;
            	$response['service_id']=(string)$insert_id;
				$response['message']='Job Accepted Successfully.';

				$customer=$this->db->get_where('customer', array('id' => $book['customer_id']))->result_array();

				if(count($customer)>0){

					if($customer[0]['firebase_token']!=''){

						$this->load->library('Firebase');
					    $this->load->library('Push');
					    $firebase = new Firebase();
					    $push = new Push();   
					    $this->load->helper('date');
					    $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
					    $cur_time = $date->format('d-m-Y');
					    $not_time = $date->format('H:i:s');
					    
					    $url = 'https://fcm.googleapis.com/fcm/send';
					    $priority="high";

					    $customer_name='';
					    if(count($customer)>0){
					    	$customer_name=$customer[0]['name'];
					    }
					    $vendor=$this->db->get_where('vendor', array('id' => $id))->result_array();
						$name='';
						if(count($vendor)>0){
							$name=$vendor[0]['name'];
						}

					    $title = 'Service Request Accepted';
						$title1='Your service request '.$job_id.' accepted by '.$name.' ';
						$type='service_request_accept';
						$type1='vendor';

						$registrationIds=$customer[0]['firebase_token'];

						$send_id=$id;
					    $rec_id=$customer[0]['id'];

					    $notification= array('title' => $title,'message' => 'Hi '.$customer_name.','.$title1.' ','image'=>'','timestamp'=>$not_time );

					    $notification_array= array('sender_id'=>$send_id,'title' => $title,'message' => 'Hi '.ucfirst($customer_name).','.$title1,'status'=>0,'receiver_id'=>$rec_id,'type'=>$type1,'date'=>$cur_time,'time'=>$not_time);
				   
					    //print_r($registrationIds);
					    $payload = array();
					    $message = $notification['message'];
					    $push_type = isset($data->push_type) ? $data->push_type : '';
					    $push->setTitle($title);
					    $push->setMessage($message);
					    $push->setType($type);
					    $push->setId($data->request_id);
					    $push->setImage("");
					    $push->setIsBackground(FALSE);
					    $push->setPayload($payload);
					    $push_time = $date->format('Y-m-d G:i:s');
					    //$push->setTime($push_time);
					    $json = '';
					    $responses = '';
					    $json = $push->getPush();
					    $responses = $firebase->send($registrationIds, $json);
					  //print_r($responses);
					    if($responses){
				            $insert = $this->db->insert('notification',$notification_array); 
				        }

					}

				}
				return $response;
            }

		}
		else{

			$response['status']=false;
			$response['service_id']='';
			$response['message']='Job Already Accepted.';
			return $response;
			
		}


	}

	public function Getservicelist(){
		$service = $this->db->get_where('servicetype', array('status'=>1))->result_array();
		$i=0;$ser=array();
		if(count($service)>0){
			foreach ($service as $key => $value) {
				$ser[$i]=new stdClass();
				$ser[$i]->service_id=$value['id'];
				$ser[$i]->service_name=$value['service_type'];
				$i++;
			}
		}
		return $ser;
	}

	public function category_based_services($id){

		$category = $this->db->get_where('category', array('status'=>0))->result_array();
		$i=0;$cat=array();
		if(count($category)>0){
			foreach ($category as $key => $value) {
				$cat[$i]=new stdClass();
				$cat[$i]->category_id=$value['id'];
				$cat[$i]->category_name=$value['category_name'];
				$i++;
			}
		}
		$service = $this->db->get_where('servicetype', array('status'=>1))->result_array();
		$j=0;$ser=array();
		if(count($service)>0){
			foreach ($service as $key => $value) {
				$ser[$j]=new stdClass();
				$ser[$j]->service_id=$value['id'];
				$ser[$j]->service_name=$value['service_type'];
				$j++;
			}
		}
		$response['status']=true;
		$response['category']=$cat;
		$response['service']=$ser;
		return $response;

	}

	public function get_booking_details($id,$data){
		$condition="a.id='".$data->request_id."'";
		$this->db->select('a.commands,a.id as job_id,b.name as customer_name,b.mobile as customer_mobile,a.status as job_status,b.id as customer_address_id,c.category_name,a.date,a.time');
		$this->db->where($condition);
		$this->db->from('service a');
		$this->db->join('customer b', 'b.id = a.customer_id', 'left');
		$this->db->join('category c', 'c.id = a.category_id', 'left');
		$query = $this->db->get();
		$job_list = $query->result_array();
		$job=array();
      	if(count($job_list)>0){
        	$i=0;
	        foreach ($job_list as $key => $value) {
				$job[$i]['job_id']=$value['invoice_ref_no'];
				$job[$i]['customer_name']=$value['customer_name'];
				$job[$i]['customer_mobile']=$value['customer_mobile'];
				$address = $this->db->get_where('customer_address', array('customer_id'=>$value['customer_address_id']))->result_array();
				$add='';
				if(count($address)>0){
				$add=$address[0]['address'];
				}
				$job[$i]['customer_address']=$add;
				$job[$i]['date']=date('d-m-Y',strtotime($value['date']));
				$job[$i]['time']=date('h:i A',strtotime($value['time']));
				
				$i++;
	        }
    	}
        $response['status']=true;
		$response['job_details']=$job;
		return $response;
	}


	public function today_earnings($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=$date->format('h:i A');
		$cur_date=$date->format('d-m-Y');

		$condition="(vendor_id='".$id."' OR reassign_vendor_id='".$id."') AND status=5 AND complete_date='".$cur_date."'";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$query = $this->db->get();
		$today_service = $query->result_array();

		//$today_service = $this->db->get_where('service', array('vendor_id'=>$id, 'status'=>5,'complete_date'=>$cur_date))->result_array();
		$today=array();$i=0;$today_earn=0;
		if(count($today_service)>0){
			foreach ($today_service as $key => $value) {
				$today_earn += $value['total_cost'];
			}
		}
		$response['status']=true;
		$response['earnings']=$today_earn;
		$response['message']='';
		return $response;
	}


	public function month_earnings($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		$cur_time=$date->format('h:i A');
		$cur_date=$date->format('Y-m-d');
		$cur_month = $date->format('m');
		$cur_year = $date->format('Y');

		$month_val=date('m',strtotime($data->month));
		//print_r(date('m',$monn));exit;

		$condition="(vendor_id='".$id."' OR reassign_vendor_id='".$id."') AND status=5 AND month='".$month_val."' AND year='".$data->year."'";
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where($condition);
		$query = $this->db->get();
		$month_service = $query->result_array();


		//$month_service = $this->db->get_where('service', array('vendor_id'=>$id,'status'=>5,'month'=>$month_val,'year'=>$data->year))->result_array();
		$month=array();$i=0;$month_earn=0;
		if(count($month_service)>0){
			foreach ($month_service as $key => $value) {
				$month_earn += $value['total_cost'];
			}
		}
		$response['status']=true;
		$response['earnings']=$month_earn;
		$response['message']='';
		return $response;
	}

	public function custom_earnings($id,$data){
		$date = new DateTime('now', new DateTimeZone($this->timezone));
		if(strtotime($data->from)>strtotime($data->to)){
			$response['status']=false;
			$response['earnings']=0;
			$response['message']='Invalid To Date';
		}
		else{
			$date = new DateTime('now', new DateTimeZone($this->timezone));
			$date_from=date('d-m-Y',strtotime($data->from));
			$date_to=date('d-m-Y',strtotime($data->to));
			//$custom_earning=$this->db->where('complete_date  BETWEEN "'.$date_from.'" AND "'.$date_to.'"')->get_where('service',array('vendor_id'=>$id))->result_array();

			$condition="(vendor_id='".$id."' OR reassign_vendor_id='".$id."') AND status=5 AND complete_date  BETWEEN '".$date_from."' AND '".$date_to."' ";
			$this->db->select('*');
			$this->db->from('service');
			$this->db->where($condition);
			$query = $this->db->get();
			$custom_earning = $query->result_array();


			$custom=array();$i=0;$custom_earn=0;
			if(count($custom_earning)>0){
				foreach ($custom_earning as $key => $value) {
					$custom_earn += $value['total_cost'];
				}
			}
			$response['status']=true;
			$response['earnings']=$custom_earn;
			$response['message']='';
		}

		
		return $response;

	}


	public function insert_chat_message($id,$data){
		$admin = $this->db->get('admin')->result_array();
		$receiver_id=$admin[0]['id'];
		$type='user';
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$date_time = $date->format('Y-m-d H:i:s');
		$date1 = $date->format('Y-m-d');
		$time = $date->format('H:i A');
		
		$insert_array=array('sender_id'=>$id,'receiver_id'=>$receiver_id,'type'=>$type,'status'=>0,'date_time'=>$date_time,'date'=>$date1,'time'=>$time,'message' => $data->message,'timestmp' => strtotime($date_time));
		$this->db->insert('chat',$insert_array);
        $chat_id=$this->db->insert_id();


        $admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
        $cond1 = "(sender_id ='" . $id . "' AND receiver_id ='".$sender_id."' ) OR (sender_id ='" . $sender_id . "' AND receiver_id ='".$id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$this->db->group_by("date");
		$query = $this->db->get();
		$res=$query->result_array();
		$j=0;
		if(count($res)>0){
			$chat_msg=array();
			foreach ($res as $key => $value) {
				$i=0;
				
				if($value['date']!=''){

					$cond1 = "((sender_id ='" . $id . "' AND receiver_id ='".$sender_id."' ) OR (sender_id ='" . $sender_id . "' AND receiver_id ='".$id."')) AND date='".$value['date']."'";
					$this->db->select('*');
					$this->db->from('chat');
					$this->db->where($cond1);
					$this->db->order_by('timestmp','ASC');
					$query1 = $this->db->get();
					$res1=$query1->result_array();
					
					//print_r($res1);
					$chat=array();
					foreach ($res1 as $key1 => $value1) {
						$chat[$i]=new stdClass();
						if($value1['type']=='admin'){
							$chat[$i]->sender_name=$admin[0]['username'];
							$chat[$i]->sender_id=$admin[0]['id'];
							$chat[$i]->sender_image='';
							$chat[$i]->sender_email=$admin[0]['email'];

							$customer = $this->db->get_where('vendor', array('id' => $value1['receiver_id']))->result_array();
							$chat[$i]->receiver_id=$value1['receiver_id'];
							$chat[$i]->receiver_name=$customer[0]['name'];
							$chat[$i]->receiver_image='';
							$chat[$i]->receiver_email=$customer[0]['email'];
						}
						else{
							$chat[$i]->receiver_name=$admin[0]['username'];
							$chat[$i]->receiver_id=$value1['receiver_id'];
							$chat[$i]->receiver_image='';
							$chat[$i]->receiver_email=$admin[0]['email'];
							$customer = $this->db->get_where('vendor', array('id' => $value1['sender_id']))->result_array();
							$chat[$i]->sender_id=$value1['sender_id'];
							$chat[$i]->sender_name=$customer[0]['name'];
							$chat[$i]->sender_image='';
							$chat[$i]->sender_email=$customer[0]['email'];
						}
						$chat[$i]->type=$value1['type'];
						$chat[$i]->message=$value1['message'];
						$chat[$i]->date=$value1['date'];
						$chat[$i]->time=$value1['time'];
						$chat[$i]->date_time=$this->timeago($value1['timestmp']);
						if($date1 == $value1['date']){
							$chat[$i]->date_msg='Today';
						}
						else{
							$dt=date('F d, Y',strtotime($value1['date']));
							$chat[$i]->date_msg=$dt;
						}
						$i++;
					}
				}
				//$chat_msg[$j][$value['date']]=$chat;
				if($date1 == $value['date']){
					$chat_msg[$j]['date_msg']='Today';
				}
				else{
					$dt=date('F d, Y',strtotime($value['date']));
					$chat_msg[$j]['date_msg']=$dt;
				}
				$chat_msg[$j]['message_list']=$chat;

				$j++;
			}

		}
		$response['status']=true;
		//$response['messages']=$chat_msg;
		$response['message']='Message Send Successfully.';
		return $response;
	}

		public function chat($id){
		$this->load->helper('date');
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$cur_datetime = $date->format('Y-m-d H:i A');
		$admin = $this->db->get('admin')->result_array();
		$sender_id=$admin[0]['id'];
		$cur_date=$date->format('Y-m-d');
		$cond1 = "(sender_id ='" . $id . "' AND receiver_id ='".$sender_id."' ) OR (sender_id ='" . $sender_id . "' AND receiver_id ='".$id."')";
		$this->db->select('*');
		$this->db->from('chat');
		$this->db->where($cond1);
		$this->db->order_by('timestmp','ASC');
		$this->db->group_by("date");
		$query = $this->db->get();
		$res=$query->result_array();
		
		$j=0;$chat_msg=array();
		if(count($res)>0){
			
			foreach ($res as $key => $value) {
				$i=0;
				
				if($value['date']!=''){

					$cond1 = "((sender_id ='" . $id . "' AND receiver_id ='".$sender_id."' ) OR (sender_id ='" . $sender_id . "' AND receiver_id ='".$id."')) AND date='".$value['date']."'";
					$this->db->select('*');
					$this->db->from('chat');
					$this->db->where($cond1);
					$this->db->order_by('timestmp','ASC');
					$query1 = $this->db->get();
					$res1=$query1->result_array();
					
					//print_r($res1);
					$chat=array();
					foreach ($res1 as $key1 => $value1) {
						$chat[$i]=new stdClass();
						if($value1['type']=='admin'){
							$chat[$i]->sender_name=$admin[0]['username'];
							$chat[$i]->sender_id=$admin[0]['id'];
							$chat[$i]->sender_image='';
							$chat[$i]->sender_email=$admin[0]['email'];

							$customer = $this->db->get_where('vendor', array('id' => $value1['receiver_id']))->result_array();
							$chat[$i]->receiver_id=$value1['receiver_id'];
							$chat[$i]->receiver_name=$customer[0]['name'];
							$chat[$i]->receiver_image='';
							$chat[$i]->receiver_email=$customer[0]['email'];
						}
						else{
							$chat[$i]->receiver_name=$admin[0]['username'];
							$chat[$i]->receiver_id=$value1['receiver_id'];
							$chat[$i]->receiver_image='';
							$chat[$i]->receiver_email=$admin[0]['email'];
							$customer = $this->db->get_where('vendor', array('id' => $value1['sender_id']))->result_array();
							$chat[$i]->sender_id=$value1['sender_id'];
							$chat[$i]->sender_name=$customer[0]['name'];
							$chat[$i]->sender_image='';
							$chat[$i]->sender_email=$customer[0]['email'];
						}
						$chat[$i]->type=$value1['type'];
						$chat[$i]->message=$value1['message'];
						$chat[$i]->date=$value1['date'];
						$chat[$i]->time=$value1['time'];
						$chat[$i]->date_time=$this->timeago($value1['timestmp']);
						if($cur_date == $value1['date']){
							$chat[$i]->date_msg='Today';
						}
						else{
							$dt=date('F d, Y',strtotime($value1['date']));
							$chat[$i]->date_msg=$dt;
						}
						$i++;
					}
				}
				//$chat_msg[$j][$value['date']]=$chat;
				if($cur_date == $value['date']){
					$chat_msg[$j]['date_msg']='Today';
				}
				else{
					$dt=date('F d, Y',strtotime($value['date']));
					$chat_msg[$j]['date_msg']=$dt;
				}
				$chat_msg[$j]['message_list']=$chat;

				$j++;
			}

		}
		//print_r($chat_msg);

		$response['status']=true;
		$response['messages']=$chat_msg;
		return $response;
		

	}


	public function timeago($time) { 
  	
	    // Calculate difference between current 
	    // time and given timestamp in seconds 
		$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
		$date_time = $date->format('Y-m-d H:i:s');
	   
	    $diff     = strtotime($date_time) - $time; 

	      //1573036765 1573043473
	    // Time difference in seconds 
	    $sec     = $diff; 
	      
	    // Convert time difference in minutes 
	    $min     = round($diff / 60 ); 
	      
	    // Convert time difference in hours 
	    $hrs     = round($diff / 3600); 
	      
	    // Convert time difference in days 
	    $days     = round($diff / 86400 ); 
	      
	    // Convert time difference in weeks 
	    $weeks     = round($diff / 604800); 
	      
	    // Convert time difference in months 
	    $mnths     = round($diff / 2600640 ); 
	      
	    // Convert time difference in years 
	    $yrs     = round($diff / 31207680 ); 

	    // Check for seconds 
	    if($sec <= 60) { 
	        $a= "$sec seconds ago"; 
	    } 
	      
	    // Check for minutes 
	    else if($min <= 60) { 
	        if($min==1) { 
	            $a= "one minute ago"; 
	        } 
	        else { 
	            $a= "$min minutes ago"; 
	        } 
	    } 
	      
	    // Check for hours 
	    else if($hrs <= 24) { 
	        if($hrs == 1) {  
	            $a= "an hour ago"; 
	        } 
	        else { 
	            $a= "$hrs hours ago"; 
	        } 
	    } 
	      
	    // Check for days 
	    else if($days <= 7) { 
	        if($days == 1) { 
	            $a= "Yesterday"; 
	        } 
	        else { 
	            $a= "$days days ago"; 
	        } 
	    } 
	      
	    // Check for weeks 
	    else if($weeks <= 4.3) { 
	        if($weeks == 1) { 
	            $a= "a week ago"; 
	        } 
	        else { 
	            $a= "$weeks weeks ago"; 
	        } 
	    } 
	      
	    // Check for months 
	    else if($mnths <= 12) { 
	        if($mnths == 1) { 
	            $a= "a month ago"; 
	        } 
	        else { 
	            $a= "$mnths months ago"; 
	        } 
	    } 
	      
	    // Check for years 
	    else { 
	        if($yrs == 1) { 
	            $a= "one year ago"; 
	        } 
	        else { 
	            $a= "$yrs years ago"; 
	        } 
	    } 

	    return $a;
	}
	

	

 
}
?>