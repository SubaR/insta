<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<style type="text/css">

@media screen and (max-width: 600px) {
    table[class="container"] {
        width: 95% !important;
    }
}

	#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
			color: red !important; 
		 }

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
			color: purple !important; 
		}

		table td {border-collapse: collapse;}

		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

		a {color: #000;}

		@media only screen and (max-device-width: 480px) {

			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: black; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important; /* or whatever your want */
						pointer-events: auto;
						cursor: default;
					}
		}


		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			a[href^="tel"], a[href^="sms"] {
						text-decoration: none;
						color: blue; /* or whatever your want */
						pointer-events: none;
						cursor: default;
					}

			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
						text-decoration: default;
						color: orange !important;
						pointer-events: auto;
						cursor: default;
					}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {
			/* Put your iPhone 4g styles in here */
		}

		@media only screen and (-webkit-device-pixel-ratio:.75){
			/* Put CSS for low density (ldpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1){
			/* Put CSS for medium density (mdpi) Android layouts in here */
		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){
			/* Put CSS for high density (hdpi) Android layouts in here */
		}
		/* end Android targeting */
		h2{
			color:#181818;
			font-family: 'Open Sans', sans-serif;
			font-size:22px;
			line-height: 22px;
			font-weight: normal;
		}
		a.link1{

		}
		a.link2{
			color:#fff;
			text-decoration:none;
			font-family: 'Open Sans', sans-serif;
			font-size:16px;
			color:#fff;border-radius:4px;
		}
		p,b{
			color:#555;
			font-family: 'Open Sans', sans-serif;
			font-size:12px;
			line-height:24px;
			text-align:justify;
                        letter-spacing: 1px;
		}
		.social_links li,.app_links li
		{
			list-style:none;
			margin-left:10px;
			margin-right:10px;
			float:left;
                        font-family: 'Open Sans', sans-serif;
		}
		.app_links li
		{
			list-style:none;
			margin-left:10px;
			margin-right:10px;
			float:left;
			width:150px;
		}
		.social_links li img
		{
			width:36px;
			height:36px;
		}
		.app_links li img
		{
			width:100%;
			
		}
		b{
			color:#000;
			font-family: 'Open Sans', sans-serif;
			font-size:14px;
			line-height:160%;
			text-align:center;
			margin-top:10px;
		}
	</style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"fff",
    "color":"555555",
    "bgItem":"ffffff",
    "title":"181818"
  }
</script>


</head>
<body style="font-family: 'Open Sans', sans-serif !important;">
	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
	<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody' style="font-family: 'Open Sans', sans-serif !important;">
	<tr>
		<td>
			<table cellpadding="0" width="800" class="container" align="center" cellspacing="0" border="0">
				<tr>
					<td>
		

						<table cellpadding="0" cellspacing="0" border="0" align="center" width="800" class="container" style="border:1px solid #f2f2f2;margin-top:30px;">
							<tr>
								<td class='movableContentContainer bgItem'>
									<div class='movableContent' style=" ">
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="800" class="container">
											<tr>
												<td width="550" align="center">
													<div class='movableContent' style="overflow: hidden;
height: 800px;display: inline-block;margin-top:0px;background:#fff;width:795px;height:799px;box-shadow: 1px 1px 15px #cccccc;-webkit-box-shadow: 1px 1px 15px #cccccc;-moz-box-shadow: 1px 1px 15px #cccccc;border:5px solid #00aeef;margin-bottom: -5px;
"><!-- border-right:none; -->
														<table cellpadding="0" cellspacing="0" border="0" class="container">
															<tr>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-left:50px;">RECEIPT # <br> <span style="color:#00aeef;font-weight:bold;"><?=$content['job_id'];?></span></p>
																	<p style="color:#000;font-weight:bold;padding-left:50px;">SERVICE ADDRESS <br> <span style="color:#00aeef;font-weight:bold;"><?=$content['service_address'];?></span></p>
																	<!-- <p style="color:#000;font-weight:bold;padding-left:50px;">SERVICE DURATION <br> <span style="color:#00aeef;font-weight:bold;"><?=$content['duration'];?></span></p> -->
																</td>
																<td width="300"></td>
<!-- <td width="300">
<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;">TRIP TIME & DATE <br> <span style="color:#00aeef;font-weight:bold;"></span></p>
<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;">DROP POINT <br> <span style="color:#00aeef;font-weight:bold;"></span></p>
<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;">TRIP DURATION <br> <span style="color:#00aeef;font-weight:bold;"></span></p>
</td> -->
															</tr>
														</table>
															<table cellpadding="0" cellspacing="0" border="0" class="container">
																<tr>
																	<td width="300">
																		<p style="color:#000;font-weight:bold;padding-left:50px;">RECEIPT SUMMARY <br></p>
																			<p style="color:#000;font-weight:bold;padding-left:50px;">PAYMENT METHOD : <span style="color:#00aeef;font-weight:bold;">CASH</span> </p>

																	</td>
																	<td width="300"></td>
																</tr>
															</table>
															<table cellpadding="0" cellspacing="0" border="0" class="container">
															<tr>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-left:50px;padding-bottom:0px;">DESCRIPTION</p>
																</td>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;padding-bottom:0px;">AMOUNT</p>
																</td>
															</tr>
															<tr>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-left:50px;padding-bottom:0px;margin: 0;line-height: 16px;">SERVICE COST</p>
																</td>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;"><span style="color:#00aeef;font-weight:bold;padding-bottom:0px;margin: 0;
															line-height: 16px;"><?=$content['total_fare'];?>		</span></p>
																</td>
															</tr>
															<!--<tr><td width="300">
															<p style="color:#000;font-weight:bold;padding-left:50px;padding-bottom:0px;margin: 0;
															line-height: 16px;">PROMOTION</p></td>
															<td width="300">
															<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;"><span style="color:#00aeef;font-weight:bold;padding-bottom:0px;margin: 0;
															line-height: 16px;">-99</span></p></td></tr>-->
															<tr>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-left:50px;padding-bottom:0px;margin: 0;line-height: 16px;">TOTAL</p>
																</td>
																<td width="300">
																	<p style="color:#000;font-weight:bold;padding-right:50px;text-align:right;"><span style="color:#00aeef;font-weight:bold;padding-bottom:0px;margin: 0;
																line-height: 16px;"><?=$content['total_fare'];?>	</span></p>
																</td>
															</tr>

														</table>
														<table cellpadding="0" cellspacing="0" border="0" class="container">
														<tr>
															<td width="600">
																<p style="color:#000;font-weight:bold;padding-left:50px;padding-right:10px;text-transform: uppercase;
																text-align: justify;">Dear <?=$content['customer_name'];?>, We hope you had a pleasent and comfortable service experience with us !</p>


															</td>

														</tr>
														<tr>
															<td width="600">
																<p style="color:#000;font-weight:bold;padding-left:50px;padding-right:10px;font-size:20px;">TOTAL FARE <span style="color:#00aeef;font-weight:bold;padding-bottom:0px;margin: 0;
															line-height: 16px;"><?=$content['total_fare'];?></span></p>
															</td>

														</tr>
													</table>
													<table cellpadding="0" cellspacing="0" border="0" class="container" style="border-top:2px solid #ddd;">
													<tr>
														<td width="600" colspan="2"><p style="color:#000;font-weight:bold;padding-left:50px;">VENDOR DETAILS <br></p>
														</td>

													</tr>
													<tr>
														<td width="200" align="center">
															<?php if($content['image']!=''){ ?>
															<img src="<?=$content['image'];?>" style="width:90px;height:90px;" />
															<?php } else { ?>
															<img src="<?=$theme_path?>img/user_profile.png" style="width:90px;height:90px;" />	
															<?php } ?>
														</td>
														<td width="400">
															<p style="color:#000;font-weight:bold;">VENDOR NAME <br> <span style="color:#00aeef;font-weight:bold;"><?=ucfirst($content['vendor_name']);?></span></p>
															<p style="color:#000;font-weight:bold;">CATEGORY NAME <br> <span style="color:#00aeef;font-weight:bold;"><?=ucfirst($content['category_name']);?></span></p>
															<p style="color:#000;font-weight:bold;">SERVICE NAME <br> <span style="color:#00aeef;font-weight:bold;"><?=ucfirst($content['service_name']);?></span></p>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</td></tr></table>
	

</body>
</html>
