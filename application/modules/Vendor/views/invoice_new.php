<?php  $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/admin_template/'; ?>
<!-- Main Linking Action Content -->
<?php 
     $url=$this->config->item('base_url').'Admin'; 
     //print_r($sales);exit;
?>
<style>
	    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
        background:#fff;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>
<!-- Main Linking Action Content End -->

<!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5 class="f_width">Sales Transaction </h5>
      </div><!-- kt-pagetitle -->

      <div class="kt-pagebody">
		  
		  <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="<?=$theme_path;?>img/logo.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                Invoice #: <?=$content['job_id'];?><br>
                                Created: <br>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
								<b>Customer Details</b> <br>
                                <?=$content['customer_name'];?><br>
                                <?=$content['customer_email'];?><br>
                                <!-- <?=$sales->c_mobile;?> -->
                            </td>
                            
                            <td>
								<b>Driver Details</b><br>
                                <?=$content['vendor_name'];?><br>
                                <?=$content['vendor_email'];?><br>
                                <!-- <?=$sales->d_mobile;?> -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Cash
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Category Name
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    <?=$content['category_name'];?>
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="heading">
                <td style="width:60%;">
                    Trip Summary
                </td>
                
                <td style="width:40%;">
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td style="width:60%;">
                     <strong><?=$content['service_address'];?></strong><br>
                </td>
                
                <td style="width:40%;">
                    <?=$content['total_fare'];?>
                </td>
            </tr>
          
           
        </table>
</div>


		
        
      </div><!-- kt-pagebody -->