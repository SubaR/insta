-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 02, 2020 at 12:17 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `instahome2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `email` varchar(500) NOT NULL,
  `photo` varchar(500) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `hash`, `email`, `photo`, `name`, `mobile`, `role`, `status`) VALUES
(1, 'Kiran', '202cb962ac59075b964b07152d234b70', '123', 'kiraninsta@mailinator.com', NULL, 'kiran', '8344477251', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `amount_history`
--

CREATE TABLE `amount_history` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `request_id` int(11) NOT NULL COMMENT 'booking prim id',
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_request`
--

CREATE TABLE `book_request` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `commands` varchar(1000) NOT NULL,
  `service_name` varchar(300) DEFAULT NULL,
  `service_id` int(11) NOT NULL DEFAULT '0',
  `sub_category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `time` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `level` varchar(50) NOT NULL,
  `reassign_status` int(11) NOT NULL COMMENT '0-notassign,1-reassign',
  `notify_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `status`) VALUES
(1, 'Others', 1),
(2, 'GE (US)', 1),
(3, 'LG (Korea)', 1),
(4, 'Electrolux (Sweden)', 1),
(5, 'Samsung (Korea)', 1),
(6, 'Haier (China)', 1),
(7, 'GODREJ', 1),
(8, 'Midea (China)', 1),
(10, 'Panasonic', 1),
(11, 'Philips', 1),
(12, 'Whirlpool', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `category_amount` varchar(250) NOT NULL,
  `icon` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `category_amount`, `icon`, `status`) VALUES
(1, 'Washing Machine', '199.00', 'upload_file_10803254.jpeg', 0),
(2, 'Refrigerator', '199.00', 'upload_file_432925633.jpeg', 0),
(3, 'AirConditioner', '199.00', 'upload_file_698952739.jpeg', 0),
(4, 'Television', '250.00', 'upload_file_1843127480.jpeg', 0),
(5, 'Microwave Oven', '199.00', 'upload_file_377444276.jpeg', 0),
(6, 'Ro Water Purifier', '199.00', 'upload_file_2054069290.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_brand`
--

CREATE TABLE `category_brand` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_brand`
--

INSERT INTO `category_brand` (`id`, `category_id`, `brand_id`, `status`, `date`, `time`) VALUES
(1, 2, '3,5,6,7', 1, '2019-12-28 14:38:53', '14:38:53'),
(2, 1, '1,3,6', 1, '2019-12-23 14:04:14', '14:04:14'),
(3, 3, '4,5', 1, '2019-12-23 14:05:12', '14:05:12'),
(4, 4, '3,5', 1, '2019-12-23 14:05:21', '14:05:21'),
(5, 5, '10,11', 1, '2019-12-23 14:08:18', '14:08:18'),
(6, 6, '3,11', 1, '2019-12-23 14:08:57', '14:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `date_time` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `timestmp` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cron_job_cache`
--

CREATE TABLE `cron_job_cache` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `country_code` int(11) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `photo` varchar(500) NOT NULL DEFAULT '0',
  `token` varchar(1000) DEFAULT NULL,
  `device_id` varchar(1000) DEFAULT NULL,
  `firebase_token` varchar(1000) NOT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `reg_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE `customer_address` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `rating` int(11) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `offer_name` varchar(250) NOT NULL,
  `offer_per` int(11) NOT NULL COMMENT '%',
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `pro_cate_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `otp` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL DEFAULT '0',
  `key` varchar(150) NOT NULL,
  `login_status` int(11) NOT NULL,
  `country_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `invoice_ref_no` varchar(250) NOT NULL,
  `request_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `reassign_vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `start_otp` varchar(10) NOT NULL,
  `end_otp` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `start_time` varchar(20) NOT NULL,
  `end_time` varchar(20) NOT NULL,
  `commands` varchar(1000) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `duration` varchar(250) NOT NULL,
  `amount` varchar(250) NOT NULL,
  `offer_cost` varchar(250) NOT NULL,
  `additional_cost` varchar(250) NOT NULL,
  `total_cost` varchar(250) NOT NULL,
  `start_otp_verify` int(11) NOT NULL,
  `end_otp_verify` int(11) NOT NULL,
  `followup_date` varchar(50) NOT NULL,
  `followup_time` varchar(50) NOT NULL,
  `followup_comments` text NOT NULL,
  `reassign_status` int(11) NOT NULL COMMENT '1-not assign,0-reassign',
  `action_status` int(11) NOT NULL COMMENT '1-followup, 2-complete',
  `month` varchar(50) NOT NULL,
  `year` varchar(10) NOT NULL,
  `complete_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `servicetype`
--

CREATE TABLE `servicetype` (
  `id` int(11) NOT NULL,
  `service_type` varchar(250) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicetype`
--

INSERT INTO `servicetype` (`id`, `service_type`, `status`) VALUES
(1, 'Service', 1),
(2, 'Repairs', 1),
(3, 'Installation', 1),
(4, 'UnInstallation', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(200) NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `service_type` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `image`, `status`, `service_type`) VALUES
(1, 1, 'TOP LOAD', 'upload_file_225901718.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(2, 1, 'Front Load', 'upload_file_424772961.jpeg', 0, 'Service,Repairs'),
(3, 1, 'Fully automatic', 'upload_file_106649950.jpeg', 0, 'Service,Repairs,Installation'),
(4, 1, 'Others', 'upload_file_1067732818.jpeg', 0, 'Service,Repairs,Installation'),
(5, 2, 'Single Door', 'upload_file_1294601132.jpeg', 0, 'Service,Repairs,Installation'),
(6, 2, 'Double Door', 'upload_file_1919312695.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(7, 2, 'Side By Side Dr', 'upload_file_608206825.jpeg', 0, 'Service,Repairs,Installation'),
(8, 2, 'Others', 'upload_file_953854261.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(9, 3, 'Split Ac', 'upload_file_287496235.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(10, 3, 'Window Ac', 'upload_file_373242431.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(11, 3, 'Cassette Ac', 'upload_file_2006821987.jpeg', 0, 'Service,Repairs,Installation'),
(12, 3, 'Others', 'upload_file_740276021.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(13, 4, 'Led Tv', 'upload_file_20177917.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(14, 4, 'Lcd Tv', 'upload_file_261068188.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(15, 4, 'Oled Tv', 'upload_file_1680401007.jpeg', 0, 'Service,Repairs,Installation'),
(16, 4, 'Ultra 4k', 'upload_file_507110878.jpeg', 0, 'Service,Repairs,Installation,UnInstallation'),
(17, 4, 'Full Hd', 'upload_file_41167950.jpeg', 0, 'Service,Repairs,Installation'),
(18, 5, 'Grill microwave oven', 'upload_file_765881365.jpeg', 0, ''),
(19, 5, 'Convection Microwave Oven', 'upload_file_1400945135.jpeg', 0, ''),
(20, 5, 'Others', 'upload_file_2128802928.jpeg', 0, ''),
(21, 6, 'UF Water Purifier', 'upload_file_1296139772.jpeg', 0, ''),
(22, 6, 'UV Water Purifier', 'upload_file_2091471372.jpeg', 0, ''),
(23, 1, 'OT', 'upload_file_286779341.jpeg', 0, 'Service,Repairs'),
(24, 4, 'HD', 'upload_file_227337955.jpeg', 0, 'Service,Repairs,Installation');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `country_code` int(11) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `id_proof` varchar(200) NOT NULL,
  `token` varchar(1000) NOT NULL,
  `device_id` varchar(1000) NOT NULL,
  `platform` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `reg_date` datetime NOT NULL,
  `commands` varchar(1000) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `category` varchar(200) NOT NULL,
  `service_type` varchar(1000) NOT NULL,
  `firebase_token` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amount_history`
--
ALTER TABLE `amount_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_request`
--
ALTER TABLE `book_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_brand`
--
ALTER TABLE `category_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cron_job_cache`
--
ALTER TABLE `cron_job_cache`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicetype`
--
ALTER TABLE `servicetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amount_history`
--
ALTER TABLE `amount_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_request`
--
ALTER TABLE `book_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category_brand`
--
ALTER TABLE `category_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cron_job_cache`
--
ALTER TABLE `cron_job_cache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_address`
--
ALTER TABLE `customer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servicetype`
--
ALTER TABLE `servicetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
