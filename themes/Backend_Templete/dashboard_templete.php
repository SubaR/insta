<?php
$theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
$base_url=$this->config->item('base_url').'Admin'; 
$alert=$this->session->userdata('tostartnotify');
$alert_msg=$alert['toastr_value']; 
$admin_login=$this->session->userdata('admin'); 

$notification = $this->Admin_model->get_all_notification_admin();
$count=count($notification);


if($admin_login['email']==''){ 
   redirect('/Admin/logout');
} ?>
<!doctype html>
<html lang="en">
<head>
<title>:: Insta Home Serve :: Dashboard</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="<?=$theme_path;?>assets/images/loading/favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/toastr/toastr.min.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=$theme_path;?>assets/main_assets/css/main.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/main_assets/css/color_skins.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/parsleyjs/css/parsley.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/sweetalert/sweetalert.css">

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">

<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">

<style type="text/css">
    .menu-icon {
        margin-right: 1.25rem;
        width: 25px;
        line-height: 1; }
        .notification-dot{
            background-color: #3bc978 !important; 
        }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
 --><!-- Javascript -->
<script src="<?=$theme_path;?>assets/main_assets/bundles/libscripts.bundle.js"></script>    
<script src="<?=$theme_path;?>assets/main_assets/bundles/vendorscripts.bundle.js"></script>
<script src="<?=$theme_path;?>assets/main_assets/bundles/chartist.bundle.js"></script>
<script src="<?=$theme_path;?>assets/main_assets/bundles/knob.bundle.js"></script> 
<script src="<?=$theme_path;?>assets/vendor/toastr/toastr.js"></script>
<script src="<?=$theme_path;?>assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js"></script>
<script src="<?=$theme_path;?>assets/main_assets/bundles/mainscripts.bundle.js"></script>
<!-- Welcome Notification-->
<!-- <script src="<?=$theme_path;?>assets/main_assets/js/index.js"></script> -->
<!-- Tostar Notification-->
<script src="<?=$theme_path;?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
 <!-- SweetAlert Plugin Js --> 
 <script src="<?=$theme_path;?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>-->
 <!-- JS FORM Validation Plugin Js --> 
<script src="<?=$theme_path;?>assets/vendor/parsleyjs/js/parsley.min.js"></script>

<script src="<?=$theme_path;?>assets/vendor/jquery-validation/jquery.validate.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-steps/jquery.steps.js"></script> 

<!-- Editable Table Plugin Js --> 
<script src="<?=$theme_path;?>assets/vendor/editable-table/mindmup-editabletable.js"></script> 
<script src="<?=$theme_path;?>assets/main_assets/js/pages/tables/editable-table.js"></script>
<script src="<?=$theme_path;?>assets/main_assets/bundles/datatablescripts.bundle.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?=$theme_path;?>assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="<?=$theme_path;?>assets/main_assets/js/pages/tables/jquery-datatable.js"></script>


<script src="<?=$theme_path;?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>


</head>
<body class="theme-cyan">
<div>
<!-- Toaster Alert Page By boopathi -->
<?php $this->load->view('Admin/toast'); ?>  
<!-- End Toaster Alert Page By boopathi -->
</div>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="<?=$theme_path;?>assets/images/loading/loading.gif"  alt="Lucid"></div>
              
    </div>
</div>
<!-- Overlay For Sidebars -->

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <div class="navbar-brand">
                <a href="<?=$base_url;?>"><img src="<?=$this->config->item('base_url');?>/icons/sample_logo.png" alt="Lucid Logo" style="height: 30px;width: 35px;" class="img-responsive logo"></a>                
            </div>
            
            <div class="navbar-right">
                           

                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                       

                          <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="<?php if($count!=0){?>notification-dot<?php }?>"></span>
                            </a>
                            <ul class="dropdown-menu notifications">
                                <?php if($count!=0){?>
                                <li class="header"><strong>You have <?php echo $count;?> new Notifications</strong></li><?php } else{?>
                                   <li class="header"><strong>No Notification</strong></li> 
                                <?php }?>
                            <?php if(count($notification)!=0){ 
                                 foreach($notification as $val)
                                 {
                                 ?>
                                <li>
                                    <a href="<?=$base_url;?>/notification_read" class="dropdown-item preview-item">
                                        <div class="media">
                                            <div class="media-left">
                                                <i class="icon-info text-warning"></i>
                                            </div>
                                            <div class="media-body">
                                                <p class="text"><?=$val['message'];?></p>
                                                <span class="timestamp"><?=$val['date'].' '.$val['time'];?></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>                               
                                
                                <?php }}?>
                            </ul>
                        </li>









                        <li>
                            <a href="<?=$base_url;?>/logout" class="icon-menu"><i class="icon-login"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                <img src="<?=$theme_path;?>assets/images/user.png" class="rounded-circle user-photo" alt="User Profile Picture">
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?=$admin_login['name'];?></strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account">
                        <li><a href="<?=$base_url;?>/profile"><i class="icon-user"></i>My Profile</a></li>
                        
                        <li class="divider"></li>

                        <li><a href="<?=$base_url;?>/logout"><i class="icon-power"></i>Logout</a></li>
                    </ul>
                </div>
                <hr>
          
            </div>
         
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">                            
                            <li class="<?php if($Activebar=='dashboard'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>" ><i class="icon-home"></i> <span>Dashboard</span></a>
                            </li>
                            <li class="<?php if($Activebar=='vendor'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/vendor" ><i class="icon-users"></i> <span>Vendor Management</span></a>
                            </li>
                            <li class="<?php if($Activebar=='customer'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/customer" ><i class="icon-user"></i> <span>Customer Management</span></a>
                            </li>
                            <li class="<?php if($Activebar=='servicetype'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/servicelist" ><!-- <i class="icon-list"></i> --> 
                                    <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/service.png"><span>Service Type</span></a>
                            </li>
                            <li class="<?php if($Activebar=='category'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/category" ><!-- <i class="icon-list"></i> --> 
                                     <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/category.png"><span>Category</span></a>
                            </li>
                            <li class="<?php if($Activebar=='offer'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/offer" ><!-- <i class="icon-list"></i> --> 
                                     <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/offer.png"><span>Offer Management</span></a>
                            </li>
                             <li class="<?php if($Activebar=='subcategory'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/subcategory" ><!-- <i class="icon-list"></i> --> <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/subcategory.png"> <span>Sub Category</span></a>
                            </li>

                            <li class="<?php if($Activebar=='brand'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/brand" ><!-- <i class="icon-list"></i> --> 
                                     <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/category.png"><span>Brand</span></a>
                            </li>

                            <li class="<?php if($Activebar=='service_request'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/service_request" ><!-- <i class="icon-list"></i> --> <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/job_request.png"><span>Job Request</span></a>
                            </li>

                             <li class="<?php if($Activebar=='job_assign'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/assign_list/<?=base64_encode(base64_encode('1'))?>" ><i class="icon-list"></i> <span>Job List</span></a>
                            </li>

                            <!-- <li class="<?php if($Activebar=='job_completed_list'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/job_list/<?=base64_encode(base64_encode('1'))?>" ><i class="icon-list"></i> <span>Job List</span></a>
                            </li> -->

                            <li class="<?php if($Activebar=='chat'){ echo 'active';} ?>">
                                <a href="<?=$base_url;?>/chat" ><!-- <i class="icon-list"></i> --> 

                                    <img class="menu-icon" src="<?=$this->config->item('base_url')?>icons/chat.png">

                                    <span>Chat</span></a>
                            </li>

                           
                       
                        </ul>
                    </nav>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="Chat">
                    <form>
                        <div class="input-group m-b-20">
                            <div class="input-group-prepend">
                                <span class="input-group-text" ><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="right_chat list-unstyled">
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="<?=$theme_path;?>assets/images/xs/avatar4.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Chris Fox</span>
                                        <span class="message">Designer, Blogger</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="<?=$theme_path;?>assets/images/xs/avatar5.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Joge Lucky</span>
                                        <span class="message">Java Developer</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="<?=$theme_path;?>assets/images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Isabella</span>
                                        <span class="message">CEO, Thememakker</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="<?=$theme_path;?>assets/images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Folisise Chosielie</span>
                                        <span class="message">Art director, Movie Cut</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="<?=$theme_path;?>assets/images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander</span>
                                        <span class="message">Writter, Mag Editor</span>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </a>                            
                        </li>                        
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="setting">
                    <h6>Choose Skin</h6>
                    <ul class="choose-skin list-unstyled">
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>                   
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="cyan" class="active">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="blush">
                            <div class="blush"></div>
                            <span>Blush</span>
                        </li>
                    </ul>
                    <hr>
                    <h6>General Settings</h6>
                    <ul class="setting-list list-unstyled">
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Report Panel Usag</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Email Redirect</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox" checked>
                                <span>Notifications</span>
                            </label>                      
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Auto Updates</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Offline</span>
                            </label>
                        </li>
                        <li>
                            <label class="fancy-checkbox">
                                <input type="checkbox" name="checkbox">
                                <span>Location Permission</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane p-l-15 p-r-15" id="question">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" ><i class="icon-magnifier"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                    <ul class="list-unstyled question">
                        <li class="menu-heading">HOW-TO</li>
                        <li><a href="javascript:void(0);">How to Create Campaign</a></li>
                        <li><a href="javascript:void(0);">Boost Your Sales</a></li>
                        <li><a href="javascript:void(0);">Website Analytics</a></li>
                        <li class="menu-heading">ACCOUNT</li>
                        <li><a href="javascript:void(0);">Cearet New Account</a></li>
                        <li><a href="javascript:void(0);">Change Password?</a></li>
                        <li><a href="javascript:void(0);">Privacy &amp; Policy</a></li>
                        <li class="menu-heading">BILLING</li>
                        <li><a href="javascript:void(0);">Payment info</a></li>
                        <li><a href="javascript:void(0);">Auto-Renewal</a></li>                        
                        <li class="menu-button m-t-30">
                            <a href="javascript:void(0);" class="btn btn-primary"><i class="icon-question"></i> Need Help?</a>
                        </li>
                    </ul>
                </div>                
            </div>          
        </div>
    </div>



    <div id="main-content">
       <?php echo $content; ?>
    </div>
    


</div>
<script type="text/javascript">
   $(function() {
        // validation needs name of the element
        $('#privilege').multiselect();
    });

var alertmsg = '<?php echo $alert_msg;?>'; 
var data = alertmsg;
        if(data==1)
       {
            toastr.success("Insert Successfully..!!");
       }
       else if(data==2)
       {  
             toastr.error("Insert Fail, Something Worng..!!");
       }
      else if(data==11)
       {  
             toastr.success("Update Successfully..!!");
       }
      else if(data==22)
       {  
             toastr.error("Update Fail, Something Worng..!!");
       }
      else if(data==111)
       { 
             toastr.success("Delete Successfully..!!");
       }
      else if(data==222)
       {  
             toastr.error("Delete Fail, Something Worng..!!");
       }
       else if(data==123)
       {  
             toastr.info("File Upload Fail, Something Worng, Try Again..!!");
       }
       else if(data=='Sendpushnotify')
       {  
             toastr.success("Teacher Push Notification, Sending Success..!!");
       }
       else if(data=='Failpushnotify')
       {  
             toastr.error("Teacher Push Notification, Sending Fail..!!..!!");
       }
<?php
   // Removing session data
    $session_alert = array(
    'toastr_value' => ''
    );

   $noti_data = array(
    'session_login_alert' => ''
    );
   $this->session->set_userdata('tostartnotify',$session_alert); 
?>
</script>

<script type="text/javascript">
    
    document.addEventListener('DOMContentLoaded', function() {
 if (!Notification) {
  alert('Desktop notifications not available in your browser. Try Chromium.');
  return;
 }

 if (Notification.permission !== 'granted')
  Notification.requestPermission();
});
var url1='<?php echo $base_url;?>';
function notifyMe() {//alert(Notification.permission);
    if (!window.Notification) {
        console.log('Browser does not support notifications.');
    } else {
        // check if permission is already granted
        if (Notification.permission === 'granted') {
            
            $.ajax({
                type: "GET",
                url: url1+"/getnewbooking",
                success: function(data)
                {//alert(data);
                    var objJSON = JSON.parse(data);
                   // alert(objJSON);
                    if(objJSON.name!=''){
                        var options = {
                          body: "Hi "+objJSON.admin_name+", You have received a new service request from" +objJSON.name+"  ",
                          icon: objJSON.image,
                          dir : "ltr"
                        };
                        var notification = new Notification("Hi there",options);
                        if(notification){
                            $.ajax({
                                type: "POST",
                                url: url1+"/updatenotifystatus",
                                data:{booking_id:objJSON.booking_id},
                                success: function(data)
                                {
                                    //alert(data);
                                   
                                    if(data==1){
                                        
                                    }else{
                                       
                                    }
                                }

                            });
                        }
                    }else{
                       
                    }
                }

            });
            


        } else {
            // request permission from user
            Notification.requestPermission().then(function (p) {
                if (p === 'granted') {
                    // show notification here
                    var notify = new Notification('Hi there!', {
                        icon: 'http://localhost/edlite_web/uploads/student/upload_file_102543959.jpeg',
                        body: '<div class="notifyjs-corner">How are you doing?</div>',
                       
                    });
                } else {
                    console.log('User blocked notifications.');
                }
            }).catch(function (err) {
                console.error(err);
            });
        }
    }
}




//setInterval(notifyMe, 2000);
</script>




</body>
</html>


<!-- <html>
<title>Firebase Messaging Demo</title>
<style>
    div {
        margin-bottom: 15px;
    }
</style>
<body>
    <div id="token"></div>
    <div id="msg"></div>
    <div id="notis"></div>
    <div id="err"></div>
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
    <script>
        MsgElem = document.getElementById("msg")
        TokenElem = document.getElementById("token")
        NotisElem = document.getElementById("notis")
        ErrElem = document.getElementById("err")
        // Initialize Firebase
        // TODO: Replace with your project's customized code snippet
        var config = {
            apiKey: "AIzaSyBVC_mZKN7_xl7t-EsFQdlCKRV9cKCQiJs",
            authDomain: "unique-balancer-231811.firebaseapp.com",
            databaseURL: "https://unique-balancer-231811.firebaseio.com",
            storageBucket: "unique-balancer-231811.appspot.com",
            messagingSenderId: "571539118917",
        };
        firebase.initializeApp(config);

        const messaging = firebase.messaging();
        messaging
            .requestPermission()
            .then(function () {
                MsgElem.innerHTML = "Notification permission granted." 
                console.log("Notification permission granted.");
                console.log(messaging.getToken());
                // get the token in the form of promise
                return messaging.getToken()
            })
            .then(function(token) {
                TokenElem.innerHTML = "token is : " + token
            })
            .catch(function (err) {
                ErrElem.innerHTML =  ErrElem.innerHTML + "; " + err
                console.log("Unable to get permission to notify.", err);
            });

        messaging.onMessage(function(payload) {
            console.log("Message received. ", payload);
            NotisElem.innerHTML = NotisElem.innerHTML + JSON.stringify(payload) 
        });
    </script>

    <body>

</html> -->


 <script type="text/javascript">
    /*$( document ).ready(function() {
        toastr.success("send");
    });*/
    // <?php if($this->session->flashdata('success')){ ?>
    //     toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    // <?php }else if($this->session->flashdata('error')){  ?>
    //     toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    // <?php }else if($this->session->flashdata('warning')){  ?>
    //     toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    // <?php }else if($this->session->flashdata('info')){  ?>
    //     toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    // <?php } ?>

</script>