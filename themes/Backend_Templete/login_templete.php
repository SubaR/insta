<?php
       $theme_path = $this->config->item('theme_locations').$this->config->item('active_template').'/'; 
       $base_url=$this->config->item('base_url').'Admin'; 
    $admin_login=$this->session->userdata('admin'); 
if($admin_login['email']!=''){ 
   redirect('/Admin');
}

?>
<!doctype html>
<html lang="en">
<head>
<title>:: Insta Home Serve  :: Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" href="<?=$theme_path;?>assets/images/loading/favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/font-awesome/css/font-awesome.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?=$theme_path;?>assets/main_assets/css/main.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/main_assets/css/color_skins.css">
<link rel="stylesheet" href="<?=$theme_path;?>assets/vendor/toastr/toastr.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?=$theme_path;?>assets/vendor/toastr/toastr.js"></script>
 <!-- SweetAlert Plugin Js --> 
<script src="<?=$theme_path;?>assets/vendor/sweetalert/sweetalert.min.js"></script>
 <!-- JS FORM Validation Plugin Js --> 
<script src="<?=$theme_path;?>s/vendor/parsleyjs/js/parsley.min.js"></script>
</head>

<body class="theme-cyan">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                        <?php echo $content; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>
</html>